<?php get_header(); ?>
<?php get_sidebar(); ?>
<link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css" rel="stylesheet">
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/dataTables.bootstrap.js"></script>
<!--   **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3><i class="fa fa-angle-right"></i> Venues</h3>

              <div class="row mt">
                  <div class="col-md-12">
                      <div class="content-panel">
                          <table class="table table-striped table-advance table-hover" id="new">
	                  	  	  <h4><i class="fa fa-angle-right"></i> Advanced Table</h4>

	                  	  	  <hr>
                              <thead>
                              <tr>
                                  <th><i class="fa fa-bullhorn"></i> Flowers</th>
                                  <th><i class="fa fa-question-circle"></i>Colors</th>
                                  <th><i class="fa fa-bookmark"></i> Stems Per Bunch</th>
                                  <th>Cost/Bunch</th>
                                  <th><i class=" fa fa-edit"></i> Type</th>
                                  <th></th>
                              </tr>
                              </thead>
                              <tbody>
                                <?php 
                                query_posts('post_type=flowers&p=');
                                if(have_posts()) : while(have_posts()) : the_post();
                                ?>
                              <tr>
                                  <td><a href="javascript:void(0);" class="flower_title"><?php the_title(); ?></a></td>
                                  <td class="hidden-phone"><?php echo get_field('flower_color'); ?></td>
                                  <td><?php echo get_field('stemp'); ?></td>
                                  <td><input type="text" name="cost" placeholder="10.00"></td>
                                  <td>Focal/Accent</td>
                                  <td>
                                      <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button>
                                  </td>
                              </tr>
                            <?php endwhile; 
								else:
									echo "<h2>No Flower.</h2>";
								endif;
							wp_reset_query(); ?>
                            
                              </tbody>
                          </table>
                      </div><!-- /content-panel -->
                  </div><!-- /col-md-12 -->
              </div><!-- /row -->

    		</section><!--/wrapper -->
      </section><!-- /MAIN CONTENT -->
      <script>
      $(function() {
            $("#new").dataTable();
      });
</script>
      <!--main content end-->

<?php get_footer(); ?>