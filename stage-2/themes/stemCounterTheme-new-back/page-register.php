<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="StemCounter.com">
    <meta name="keyword" content="StemCounter, Stem Count, Flowers, Wedding Flowers">

    <title><?php wp_title(' ', true, 'right'); ?></title>
    
	<link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet">
    <?php wp_head(); ?>
    
    <!-- Bootstrap core CSS -->
    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="<?php echo get_template_directory_uri(); ?>/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <style type="text/css">
  br{
    display: none;
  }

.checkboxRegistration{

	float: left;
	margin: 0 !important;

}
.ui-widget-content {
  position: absolute;
  border: 3px solid #D6D6D6;
  background-color: #FFFFFF;
  border-radius: 10px;
  padding: 5px;
  width: 400px;
}
  </style>
<?php 
add_action('template_redirect', 'login_header');
function login_header( $title = 'Log In', $message = '', $wp_error = '' ) 
{
  global $error, $interim_login, $action;
  add_action( 'login_head', 'wp_no_robots' );
  if ( wp_is_mobile() )
   //add_action( 'login_head', 'wp_login_viewport_meta' );

  if ( empty($wp_error) )
    $wp_error = new WP_Error();
  $shake_error_codes = array( 'empty_password', 'empty_email', 'invalid_email', 'invalidcombo', 'empty_username', 'invalid_username', 'incorrect_password','username_exists' );
  
  $shake_error_codes = apply_filters( 'shake_error_codes', $shake_error_codes );

	
	if(is_numeric( $wp_error ) )
	{
	
		$creds = array();
		$creds['user_login'] = $_POST['user_login'];
		$creds['user_password'] =$_POST['ws_plugin__s2member_custom_reg_field_user_pass1'];
		$creds['remember'] = true;	
		$_SESSION['user_session']=$creds;
	?>	
	<script>window.location.href = '<?php bloginfo('url'); ?>/events/';</script>
	<?php 	
	exit;
	}
	
	if ( $shake_error_codes && $wp_error->get_error_code() && in_array( $wp_error->get_error_code(), $shake_error_codes ) )
	add_action( 'login_head', 'wp_shake_js', 12 );
  ?>
  <?php

  //wp_admin_css( 'login', true );

  /*
   * Remove all stored post data on logging out.
   * This could be added by add_action('login_head'...) like wp_shake_js(),
   * but maybe better if it's not removable by plugins
   */
  if ( 'loggedout' == $wp_error->get_error_code() ) {
    ?>
    <script>if("sessionStorage" in window){try{for(var key in sessionStorage){if(key.indexOf("wp-autosave-")!=-1){sessionStorage.removeItem(key)}}}catch(e){}};</script>
    <?php
  }



  if ( is_multisite() ) {
    $login_header_url   = network_home_url();
    $login_header_title = get_current_site()->site_name;
  } else {
    $login_header_url   = __( 'https://wordpress.org/' );
    $login_header_title = __( 'Powered by WordPress' );
  }

 
  $login_header_url = apply_filters( 'login_headerurl', $login_header_url );
  
  $login_header_title = apply_filters( 'login_headertitle', $login_header_title );

  $classes = array( 'login-action-' . $action, 'wp-core-ui' );
  if ( wp_is_mobile() )
    $classes[] = 'mobile';
  if ( is_rtl() )
    $classes[] = 'rtl';
  if ( $interim_login ) {
    $classes[] = 'interim-login';
    ?>
    <style type="text/css">html{background-color: transparent;}</style>
    <?php

    if ( 'success' ===  $interim_login )
      $classes[] = 'interim-login-success';
  }
  $classes[] =' locale-' . sanitize_html_class( strtolower( str_replace( '_', '-', get_locale() ) ) );

  /**
   * Filter the login page body classes.
   *
   * @since 3.5.0
   *
   * @param array  $classes An array of body classes.
   * @param string $action  The action that brought the visitor to the login page.
   */
  //$classes = apply_filters( 'login_body_class', $classes, $action );

?>
<div id="login" class="custom_regForm">
  <?php
  unset( $login_header_url, $login_header_title );

  /**
   * Filter the message to display above the login form.
   *
   * @since 2.1.0
   *
   * @param string $message Login message text.
   */
  $message = apply_filters( 'login_message', $message );
  if ( !empty( $message ) )
    echo $message . "\n";

  // In case a plugin uses $error rather than the $wp_errors object
  if ( !empty( $error ) ) {
    $wp_error->add('error', $error);
    unset($error);
  }

  if ( $wp_error->get_error_code() ) {
    $errors = '';
    $messages = '';
    foreach ( $wp_error->get_error_codes() as $code ) {
      $severity = $wp_error->get_error_data( $code );
      foreach ( $wp_error->get_error_messages( $code ) as $error_message ) {
        if ( 'message' == $severity )
          $messages .= '  ' . $error_message . "<br />\n";
        else
          $errors .= '  ' . $error_message . "<br />\n";
      }
    }
    if ( ! empty( $errors ) ) {
      /**
       * Filter the error messages displayed above the login form.
       *
       * @since 2.1.0
       *
       * @param string $errors Login error message.
       */
      echo '<div id="login_error">' . apply_filters( 'login_errors', $errors ) . "</div>\n";
    }
    if ( ! empty( $messages ) ) {
      /**
       * Filter instructional messages displayed above the login form.
       *
       * @since 2.5.0
       *
       * @param string $messages Login messages.
       */
      echo '<p class="message">' . apply_filters( 'login_messages', $messages ) . "</p>\n";
    }
  }
} // End of login_header()

function login_footer($input_id = '') {
  global $interim_login;

  // Don't allow interim logins to navigate away from the page.
  /*
  if ( ! $interim_login ): ?>
  <p id="backtoblog"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php esc_attr_e( 'Are you lost?' ); ?>"><?php printf( __( '&larr; Back to %s' ), get_bloginfo( 'title', 'display' ) ); ?></a></p>
  <?php endif; */ ?>
  
  <?php if ( !empty($input_id) ) : ?>
  <script type="text/javascript">
  try{document.getElementById('<?php echo $input_id; ?>').focus();}catch(e){}
  if(typeof wpOnload=='function')wpOnload();
  </script>
  <?php endif; ?>

  <?php
  /**
   * Fires in the login page footer.
   *
   * @since 3.1.0
   */
  do_action( 'login_footer' ); ?>
  <div class="clear"></div>
  <?php
}

function wp_shake_js() {
  if ( wp_is_mobile() )
    return;
?>
<script type="text/javascript">
addLoadEvent = function(func){if(typeof jQuery!="undefined")jQuery(document).ready(func);else if(typeof wpOnload!='function'){wpOnload=func;}else{var oldonload=wpOnload;wpOnload=function(){oldonload();func();}}};
function s(id,pos){g(id).left=pos+'px';}
function g(id){return document.getElementById(id).style;}
function shake(id,a,d){c=a.shift();s(id,c);if(a.length>0){setTimeout(function(){shake(id,a,d);},d);}else{try{g(id).position='static';wp_attempt_focus();}catch(e){}}}
addLoadEvent(function(){ var p=new Array(15,30,15,0,-15,-30,-15,0);p=p.concat(p.concat(p));var i=document.forms[0].id;g(i).position='relative';shake(i,p,20);});
</script>
<?php
}


/**
 * Handles sending password retrieval email to user.
 *
 * @uses $wpdb WordPress Database object
 *
 * @return bool|WP_Error True: when finish. WP_Error on error
 */
function retrieve_password() {
  global $wpdb, $wp_hasher;

  $errors = new WP_Error();

  if ( empty( $_POST['user_login'] ) ) {
    $errors->add('empty_username', __('<strong>ERROR</strong>: Enter a username or e-mail address.'));
  } else if ( strpos( $_POST['user_login'], '@' ) ) {
    $user_data = get_user_by( 'email', trim( $_POST['user_login'] ) );
    if ( empty( $user_data ) )
      $errors->add('invalid_email', __('<strong>ERROR</strong>: There is no user registered with that email address.'));
  } else {
    $login = trim($_POST['user_login']);
    $user_data = get_user_by('login', $login);
  }

  /**
   * Fires before errors are returned from a password reset request.
   *
   * @since 2.1.0
   */
  do_action( 'lostpassword_post' );

  if ( $errors->get_error_code() )
    return $errors;

  if ( !$user_data ) {
    $errors->add('invalidcombo', __('<strong>ERROR</strong>: Invalid username or e-mail.'));
    return $errors;
  }

  // Redefining user_login ensures we return the right case in the email.
  $user_login = $user_data->user_login;
  $user_email = $user_data->user_email;

  /**
   * Fires before a new password is retrieved.
   *
   * @since 1.5.0
   * @deprecated 1.5.1 Misspelled. Use 'retrieve_password' hook instead.
   *
   * @param string $user_login The user login name.
   */
  do_action( 'retreive_password', $user_login );

  /**
   * Fires before a new password is retrieved.
   *
   * @since 1.5.1
   *
   * @param string $user_login The user login name.
   */
  do_action( 'retrieve_password', $user_login );

  /**
   * Filter whether to allow a password to be reset.
   *
   * @since 2.7.0
   *
   * @param bool true           Whether to allow the password to be reset. Default true.
   * @param int  $user_data->ID The ID of the user attempting to reset a password.
   */
  $allow = apply_filters( 'allow_password_reset', true, $user_data->ID );

  if ( ! $allow )
    return new WP_Error('no_password_reset', __('Password reset is not allowed for this user'));
  else if ( is_wp_error($allow) )
    return $allow;

  // Generate something random for a password reset key.
  $key = wp_generate_password( 20, false );

  /**
   * Fires when a password reset key is generated.
   *
   * @since 2.5.0
   *
   * @param string $user_login The username for the user.
   * @param string $key        The generated password reset key.
   */
  do_action( 'retrieve_password_key', $user_login, $key );

  // Now insert the key, hashed, into the DB.
  if ( empty( $wp_hasher ) ) {
    require_once ABSPATH . WPINC . '/class-phpass.php';
    $wp_hasher = new PasswordHash( 8, true );
  }
  $hashed = $wp_hasher->HashPassword( $key );
  $wpdb->update( $wpdb->users, array( 'user_activation_key' => $hashed ), array( 'user_login' => $user_login ) );

  $message = __('Someone requested that the password be reset for the following account:') . "\r\n\r\n";
  $message .= network_home_url( '/' ) . "\r\n\r\n";
  $message .= sprintf(__('Username: %s'), $user_login) . "\r\n\r\n";
  $message .= __('If this was a mistake, just ignore this email and nothing will happen.') . "\r\n\r\n";
  $message .= __('To reset your password, visit the following address:') . "\r\n\r\n";
  $message .= '<' . network_site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($user_login), 'login') . ">\r\n";

  if ( is_multisite() )
    $blogname = $GLOBALS['current_site']->site_name;
  else
    /*
     * The blogname option is escaped with esc_html on the way into the database
     * in sanitize_option we want to reverse this for the plain text arena of emails.
     */
    $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

  $title = sprintf( __('[%s] Password Reset'), $blogname );

  /**
   * Filter the subject of the password reset email.
   *
   * @since 2.8.0
   *
   * @param string $title Default email title.
   */
  $title = apply_filters( 'retrieve_password_title', $title );
  /**
   * Filter the message body of the password reset mail.
   *
   * @since 2.8.0
   *
   * @param string $message Default mail message.
   * @param string $key     The activation key.
   */
  $message = apply_filters( 'retrieve_password_message', $message, $key );

  if ( $message && !wp_mail( $user_email, wp_specialchars_decode( $title ), $message ) )
    wp_die( __('The e-mail could not be sent.') . "<br />\n" . __('Possible reason: your host may have disabled the mail() function.') );

  return true;
}

//
// Main
//

$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'login';
$errors = new WP_Error();

if ( isset($_GET['key']) )
  $action = 'resetpass';

// validate action so as to default to the login screen
if ( !in_array( $action, array( 'postpass', 'logout', 'lostpassword', 'retrievepassword', 'resetpass', 'rp', 'register', 'login' ), true ) && false === has_filter( 'login_form_' . $action ) )
  $action = 'login';

nocache_headers();

@header('Content-Type: '.get_bloginfo('html_type').'; charset='.get_bloginfo('charset'));

if ( defined( 'RELOCATE' ) && RELOCATE ) { // Move flag is set
  if ( isset( $_SERVER['PATH_INFO'] ) && ($_SERVER['PATH_INFO'] != $_SERVER['PHP_SELF']) )
    $_SERVER['PHP_SELF'] = str_replace( $_SERVER['PATH_INFO'], '', $_SERVER['PHP_SELF'] );

  $url = dirname( set_url_scheme( 'http://' .  $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] ) );
  if ( $url != get_option( 'siteurl' ) )
    update_option( 'siteurl', $url );
}

//Set a cookie now to see if they are supported by the browser.
$secure = ( 'https' === parse_url( site_url(), PHP_URL_SCHEME ) && 'https' === parse_url( home_url(), PHP_URL_SCHEME ) );
@setcookie( TEST_COOKIE, 'WP Cookie check', 0, COOKIEPATH, COOKIE_DOMAIN, $secure );
if ( SITECOOKIEPATH != COOKIEPATH )
  @setcookie( TEST_COOKIE, 'WP Cookie check', 0, SITECOOKIEPATH, COOKIE_DOMAIN, $secure );

/**
 * Fires when the login form is initialized.
 *
 * @since 3.2.0
 */
do_action( 'login_init' );
/**
 * Fires before a specified login form action.
 *
 * The dynamic portion of the hook name, $action, refers to the action
 * that brought the visitor to the login form. Actions include 'postpass',
 * 'logout', 'lostpassword', etc.
 *
 * @since 2.8.0
 */
do_action( 'login_form_' . $action );

$http_post = ('POST' == $_SERVER['REQUEST_METHOD']);
$interim_login = isset($_REQUEST['interim-login']);

switch ($action) {

case 'register' :
default:
  if ( is_multisite() ) {
    /**
     * Filter the Multisite sign up URL.
     *
     * @since 3.0.0
     *
     * @param string $sign_up_url The sign up URL.
     */
    wp_redirect( apply_filters( 'wp_signup_location', network_site_url( 'wp-signup.php' ) ) );
    exit;
  }

  if ( !get_option('users_can_register') ) {
    wp_redirect( site_url('wp-login.php?registration=disabled') );
    exit();
  }

  $user_login = '';
  $user_email = '';
  if ( $http_post ) {
    $user_login = $_POST['user_login'];
    $user_email = $_POST['user_email'];
	
	
	
	
	
    $errors = register_new_user($user_login, $user_email);
	
	
	
	
    if ( !is_wp_error($errors) ) {
    	
		
		
		
		
		//echo custom_login($_POST['user_login'],$_POST['ws_plugin__s2member_custom_reg_field_user_pass1']);
		
      $redirect_to = !empty( $_POST['redirect_to'] ) ? $_POST['redirect_to'] : 'wp-login.php?checkemail=registered';
	  //die('goig to redirect');
      //wp_safe_redirect( $redirect_to );
      //exit();
    }
  }

  $registration_redirect = ! empty( $_REQUEST['redirect_to'] ) ? $_REQUEST['redirect_to'] : '';
  /**
   * Filter the registration redirect URL.
   *
   * @since 3.0.0
   *
   * @param string $registration_redirect The redirect destination URL.
   */
  $redirect_to = apply_filters( 'registration_redirect', $registration_redirect );
  login_header(__('Registration Form'), '<h2 class="Regheading">' . __('Register For This Site') . '</h2>', $errors);
?>
<div class="innerRegform">
<form name="registerform" id="registerform" action="<?php echo esc_url( site_url('/register', 'login_post') ); ?>" method="post" novalidate>
  <p>
    <label for="user_login"><?php _e('Username') ?><br />
    <input type="text" name="user_login" id="user_login" class="input form-control" value="<?php echo esc_attr(wp_unslash($user_login)); ?>" size="20" /></label>
  </p>
  <p>
    <label for="user_email"><?php _e('E-mail') ?><br />
    <input type="email" name="user_email" id="user_email" class="input form-control" value="<?php echo esc_attr( wp_unslash( $user_email ) ); ?>" size="25" /></label>
  </p>
  <?php
  /**
   * Fires following the 'E-mail' field in the user registration form.
   *
   * @since 2.1.0
   */
  do_action( 'register_form' );
  ?>
  <!-- <p id="reg_passmail"><?php _e('A password will be e-mailed to you.') ?></p> -->
  <br class="clear" />
  <input type="hidden" name="redirect_to" value="<?php echo esc_attr( $redirect_to ); ?>" />
  <p class="submit"><input type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large btn btn-theme btn-block" value="<?php esc_attr_e('Register'); ?>" /></p>
</form>
</div><!--.innerRegform-->
<!--
<p id="nav">
<a href="<?php echo esc_url( wp_login_url() ); ?>"><?php _e( 'Log in' ); ?></a> |
<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>" title="<?php esc_attr_e( 'Password Lost and Found' ) ?>"><?php _e( 'Lost your password?' ); ?></a>
</p>
-->
<?php
login_footer('user_login');
break;
} ?>
</div><!--//#login-->
<?php //wp_footer(); ?>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.backstretch.min.js"></script>
<script>
jQuery(window).load(function(){
  $( document ).tooltip();
var i = 0;
	$("form#registerform input").each(function(){
	//	alert($(this).attr('newtitle'));
	    if($(this).attr('newtitle')=="" || $(this).attr('newtitle')===undefined){
	      
	    }else{
	      $(this).prev().prev().append("<span style='color:#428bca; font-style:italic;' title='"+$(this).attr('newtitle')+"'> What's this?</span>");
	    }
		i++;
		$(this).attr("tabindex", i);
	});	
});
jQuery.backstretch("<?php bloginfo('template_url'); ?>/img/login-bg.jpg", {speed: 500});
$(function() {
  //alert('asdf');
$( document ).tooltip();

	
// Add Line in form

$("#ws-plugin--s2member-custom-reg-field-company-name").parent().after('<p><strong>When creating a proposal I:</strong></p>')

	
//add checkbox
	$("#ws-plugin--s2member-custom-reg-field-hardgood-multiple").hide();
	$("#ws-plugin--s2member-custom-reg-field-hardgood-multiple").val('1');
	$("#ws-plugin--s2member-custom-reg-field-hardgood-multiple").prev().prev().children().before('<input type="checkbox" class="checkboxRegistration" id="forHardgood">');

	$("#ws-plugin--s2member-custom-reg-field-fresh-flower-multiple").hide();
	$("#ws-plugin--s2member-custom-reg-field-fresh-flower-multiple").val('1');
	$("#ws-plugin--s2member-custom-reg-field-fresh-flower-multiple").prev().prev().children().before('<input type="checkbox" class="checkboxRegistration" id="forFreshFlower">');

	$("#ws-plugin--s2member-custom-reg-field---charge-card-rate").hide();
	$("#ws-plugin--s2member-custom-reg-field---charge-card-rate").val('0');
	$("#ws-plugin--s2member-custom-reg-field---charge-card-rate").prev().prev().children().before('<input type="checkbox" class="checkboxRegistration" id="forChangeCard">');

	$("#ws-plugin--s2member-custom-reg-field-sales-tax--").hide();
	$("#ws-plugin--s2member-custom-reg-field-sales-tax--").val('0');
	$("#ws-plugin--s2member-custom-reg-field-sales-tax--").prev().prev().children().before('<input type="checkbox" class="checkboxRegistration" id="forSalesTax">');

	$("#ws-plugin--s2member-custom-reg-field---delivery-cost").hide();
	$("#ws-plugin--s2member-custom-reg-field---delivery-cost").val('0');
	$("#ws-plugin--s2member-custom-reg-field---delivery-cost").prev().prev().children().before('<input type="checkbox" class="checkboxRegistration" id="forDeliveryCost">');

	$("#ws-plugin--s2member-custom-reg-field---labor").hide();
	$("#ws-plugin--s2member-custom-reg-field---labor").val('0'); 
	$("#ws-plugin--s2member-custom-reg-field---labor").prev().prev().children().before('<input type="checkbox" class="checkboxRegistration" id="forLabor">');

	$("#forHardgood").click(function(){
		if($('#forHardgood').prop("checked")==true){
			$("#ws-plugin--s2member-custom-reg-field-hardgood-multiple").show();
		}else{
			$("#ws-plugin--s2member-custom-reg-field-hardgood-multiple").hide();
			$("#ws-plugin--s2member-custom-reg-field-hardgood-multiple").val('1');
		}
	});

	$("#forFreshFlower").click(function(){
		if($('#forFreshFlower').prop("checked")==true){
			$("#ws-plugin--s2member-custom-reg-field-fresh-flower-multiple").show();
		}else{
			$("#ws-plugin--s2member-custom-reg-field-fresh-flower-multiple").hide();
			$("#ws-plugin--s2member-custom-reg-field-fresh-flower-multiple").val('1');
		}
	});

	$("#forChangeCard").click(function(){
		if($('#forChangeCard').prop("checked")==true){
			$("#ws-plugin--s2member-custom-reg-field---charge-card-rate").show();
		}else{
			$("#ws-plugin--s2member-custom-reg-field---charge-card-rate").hide();
			$("#ws-plugin--s2member-custom-reg-field---charge-card-rate").val('0');
		}
	});

	$("#forSalesTax").click(function(){
		if($('#forSalesTax').prop("checked")==true){
			$("#ws-plugin--s2member-custom-reg-field-sales-tax--").show();
		}else{
			$("#ws-plugin--s2member-custom-reg-field-sales-tax--").hide();
			$("#ws-plugin--s2member-custom-reg-field-sales-tax--").val('0');
		}
	});

	$("#forDeliveryCost").click(function(){
		if($('#forDeliveryCost').prop("checked")==true){
			$("#ws-plugin--s2member-custom-reg-field---delivery-cost").show();
		}else{
			$("#ws-plugin--s2member-custom-reg-field---delivery-cost").hide();
			$("#ws-plugin--s2member-custom-reg-field---delivery-cost").val('0')
		}
	});

	$("#forLabor").click(function(){
		if($('#forLabor').prop("checked")==true){
			$("#ws-plugin--s2member-custom-reg-field---labor").show();
		}else{
			$("#ws-plugin--s2member-custom-reg-field---labor").hide();
			$("#ws-plugin--s2member-custom-reg-field---labor").val('0')
		}
	});
});
</script>
</body>
</html>