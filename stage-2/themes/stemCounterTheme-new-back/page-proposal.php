<?php get_header(); ?>
<?php get_sidebar('event'); ?>

<!-- On Page PHP -->
<!--Fix!!!!-->
<?php 
	/* Call WP's "get_results" on your query and create the array */
	global $wpdb;
	
	/*Get User Multiple*/
	//USER SECURITY
	$user_ID = get_current_user_id();
	$user_meta = get_user_meta($user_ID, 'wp_s2member_custom_fields');
	
	//Defining the invoice factors. If they are empty, defining them as standard. This protection eventually needs to be moved to the profile page.
	$hardgood_multiple = $user_meta[0]['hardgood_multiple'];
	if(!empty($hardgood_multiple)){
		$hardgood_multiple; } else { $hardgood_multiple = 1;}
	
	$flower_multiple = $user_meta[0]['fresh_flower_multiple'];
	if(!empty($flower_multiple)){
		$flower_multiple; } else { $flower_multiple = 1;}
		
	$sales_tax = $user_meta[0]['sales_tax__'];
	if(!empty($sales_tax)){
		$sales_tax; } else { $sales_tax = 0;}
		
	$card_rate = $user_meta[0]['__charge_card_rate'];
	if(!empty($card_rate)){
		$card_rate; } else { $card_rate = 0;}
	
	$delivery_cost = $user_meta[0]['__delivery_cost'];
	if(!empty($delivery_cost)){
		$delivery_cost; } else { $delivery = 0;}
	
	$labor_cost = $user_meta[0]['__labor'];
	if(!empty($labor_cost)){
		$labor_cost; } else { $labor_cost = 0;}
	
	$user_events_ID = $_GET['eid'];

  $replace_zip=$current_user->wp_s2member_custom_fields; //Get User Details

	//LOAD EVENT DETAILS
	//FIX!! WE WANT THIS EVENT ID TO PULL FROM THE BROWSER
	$event_ID = "45";
	$event_details = $wpdb->get_results("SELECT * FROM `wp_user_events` WHERE user = $user_ID AND event_id = $user_events_ID") or die(mysql_error());
	//echo '<pre>'; print_r($event_details);
	foreach($event_details as $event_detail){
		$event_info = $event_detail->details;
		$event_info_json = json_decode($event_info);
    $event_name = $event_info_json->event_name;
    //print_r($event_info_json); exit();
		$date = $event_detail->event_date;
    $toPhone = $event_info_json->contact_info->phone;
    $toEmail = $event_info_json->contact_info->email; 
		$contact = $event_info_json->firstName . " " . $event_info_json->lastName;
		$venue = $event_info_json->Venue;
		$status = $event_info_json->status; ?>

  <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">

<div class="container content-panel">
      <div class="row">
        <div class="col-md-9">
          <div class="col-md-3"></div>
          <div class="col-md-3">
          
          
          
          <!--FIX!!! No logo in the first version-->
          <!--
          <h1>
            <a href="https://TwistedWillow.co">
            <img src="../assets/img/logo.png">
            </a>
          </h1>-->
          </div>
          
        </div>
          <div class="col-md-3 text-right">
          <h1>PROPOSAL</h1>
          

          <!--ADD PROPOSAL NUMBER LATER
          <h1><small>Invoice #001</small></h1>-->
          
          
        </div>
      </div>
      <div class="row">
        <div class="col-xs-5">
          <div class="panel panel-default">
            <div class="panel-heading">
           
            <!--Fix!!!!-->
            <h4>From: <a href="<?php echo get_bloginfo('url')."/stem_counter/?eid=".$user_events_ID; ?>"><?php echo $replace_zip[company_name]; ?></a></h4>
            </div>
            <div class="panel-body">
              
              <!--Fix!!!!-->
              <p>
                <?php echo $replace_zip[company_address]; ?> <br>
                <!-- Website: http://TwistedWillow.co <br>
                Email : rachael@twistedwillow.co <br><br> -->
                Mobile : <?php echo $additionalInfo[companyMobile]; ?> <br> <br>
                <!-- Instagram : <a href="https://instagram.com/TwistedWillowDesign">@TwistedWillowDesign</a> -->
                </p>
                
              </p>
            </div>
          </div>
        </div>
        <div class="col-xs-5 col-xs-offset-2 text-right">
          <div class="panel panel-default">
            <div class="panel-heading">
              
              <!--Fix!!!!-->
              <h4>To : <a href="#"><?php echo $contact; ?></a></h4>
            </div>
            <div class="panel-body">
              
              <!--Fix!!!!-->
              <p>
               <strong>Email:</strong> <?php echo $toEmail; ?> <br>
               <strong>Phone:</strong> <?php echo $toPhone; ?> <br>
              </p>
            </div>
          </div>
        </div>
      </div>
      <!-- / end client details section -->
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>
              <h4>Product/Service</h4>
            </th>
            <th>
              <h4>Description</h4>
            </th>
            <th>
              <h4>Qty</h4>
            </th>
            <th>
              <h4>Price</h4>
            </th>
            <th>
              <h4>Sub Total</h4>
            </th>
          </tr>
        </thead>
        <tbody>
			
           <?php
            /* Run a loop as usual on your array to create a table of events*/
				$arrangements = json_decode($event_detail->event_order);
				//print_r($arrangements);
				foreach($arrangements as $arrangement){
				?>
					<tr>
                    <td><?php echo $arrangement->arrangement;?></td>
					<td>
					<?php
				
					
					//Loop through all of the flowers names to list them in the proposal
					$items = $arrangement->items;
				
					$cont_var = get_object_vars($items);
					$cont_var = count($cont_var);
					
					$i = 0;
				  	foreach($items as $item => $details){
				  		$iType = $details->type;
						if($iType == "flower"){
              if(!empty($flower_multiple)){
							   $price += $details->qty * $details->cost * $flower_multiple;
              }
						}elseif($iType == "hardgood"){
              if(!empty($hardgood_multiple)){
							   $price += $details->qty * $details->cost * $hardgood_multiple;
              }
						}else{
                $price += $details->qty * $details->cost;
            } 
			
						//Add items as the loop goes through
						$i++;
					?> 
            		<?php /*?><a href="#"><?php echo $item; ?></a><?php if($i<$cont_var){ echo ","; } ?><?php */?>
					<?php 
					} 
					?>
                    <span> <?php echo $arrangement->arrangement_note; ?>
                   	</td>
                    <td class="text-right"><?php echo $arrangement->qty; ?></td>
                    <!-- Multiply the price by delivery cost and credit card -->
                    <td class="text-right"><?php $priceNew = $price + $price*($card_rate/100) + $price*($labor_cost/100); echo number_format($priceNew, 2, '.', ','); ?>	</td>
                    <td class="text-right"><?php $item_total = $arrangement->qty * $priceNew; echo number_format($item_total, 2, '.', ','); ?> </td>
                  	</tr>
                    </div>
                    <?php 
					//Reset price then end arrangement loop
					$price = 0;
					$subtotal += $item_total;
					}
					}
					?>
		
        </tbody>
      </table>
      <div class="row text-right">
        <div class="col-xs-2 col-xs-offset-8">
          <p>
            <strong>
            <?php
			/*Building labor into arrangements*/
            /*
			if(!empty($labor_cost)){
              echo 'Labor : <br>';
            }*/
			
	
            if(!empty($delivery_cost)){
              echo 'Delivery : <br>';
            }

            echo 'Sub Total : <br>';

            if(!empty($sales_tax)){
              echo 'TAX : <br>';
            }

            echo 'Total : <br><br>';

            /*if(!empty($card_rate)){
              echo 'Paying by credit card? Pay this:<br>';
            }*/
            ?>
            <!-- Delivery : <br>
            Sub Total : <br>
            TAX : <br>
            Total : <br><br>
            Paying by credit card? Pay this:<br> -->
            </strong>
          </p>
        </div>
        <div class="col-xs-2">
          <strong>
          <?php
          /*
		  if(!empty($labor_cost)){
            $labor_int = $subtotal*($labor_cost/100);
      		  $labor = number_format($labor_int, 2, '.', ',');
      		  echo $labor.'<br>';
          } else{
            $labor_int = 0;
          } */
          ?>	
          <?php /*echo number_format($subtotal, 2, '.', ',');*/ ?>
          <!-- ADD TAX RATE -->
          <?php
	
		  if(!empty($delivery_cost)){
            $delivery_int = $subtotal*($delivery_cost/100);
      		  $delivery = number_format($delivery_int, 2, '.', ',');
      		  echo $delivery.'<br>';
          } else {
            $delivery_int = 0;
          } 
          /*echo  $subtotal*($sales_tax/100);
          $taxes = number_format($subtotal * 0.0813, 2, '.',','); echo $taxes;*/ 
          ?> 
          <?php 
          $grand_totalInt = $subtotal + $labor_int+ $delivery_int;
          $grand_total = number_format($grand_totalInt, 2, '.',','); 
          echo "$".$grand_total;?><br>
          <?php 
          if(!empty($sales_tax)){
            $tax_int = $grand_totalInt*($sales_tax/100);
      		  $tax = number_format($tax_int,2, '.',',');
      		  echo $tax.'<br>'; 
          } else {
            $tax_int = 0; 
          }
          ?>
          <?php
          $final_total_int = $grand_totalInt+ $tax_int;
		  $final_total = number_format($final_total_int,2, '.',',');
		  echo $final_total;
          ?><br><br>
          <?php
          /*if(!empty($card_rate)){
            $card_cc_int =  $final_total_int*($card_rate/100);
  		      $card_cc = number_format($card_cc_int,2, '.',',');
		      } else {
            $card_cc_int = 0;
          }

		  $after_card_int = $final_total_int+$card_cc_int;
		  $after_card = number_format($after_card_int,2, '.',',');
		  echo $after_card;*/
          ?>
          </strong>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-5">
          <div class="panel panel-info">
            <div class="panel-heading">
              <h4>Event details</h4>
            </div>
            
            <div class="panel-body">
              <p>Venue: <?php echo $venue; ?></p>
              <p>Date: <?php echo $date; ?></p>
              
              <!--FIXX!!! ADD ANY OTHER DETAILS HERE AS NEEDED. ADD COLORS LATER.
              <p>Colors: Pink, Green</p>
              -->
              
            </div>
          </div>
        </div>
        <div class="col-xs-7">
          <div class="span7">
            <div class="panel panel-info">
              <div class="panel-heading">
                <h4>Contract Specifics</h4>

              </div>
              <div class="panel-body">
                <p>
                These are the disclaimers that they need to know. Invoice was created on a computer and is valid without the signature and seal.
                </p>
                <h4>Payment should be made via the invoice</h4>
              </div>
            </div>
          </div>
        </div>
      <a href="javascript:void(0);" id="clickPdf">Print PDF</a>
      </div>
    </div>
         </div>




		</section><!--/wrapper -->
      </section><!-- /MAIN CONTENT -->
      <div style="display:none;" id="canvas">
    <p>Canvas:</p>
    </div>
<div style="display:none;" id="image"></div>
<?php
$up_dir = wp_upload_dir();
$save_dir = $up_dir['basedir']."/ev_proposal/";
$event_name = str_replace(' ', '_', the_xt_event($user_events_ID, $user_ID));
?>

<!--main content end-->
<script src="http://html2canvas.hertzen.com/build/html2canvas.js"></script>
<script>

html2canvas([document.getElementById('main-content')], {
    onrendered: function (canvas) {
        document.getElementById('canvas').appendChild(canvas);
        var data = canvas.toDataURL('image/png');
        // AJAX call to send `data` to a PHP file that creates an image from the dataURI string and saves it to a directory on the server

        var image = new Image();
        image.src = data;
        document.getElementById('image').appendChild(image);
    }
});
$(document).ready(function(){
$("#clickPdf").click(function(){
//var pro_con  = "<?php echo $user_events_ID; ?>";
var pro_con = $("#image img").attr('src');
jQuery.ajax({
	type:"post",
	url: "<?php bloginfo('url'); ?>/wp-admin/admin-ajax.php",
	data: {action: 'gen_pdf', con:pro_con, uid:'<?php echo $user_ID; ?>', ename:'<?php echo $event_name; ?>'},
	success:function(edata){
		if(edata=="Success"){
      var w = window.open("<?php bloginfo('url'); ?>/wp-content/uploads/ev_proposal/<?php echo $event_name; ?>_<?php echo $user_ID; ?>_proposal.pdf");
      w.print();
      //window.open("<?php bloginfo('url'); ?>/download.php?file=<?php echo $save_dir; ?><?php echo $event_name; ?>_<?php echo $user_ID; ?>_proposal.pdf");
    }
    else{
      alert('Error');
    }
	}
	});
});
	
});
</script>
<?php get_footer(); ?>