<?php get_header(); ?>
<?php get_sidebar(); ?>

<!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      
      
      
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
          	<div class="row mt">
          		<div class="content-panel">
                <div class="col-lg-12">
                
             
		   		<p><a name="top"></a></p>
<h3 style="text-align: center;"><strong>FAQ’s of StemCounter.com</span></strong></h3>
<h4><strong><a href="#FAQ1">What is StemCounter.com?</a></span></strong></h4>
<h4><strong><a href="#FAQ2">How do you add new hardgoods?</a></span></strong></h4>
<h4><strong><a href="#FAQ3">How do you add color options to the flowers?</a></span></strong></h4>

<h3><strong><a name="FAQ1"></a>What is StemCounter.com?</span></strong></h3>
<p>StemCounter.com saves you time and money. No longer do you have to mess with long quote times and complex spreadsheets. This software does all the stem counting for you. Before you get up from consulting with a bride, you can have a completely finished and accurate quote that she can sign right there on the spot.<a href="#top">top</a></p>

<p><iframe width="560" height="315" src="//player.vimeo.com/video/117995239" frameborder="0" allowfullscreen></iframe></p>

<h3><strong><a name="FAQ2"></a>How do you add new hardgoods?</span></strong></h3>
<p>You'll want to go to Hardgoods->Add hardgood <a href="#top">top</a></p>

<h3><strong><a name="FAQ3"></a>How do you add color options to the flowers?</span></strong></h3>
<p>When adding a new flower, you can write in the different color options for the flowers. For multiple options, separate each color with a comma. <a href="#top">top</a></p>
			</div>
           </div>
          	</div>
			
		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->

<?php get_footer(); ?>