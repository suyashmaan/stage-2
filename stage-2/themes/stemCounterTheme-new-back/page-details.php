<?php get_header(); ?>
<?php get_sidebar('event'); ?>

<!-- On Page PHP -->
<!--Fix!!!!-->
<!--< ? php 
	/* Call WP's "get_results" on your query and create the array */
	global $wpdb;
	
	//USER SECURITY
   $user_ID = get_current_user_id(); 
	
	//LOAD EVENT DETAILS
	//FIX!! WE WANT THIS EVENT ID TO PULL FROM THE BROWSER
	$event_ID = "1001";
	$event_details = $wpdb->get_results("SELECT * FROM sc_events WHERE user = ".$user_ID." AND event = ".$event_ID) or die(mysql_error()); 
	foreach($event_details as $event_detail){
		$event_info = $event_detail->details;
		$event_info_json = json_decode($event_info);
		$event_name = $event_info_json->event_name;
		$date = $event_detail->event_date;
		$contact = $event_info_json->contact->first_name . " " . $event_info_json->contact->last_name;
		$venue = $event_info_json->venue;
		$status = $event_info_json->status; ?>-->
     <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
<div class="modal fade" id="genInfoPop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title" id="myModalLabel">Edit General Information</h4>
			</div>
			<div class="modal-body" id="setGenForm">
				
			</div>
		</div>
	</div>     
</div><!--pop up Genrel information-->

<div class="modal fade" id="venueInfoPop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title" id="myModalLabel">Edit Venue Information</h4>
			</div>
			<div class="modal-body" id="setVenuForm">
				
			</div>
		</div>
	</div>     
</div><!--pop up Venue information-->

	<!--main content start-->
      <section id="main-content">
      <?php
      global $wpdb;
      $uid = get_current_user_id();
	  $eid = $_GET['eid'];
	  $event_name = the_xt_event($eid, $uid);
	  $event_date = the_event_date($eid, $uid);
	  $event_date = explode("-", $event_date);
	  $edate = $event_date[1]."/".$event_date[2]."/".$event_date[0];
	  $edtl =  $wpdb->get_results("SELECT details FROM wp_user_events WHERE user = ".$uid." AND event_id = ".$eid);
      ?>	
          <section class="wrapper">
          	<!--<h3><i class="fa fa-angle-right"></i> Event Details</h3>-->
          	<!-- BASIC FORM ELELEMNTS -->
          	<?php 
          	//print_r($edtl);
			$ed = $edtl[0]->details;
			$ed = json_decode($ed);
			$venue = $ed->Venue;
			$address = $ed->address;
			$fname = $ed->firstName;
			$lname = $ed->lastName;
			$cinfo = $ed->contact_info;
			$phone = $cinfo->phone;
			$email = $cinfo->email;
			?>
          	<div class="row mt">
                <div class="col-lg-12">
                  <div class="form-panel">
                  <div class="col-lg-2">	
                  	<h4 class="mb"><i class="fa fa-angle-right"></i> General Information</h4>
                  </div>	  
                  <button class="btn btn-primary btn-lg pull-right" data-toggle="modal" data-target="#genInfoPop">Edit Info</button>
                  <div class="clearfix"></div>
                  	  <div id="genForm">
                      <form class="form-horizontal style-form" onsubmit="return no_redirect();">
                      <fieldset>
                      <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Event Name</label>
                              <div class="col-sm-10">
                              <span type="text" placeholder="e.g. Johnson Wedding" id="ev_name"> <?php echo $event_name; ?> <span>
                              </div>
                             
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Date</label>
                              <div class="col-sm-2">
                              <span type="text"  placeholder="MM/DD/YYYY" id="ev_date"><?php echo $edate; ?></span>
                              </div>
                          </div>
                          
                          <!--Bride's Info-->
                         
                          <div class="form-group col-sm-2">
                              <label class="control-label">Contact First Name</label>
                              <span type="text" id="fname"><?php echo $fname; ?></span>
                              
                          </div>
                          <div class="form-group col-sm-2" style="margin-left:2%">
                              <label class="control-label">Contact Last Name</label>
                              <span type="text" id="lname"><?php echo $lname; ?></span>
                              
                              </div>
                              
                          <div class="form-group col-sm-2" style="margin-left:2%">
                              <label class="control-label">Phone Number</label>
                              <span type="text" id="phone" <?php echo $phone; ?></span>
                              
                          </div>
                          <div class="form-group col-sm-2" style="margin-left:2%">
                              <label class="control-label">Email Address</label>
                              <span type="text" id="email"><?php echo $email; ?></span>
                          </div>
                        </fieldset>
                      <input type="submit" value="Edit Info" class="btn btn-primary" />  
                      </form>
                     </div><!--#genForm-->
              	</div>
          		</div><!--  col-lg-6-->      	
          	</div><!-- /row -->
          	
         <!-- VENUE -->
          	<div class="row">
                <div class="col-lg-12">
                  <div class="form-panel">
                  <div class="col-lg-2">	
                  	<h4 class="mb"><i class="fa fa-angle-right"></i> Venue</h4>
                  </div>	
                  	<button class="btn btn-primary btn-lg pull-right" data-toggle="modal" data-target="#venueInfoPop">Edit Info</button>
                  	<div class="clearfix"></div>
                  	<div id="getVenueForm">
                    <form class="form-horizontal style-form" onsubmit="return no_redirect();">
                     <fieldset>
                     
                     <div class="form-group">
                          <label class="col-sm-2 col-sm-2 control-label">Venue</label>
	                      <div class="col-sm-10">
	                      <span type="text" placeholder="e.g. The Coronado" id="venue"><?php echo $venue; ?></span>
	                      </div>
                     </div>
                     
					<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Address</label>
						<div class="col-sm-10">
						<span type="text" placeholder="Address" id="address"><?php echo $address; ?></span>
						</div>
					</div>
                <input type="submit" value="Edit Info" class="btn btn-primary" />    
                </form>
                </div><!--#getVenueForm-->
              	</div>
          		</div><!--col-lg-6-->      	
          	</div><!-- /row -->
		</section><!--/wrapper -->
      </section><!-- /MAIN CONTENT -->
      <!--main content end-->
<script>
$(document).ready(function(){
var genForm = $("#genForm").html();
var venForm = $("#getVenueForm").html();
$("#setGenForm").html(genForm);
$("#setVenuForm").html(venForm);
$("#setGenForm form").attr('onsubmit', 'return general_form();');
$("#setVenuForm form").attr('onsubmit', 'return venue_info();');

$("#genForm form input, #getVenueForm form input").each(function(){
	//$(this).attr('readonly','readonly');
	$(this).attr('disabled','disabled');
	//disabled="disabled"
});

});

var eid = "<?php echo $eid; ?>";
var uid = "<?php echo $uid; ?>";
function general_form(){
var ename = $("#ev_name").val();
var edate = $("#ev_date").val();
var fname = $("#fname").val();
var lname = $("#lname").val();
var phone = $("#phone").val();
var email = $("#email").val();
jQuery.ajax({
			type:"post",
			url: "<?php bloginfo('url'); ?>/wp-admin/admin-ajax.php",
			data: {action: 'user_gen_info', eid:eid, uid:uid, ename:ename, edate:edate, fname:fname, lname:lname, phone:phone, email:email},
			success:function(edata){
				if(edata == "No Changes in Record"){
					//alert(edata);	
				}else{
					//alert(edata);
					location.reload();
				}
			}
			});
			
	return false;
}

function venue_info(){
var venue = $("#venue").val();
var address = $("#address").val();
jQuery.ajax({
			type:"post",
			url: "<?php bloginfo('url'); ?>/wp-admin/admin-ajax.php",
			data: {action: 'user_venue_info', eid:eid, uid:uid, venue:venue, address:address},
			success:function(edata){
				if(edata == "No Changes in Record"){
					//alert(edata);	
				}else{
					//alert(edata);
					location.reload();
				}
			}
			});
	
	return false;
}
function no_redirect(){
	return false;
}

$(function() {
$("#ev_date").datepicker();
});
</script>      
<?php get_footer(); ?>