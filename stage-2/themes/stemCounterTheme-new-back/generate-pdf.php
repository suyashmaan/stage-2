<?php
/*Function Used for Genrate PDF of event Proposal */
function gen_pdf(){
	$up_dir = wp_upload_dir();
	$save_dir = $up_dir['basedir']."/ev_proposal/";
	$con = $_POST['con'];
	$uid = $_POST['uid'];
	$ename = $_POST['ename'];
	require('fpdf.php');
	$pdf = new FPDF('P','mm','A3');
	$pdf->AddPage('L');
	$pdf->SetLeftMargin(200);
	$pdf->SetFont('Arial','B',16);
	$pdf->Image("$con",0,0,425,250,'PNG');
	//$pdf->Cell(40,10,$con);
	$pdf->Output($save_dir.$ename.'_'.$uid.'_proposal.pdf','F');
	echo 'Success';
	die;
}
add_action('wp_ajax_gen_pdf', 'gen_pdf');
add_action('wp_ajax_nopriv_gen_pdf', 'gen_pdf');
?>