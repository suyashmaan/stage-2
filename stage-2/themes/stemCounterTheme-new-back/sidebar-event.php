<!--sidebar start-->
<aside class="___Sidebar_Event">
	<div id="sidebar"  class="nav-collapse ">
		<!-- sidebar menu start-->
		<ul class="sidebar-menu" id="nav-accordion">
			<!--<p class="centered"><img src="/img/ui-tw.jpg" class="img-circle" width="60"></p>
			<h5 class="centered">Marshall - Hendricks Wedding</h5>-->
			<li class="sub-menu">
			<a href="<?php bloginfo('url'); ?>/events"> <i class="fa fa-angle-left"></i> <span>Back To Events</span> </a>
			</li>
			<li class="sub-menu">
				<?php
				if (is_page('stem_counter')) {
					//echo "<a class=\"active\" href=\"../stem_counter\">";
					echo "<a class=\"active\" href=\"../stem_counter/?eid=".$_GET['eid']."\">";
				} else {
					echo "<a href=\"../stem_counter/?eid=".$_GET['eid']."\">";
				}
				?>
				<i class="fa fa-desktop"></i>
				<span>StemCounter</span>
				</a>
			</li>
			<li class="sub-menu">
			<?php
			if (is_page('details')) {
					echo "<a class=\"active\" href=\"../details/?eid=".$_GET['eid']."\">";
					//echo "<a class=\"active\" href=\"../details\">";
				} else {
					echo "<a href=\"../details/?eid=".$_GET['eid']."\">";
				}
				?>
				<i class="fa fa-book"></i>
				<span>Event Details</span>
			</a>
			</li>

			<li class="sub-menu">
				<?php
				if (is_page('proposal')) {
					echo "<a class=\"active\" href=\"../proposal/?eid=".$_GET['eid']."\">";
					//echo "<a class=\"active\" href=\"../proposal\">";
				} else {
					echo "<a href=\"../proposal/?eid=".$_GET['eid']."\">";
				}
				?>
				<i class="fa fa-barcode"></i>
				<span>Proposal</span>
				</a>
			</li>

			<!-- <li class="sub-menu">
				<?php
				if (is_page('Shopping')) {
					echo "<a class=\"active\" href=\"../shopping\">";
				} else {
					echo "<a href=\"../shopping/?eid=".$_GET['eid']."\">";
				}
				?>
				<i class="fa fa-check"></i>
				<span>Shopping List</span>
				</a>
			</li> -->
			<!-- sidebar menu end-->
	</div>
</aside>
<!--sidebar end-->