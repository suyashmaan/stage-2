<?php get_header(); ?>
<?php get_sidebar(); ?>
<!--MAIN CONTENT-->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<!--<h3><i class="fa fa-angle-right"></i> My Hardgoods</h3>-->

<div class="col-lg-2">
<!--<h3><i class="fa fa-angle-right"></i>My Hardgoods</h3>-->
</div>
<div class="col-lg-10">
<button class="btn btn-primary btn-lg pull-right" data-toggle="modal" data-target="#addHardgoods" style="margin-top: 10px; margin-bottom:10px;">
Add Hardgood</button>
</div>
        
              <div class="row mt">
                  <div class="col-md-12">
                      <div class="content-panel">
                          <table class="table table-striped table-advance table-hover">
                              <thead>
                              <tr>
                                  <th>Hardgood</th>
                                  <th>Description</th>
                                  <th>Number Available</th>
                                  <th>Cost Per</th>
                                  <th>Type</th>
                                  <th></th>
                              </tr>
                              </thead>
                              <tbody>
							<?php
							$user_id = get_current_user_id();
							$hardgoods = $wpdb->get_results("SELECT * FROM `wp_user_hardgood` WHERE user_id = $user_id", ARRAY_A); // get user hardgoods
							if($hardgoods) {
							$i = 0;
							foreach ($hardgoods as $row){ ?>
							<tr class="row_hardgood_<?php echo $i; ?>">
								<td><a><?php echo $row['user_item']; ?></a></td>
								<td class="hidden-phone"><?php echo $row['user_description']; ?></td>
								<td><?php echo $row['user_item_avl']; ?></td>
								<td><?php echo $row['user_cost']; ?></td>
								<td><?php echo "Hardgood"; /*$row['user_hardgood_type'];*/ ?></td>
								<td>
								<button class="btn btn-primary btn-xs" onclick="return edit_hardgood(<?php echo $row[item_id]; ?>,'<?php echo $row[user_item]; ?>','<?php echo $row[user_description]; ?>','<?php echo $row[user_item_avl]; ?>','<?php echo $row[user_cost]; ?>','<?php echo $row[user_item_type]; ?>');" hardgoodID="<?php echo $row[item_id]; ?>" data-toggle="modal" data-target="#editHardgoods">
									<i class="fa fa-pencil"></i>
									<input type="hidden" class="myHardgood" value="<?php echo $row['user_hardgood']; ?>" />
									<input type="hidden" class="myDescription" value="<?php echo $row['user_description']; ?>" />
									<input type="hidden" class="myNuAva" value="<?php echo $row['user_item_avl']; ?>" />
									<input type="hidden" class="myCostPer" value="<?php echo $row['user_cost']; ?>" />
									<input type="hidden" class="myType" value="<?php echo $row['user_hardgood_type']; ?>" />
								</button>
								<button class="btn btn-danger btn-xs" onclick="return delete_hardgood(this);" value="<?php echo $row[item_id]; ?>">
								<i class="fa fa-trash-o "></i>
								</button>
								</td>
							</tr>
							<?php 
							$i++; }
							}else{
								echo "<h2>No Hardgoods</h2>";
							} ?>
                              </tbody>
                          </table>
                      </div><!-- /content-panel -->
                  </div><!-- /col-md-12 -->
              </div><!-- /row -->
		</section><!--/wrapper -->
      </section><!-- /MAIN CONTENT -->
<!--main content end-->
<!--Add New Hardgood Form Start-->
<?php include('inc-forms/add_hardgood.php'); ?>
<!--//Add New Hardgood Form End-->

<!--Edit Hardgood Form Start-->
<div class="modal fade" id="editHardgoods" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id="myModalLabel">Edit This Hardgood</h4>
	      </div>
	      <div class="modal-body">
         <!-- <div class="form-panel">-->
		<form class="form-horizontal style-form" id="newHardgoodForm" method="post" onsubmit="return edit_new_hardgood();">
		<input value="" type="hidden" id="hardgoodID" name="hardgoodID">
			<div class="form-group">
				<label class="col-sm-2 col-sm-2 control-label">Hardgood*</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" id="hardgoodName">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-2 col-sm-2 control-label">Description</label>
				<div class="col-sm-8">
					<textarea class="form-control" id="hardgoodDesc"></textarea>	
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-2 col-sm-2 control-label">Number Available</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" id="hardgoodAvl">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-2 col-sm-2 control-label">Cost Per Hardgood</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" id="hardgoodCost">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-2 col-sm-2 control-label">Hardgood Type</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" id="hardgoodType" disabled="disabled">
				</div>
			</div>
			<div class="modal-footer">
				<div class="pull-left">
				*required
				</div>
				<button type="button" class="btn btn-default" data-dismiss="modal">
					Cancel
				</button>
				<button type="submit" name="action" value="addCustomer" class="btn btn-primary" >
					Let's Go!
				</button>
			</div>
		</form>
	      
	    </div>
	  </div>
	</div>     
</div><!--//Edit Hardgood Form Start-->

<script>


/*Ajax Function for Delete Hardgood*/
function delete_hardgood(obj){
	var itm_index = $(obj).attr('value');
	var cuser = "<?php echo $current_user->ID; ?>";
	var del_erow = $(obj).parents("tr");
	var del_row_class = "row_hardgood_"+itm_index;
	
	if(confirm("Are you sure you want to delete Hardgood")){
		jQuery.ajax({
		type:"post",
		url: "<?php bloginfo('url'); ?>/wp-admin/admin-ajax.php",
		data: {action: 'delete_user_hardgood', uid:cuser, itm_index:itm_index},
		success:function(resp){
			if(resp == del_row_class){
					del_erow.fadeOut(500).remove();
				}
		}
		});//ajax
	}
}


/*Get data in popup*/
function edit_hardgood(row_obj,myHardgood,myDescription,myNuAva,myCostPer,myType){
	$("#editHardgoods #hardgoodID").val("");
	$("#editHardgoods #hardgoodName").val("");
	$("#editHardgoods #hardgoodDesc").html("");
	$("#editHardgoods #hardgoodAvl").val("");
	$("#editHardgoods #hardgoodCost").val("");
	$("#editHardgoods #hardgoodType").val("");

	//add values
	$("#editHardgoods #hardgoodID").val(row_obj);
	$("#editHardgoods #hardgoodName").val(myHardgood);
	$("#editHardgoods #hardgoodDesc").html(myDescription);
	$("#editHardgoods #hardgoodAvl").val(myNuAva);
	$("#editHardgoods #hardgoodCost").val(myCostPer);
	$("#editHardgoods #hardgoodType").val(myType);
}

/*Ajax Function for Edit Hardgood*/
function edit_new_hardgood(){
	var iHardgoodID = $("#editHardgoods #hardgoodID").val();
	var iname = $("#editHardgoods #hardgoodName").val();
	var idesc = $("#editHardgoods #hardgoodDesc").val();
	var iavl = $("#editHardgoods #hardgoodAvl").val();
	var icost = $("#editHardgoods #hardgoodCost").val();
	var itype = $("#editHardgoods #hardgoodType").val();
	var cuser = "<?php echo $current_user->ID; ?>";
	if(iname !=""){
		jQuery.ajax({
		type:"post",
		url: "<?php bloginfo('url'); ?>/wp-admin/admin-ajax.php",
		data: {action: 'edit_user_hardgood', uid:cuser, iHardgoodID:iHardgoodID, iname: iname, idesc:idesc, iavl:iavl, icost:icost, itype:itype},
		success:function(resp){
			//alert(resp);
			location.reload();
		}
		
		});//ajax
	}
	return false;
}

</script>
<?php get_footer(); ?>