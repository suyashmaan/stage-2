<?php 
//Custom functions.php file for StemCounter.com
function theme_styles() {
    wp_register_style( 'fullpage-css', "//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css", array('jquery') );   
    wp_enqueue_style( 'fullpage-css' ); 

	wp_register_style( 'datepicker-css', get_template_directory_uri() . '/css/jquery.fullPage.css', array('jquery') );   
    wp_enqueue_style( 'datepicker-css' ); 
	
	
}
add_action('wp_enqueue_scripts', 'theme_styles');

add_theme_support('post-thumbnails');

wp_enqueue_script('jquery');
 

function addCustomer(){
       
        global $wpdb;
       
        $first_name = $_POST['first_name'];
		$last_name = $_POST['last_name'];
        $phone = $_POST['phone'];
        $email = $_POST['email'];
        $venue = $_POST['venue'];
		$event_name = $_POST['event_name'];
		$date = $_POST['date'];
       
	   			$details = '{"event_name":"' . $event_name . '", "contact": {"first_name":"' . $first_name . '", "last_name":"' . $last_name . '","phone_number":"' . $phone . '","email":"' . $email . '"}, "venue":"' . $venue . '", "status":"Quoted"}';
		
	   
        if($wpdb->insert('sc_events',array(
                'user'=> get_current_user_id(),
				'event_date' => $date,
                'details'=>$details))===FALSE){
       
                echo "Error";
               
        }
        else {
                        echo "Customer '".$name. "' successfully added, row ID is ".$wpdb->insert_id;
 
                }
        die(); 
}
add_action('wp_ajax_addCustomer', 'addCustomer');
add_action('wp_ajax_nopriv_addCustomer', 'addCustomer');

//Adding ajax

/*function new_event()
{

     wp_localize_script( 'function', 'insert_new_event', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
     wp_enqueue_script( 'function', get_template_directory_uri().'/js/stem_counter/form.js', 'jquery', true);

}
add_action('template_redirect', 'new_event');

$dirName = dirname(__FILE__);
$baseName = basename(realpath($dirName));
require_once ("$dirName/MyFunctions.php");

add_action("wp_ajax_get_my_option", "get_my_option");

*/


//THESE ARE THE THEME SCRIPTS. WE'RE GOING TO WANT TO ADJUST THESE TO ENQUEUE THE PIECES AS NEEDED PER PAGE.
add_action('wp_enqueue_scripts', 'theme_scripts');

function theme_scripts() {
	wp_deregister_script('jquery'); // Deregister WordPress jQuery
	wp_register_script('jquery', get_template_directory_uri() . '/js/jquery-1.11.0.js', array(), '1.11.0');
    wp_enqueue_script('jquery'); // Enqueue it!
	
	//TODO FIGURE OUT HOW TO GET THESE SET UP IN THE FOOTER
	
    wp_register_script( 'bootstrap.min', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery')); 
    wp_enqueue_script( 'bootstrap.min' );
	
    wp_register_script( 'accordian', get_template_directory_uri() . '/js/jquery.dcjqaccordion.2.7.js', array('jquery')); 
    wp_enqueue_script( 'accordian' );
	
    wp_register_script( 'slimscroll', get_template_directory_uri() . '/js/vendors/jquery.slimscroll.min.js', array('jquery'), null, true  );
    wp_enqueue_script( 'slimscroll' );
	
	wp_register_script( 'nicescroll', get_template_directory_uri() . '/js/jquery.nicescroll.js', array('jquery'));
    wp_enqueue_script( 'nicescroll' );

	wp_register_script( 'scrolltomin', get_template_directory_uri() . '/js/jquery.scrollTo.min.js', array('jquery'));
    wp_enqueue_script( 'scrolltomin' );
	
	
	wp_register_script( 'customswitch', get_template_directory_uri() . '/js/jquery-ui-1.9.2.custom.min.js', array('jquery'));
    wp_enqueue_script( 'customswitch' );
	
	
	
	//Date pickers
	wp_register_script( 'moment', get_template_directory_uri() . '/js/moment.js', array('jquery'));
    wp_enqueue_script( 'moment' );
	
	wp_register_script( 'datepicker', get_template_directory_uri() . '/js/bootstrap-datepicker.js', array('jquery'));
    wp_enqueue_script( 'datepicker' );
	
		
	wp_register_script( 'date', get_template_directory_uri() . '/js/bootstrap-daterangepicker/date.js', array('jquery'));
    wp_enqueue_script( 'date' );
	
		
	wp_register_script( 'daterangepicker', get_template_directory_uri() . '/js/bootstrap-daterangepicker/daterangepicker.js', array('jquery'));
    wp_enqueue_script( 'daterangepicker' );
	
	//Custom Tagsinput
	wp_register_script( 'tagsinput', get_template_directory_uri() . '/js/jquery.tagsinput.js', array('jquery'));
    wp_enqueue_script( 'tagsinput' );
	
	
	//Custom Tagsinput
	wp_register_script( 'inputmask', get_template_directory_uri() . '/js/bootstrap-inputmask/bootstrap-inputmask.min.js', array('jquery'));
    wp_enqueue_script( 'inputmask' );
	
	//Custom Tagsinput
	wp_register_script( 'formcomponent', get_template_directory_uri() . '/js/form-component.js', array('jquery'));
    wp_enqueue_script( 'formcomponent' );
	
	
	//STEMCOUNTER FILES
	wp_register_script( 'form_edit', get_template_directory_uri() . '/js/stem_counter/form.js', array('jquery'));
	wp_enqueue_script( 'form_edit' );
	
	wp_register_script( 'common-scripts', get_template_directory_uri() . '/js/common-scripts.js', array('jquery'));
    wp_enqueue_script( 'common-scripts' );
	

	
}


add_action('wp_enqueue_script','register_my_scripts');

function register_my_scripts(){
wp_enqueue_script('script');
}


    
// remove junk from head
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);

function remove_admin_bar(){
	return false;
}
add_filter('show_admin_bar', 'remove_admin_bar');

add_action('init', 'create_post_type');
function create_post_type()
{
    register_post_type('flower', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('Flowers'),
            'singular_name' => __('Flower'),
            'add_new' => __('Add Flower'),
            'add_new_item' => __('Add Flower'),
            'edit' => __('Edit'),
            'edit_item' => __('Edit Flower post'),
            'new_item' => __('New Flower Custom Post'),
            'view' => __('View Flower Custom Post'),
            'view_item' => __('View Flower Custom Post'),
            'search_items' => __('Search Flower Custom Post'),
            'not_found' => __('No Flower found'),
            'not_found_in_trash' => __('No Flower found in Trash')
        ),
        'public' => true,
        'hierarchical' => true,
        'rewrite' => array( 'slug' => 'flower' ),
        'has_archive' => true,
        'supports' => array('title','editor','excerpt','thumbnail'),
        'can_export' => true
        //'taxonomies' => array('post_tag','category')
    ));
}

function my_search() {
	global $wpdb; 
	$search_flowers = $_POST['search_flowers'];
	$myrows = $wpdb->get_results("select ID from wp_posts where post_type = 'flowers' and post_title LIKE '%".$search_flowers."%'", ARRAY_A);
	foreach ($myrows as $key => $value) {
		$new[] = $value['ID'];
	}
	print_r($new);
	
	die();
}
add_action( 'wp_ajax_my_search', 'my_search' );
add_action( 'wp_ajax_nopriv_my_search', 'my_search' );

/*Nik Development*/
function add_user_event(){
	global $wpdb;
	$ename = $_POST['ename'];
	$edate = $_POST['edate'];
	$efname = $_POST['efname'];
	$elname = $_POST['elname'];
	$evanue = $_POST['evanue'];
	$cuser = $_POST['cuser'];
	$_date = explode("/", $edate);
	
    $details = array("firstName" => $efname,"lastName" => $elname, "Venue" => $evanue);
	$details = json_encode($details);
	
	$e_date = $_date[2]."-".$_date[0]."-".$_date[1];
	
	$check_query = $wpdb->query("SELECT * FROM `wp_user_events` WHERE event_name = '$ename' && user = $cuser", ARRAY_A);
	
	if($check_query >= 1){
		echo "Event already created..";
	}
	elseif($ename){
	$in_event = $wpdb->insert("wp_user_events", array("event_name" => $ename,"event_date" => $e_date,"user" => $cuser, "details"=>"$details"));
			if($in_event){
				echo $wpdb->insert_id;
			}else{
				echo "No Event Added";
			}
	}else{
		echo "No Record Added";
	}
	die();
}
add_action('wp_ajax_add_user_event', 'add_user_event');
add_action('wp_ajax_nopriv_add_user_event', 'add_user_event');

/*Edit User Event*/
function edit_user_event(){
	global $wpdb;
	$ename = $_POST['ename'];
	$edate = $_POST['edate'];
	$efname = $_POST['efname'];
	$elname = $_POST['elname'];
	$evanue = $_POST['evanue'];
	$eid = $_POST['evID'];
	$cuser = $_POST['cuser'];
	$_date = explode("/", $edate);
	$details = array("firstName" => $efname,"lastName" => $elname, "Venue" => $evanue);
	$details = json_encode($details);
	$e_date = $_date[2]."-".$_date[0]."-".$_date[1];
	
	/*$check_query = $wpdb->query("SELECT * FROM `wp_user_events` WHERE event_name = '$ename' && user = $cuser", ARRAY_A);
	if($check_query >= 1){
		echo "Event already created.. ";
	}else{*/
	$in_event = $wpdb->update("wp_user_events", array("event_name" => $ename,"event_date" => $e_date,"user" => $cuser, "details"=>"$details"), array('event_id'=>$eid));
	echo "Event updated Sucessfully!!";
	//}
	die(); 
}

/*Remove Slashes Function*/
function stripslashes_full($input)
{
    if (is_array($input)) {
        $input = array_map('stripslashes_full', $input);
    } elseif (is_object($input)) {
        $vars = get_object_vars($input);
        foreach ($vars as $k=>$v) {
            $input->{$k} = stripslashes_full($v);
        }
    } else {
        $input = stripslashes($input);
    }
    return $input;
}

/*Ajax function for Edit User Events*/
function edit_user_event_id(){
	global $wpdb;
	$eid = $_POST['evID'];
	$user_ID = get_current_user_id();
	//echo "SELECT * FROM `wp_user_events` WHERE user =".$user_ID." && archived = 0 && event_id = ".$eid;
	$check_query = $wpdb->get_row("SELECT * FROM `wp_user_events` WHERE user =".$user_ID." && archived = 0 && event_id = ".$eid);
	
	$check_query = stripslashes_full($check_query);
	$check_query->details = json_decode($check_query->details);
	$check_query->event_order = json_decode($check_query->event_order);
	//print_r($check_query); exit();
	//return $check_query;
	echo stripslashes(json_encode($check_query));
	die();
	//print_r($check_query);
	
}

/*Ajax function Archive User Events*/
function archive_user_event_id(){
	global $wpdb;
	$eid = $_POST['evID'];
	//$cuser = $_POST['cuser'];
	//$_date = explode("/", $edate);
	//$details = array("firstName" => $efname,"lastName" => $elname, "Venue" => $evanue);
	//$details = json_encode($details);
	//$e_date = $_date[2]."-".$_date[0]."-".$_date[1];
	
	/*$check_query = $wpdb->query("SELECT * FROM `wp_user_events` WHERE event_name = '$ename' && user = $cuser", ARRAY_A);
	if($check_query >= 1){
		echo "Event already created.. ";
	}else{*/
	$in_event = $wpdb->update("wp_user_events", array("archived" => 1), array('event_id'=>$eid));
	echo "Event archived Sucessfully!!";
	//}
	die(); 
	
}
add_action('wp_ajax_archive_user_event_id', 'archive_user_event_id');
add_action('wp_ajax_edit_user_event_id', 'edit_user_event_id');
add_action('wp_ajax_edit_user_event', 'edit_user_event');
add_action('wp_ajax_nopriv_edit_user_event', 'edit_user_event');

/*Ajax Function Delete User Event*/
function delete_user_event(){
	global $wpdb;
	$event_id = $_POST['event_id'];
	$wpdb->delete("wp_user_events", array("event_id"=>$event_id));
	echo "user_event_".$event_id;
	die(); 
}
add_action('wp_ajax_delete_user_event', 'delete_user_event');
add_action('wp_ajax_nopriv_delete_user_event', 'delete_user_event');



/*Ajax Function for Add New Arrangement*/
function add_arrangement(){
	global $wpdb;
	$argName = $_POST['argName'];
	$argQty = $_POST['argQty'];
	$flow_name = $_POST['flow_name'];
	$flow_cost = $_POST['flow_cost'];
	$flow_qty = $_POST['flow_qty'];
	$itm_type = $_POST['itm_type'];
	$eid = $_POST['eid'];
	$note = $_POST['argNote'];
	
	$c = count($flow_name);
	for($i=0; $i<$c;$i++){
		$fflow_name[$flow_name[$i]] = array("qty"=>$flow_qty[$i], "cost"=>$flow_cost[$i], "type"=>$itm_type[$i]);   
	}
	
	$jo_det[] = array("arrangement" => $argName, "qty" => $argQty, 'items'=>$fflow_name, 'arrangement_note'=>$note);
	$eorder_data = json_encode($jo_det);
	
	$datacheck = $wpdb->get_results("SELECT `event_order` FROM `wp_user_events` WHERE event_id = $eid", ARRAY_A);
	
	$cd_val = json_decode($datacheck[0]['event_order'], true);
	//print_r($cd_val);
	//print_r($jo_det); //exit(); 
	if($cd_val){
		$new_ary = array_merge($cd_val,$jo_det);
		$eorder_data = json_encode($new_ary);			
	}
	
	$res = $wpdb->update("wp_user_events", array("event_order" => $eorder_data), array('event_id'=>$eid));
	if($res){
		echo "New Arrangement Added Sucessfully";
	}	
	die;
}
add_action('wp_ajax_add_arrangement', 'add_arrangement');
add_action('wp_ajax_nopriv_add_arrangement', 'add_arrangement');


/*Ajax Function for Edit Arrangement*/
function edit_arrangement(){
	global $wpdb;
	$argName = $_POST['argName'];
	$argQty = $_POST['argQty'];
	$flow_name = $_POST['flow_name'];
	$flow_cost = $_POST['flow_cost'];
	$flow_qty = $_POST['flow_qty'];
	$flow_type = $_POST['flow_type'];
	$ary_index = $_POST['ary_index'];
	$eid = $_POST['eid'];
	$argNote = $_POST['argNote'];

	//print_r($_POST); exit();

	$c = count($flow_name);
	for($i=0; $i<$c;$i++){
		$fflow_name[$flow_name[$i]] = array("qty"=>$flow_qty[$i], "cost"=>$flow_cost[$i], "type"=>$flow_type[$i]);   
	}
	
	$jo_det = array("arrangement" => $argName, "qty" => $argQty, 'items'=>$fflow_name, "arrangement_note"=>$argNote);
	$eorder_data = json_encode($jo_det);
	$datacheck = $wpdb->get_results("SELECT `event_order` FROM `wp_user_events` WHERE event_id = $eid", ARRAY_A);
	$cd_val = json_decode($datacheck[0]['event_order']);
	
	$cd_val[$ary_index]= $jo_det;
	$eorder_data = json_encode($cd_val);
	$res = $wpdb->update("wp_user_events", array("event_order" => $eorder_data), array('event_id'=>$eid));
	if($res){
		echo "Record updated successfully!!";
	}	
	die;
}
add_action('wp_ajax_edit_arrangement', 'edit_arrangement');
add_action('wp_ajax_nopriv_edit_arrangement', 'edit_arrangement');

/*Ajax Function for Delete Arrangement*/
function delete_arrangement(){
	global $wpdb;
	$ary_index = $_POST['ary_index'];
	$eid = $_POST['eid'];
	
	$datacheck = $wpdb->get_results("SELECT `event_order` FROM `wp_user_events` WHERE event_id = $eid", ARRAY_A);
	$cd_val = json_decode($datacheck[0]['event_order'], true);
	unset($cd_val[$ary_index]);
	$m=0;
	foreach ($cd_val as $key => $value) {
		$cd_val1[$m] = $value;
		$m++;
	}
	$eorder_data = json_encode($cd_val1);
	
	$res = $wpdb->update("wp_user_events", array("event_order" => $eorder_data), array('event_id'=>$eid));
	
	if($res){
		echo "Arrangement Deleted successfully!!";
	}
	die;
}
add_action('wp_ajax_delete_arrangement', 'delete_arrangement');
add_action('wp_ajax_nopriv_delete_arrangement', 'delete_arrangement');


/*Ajax Function for Edit Company Detail*/
function edit_comapany_detail(){
	global $wpdb;
	$userID = $_POST['userID'];
	$companyName = $_POST['companyName'];
	$companyAddress = $_POST['companyAddress'];
	$companyWebsite = $_POST['companyWebsite'];
	$companyMobile	= $_POST['companyMobile'];

	$datacheck = $wpdb->get_row("SELECT `meta_value` FROM `wp_usermeta` WHERE user_id= $userID AND meta_key = 'wp_s2member_custom_fields' ", ARRAY_A);
	$undata = unserialize($datacheck[meta_value]);
	//print_r($undata);
	$undata[company_name] = $companyName;
	$undata[company_address] = $companyAddress;
	$undata[company_website] = $companyWebsite;

	$serialData = serialize($undata);
	//echo $serialData;
	$res = $wpdb->update("wp_usermeta", array("meta_value" =>$serialData), array('user_id'=> $userID, 'meta_key' =>'wp_s2member_custom_fields'));
	
	$additionalInfo[companyMobile] = $companyMobile;
	$serializeAdditionalInfo = serialize($additionalInfo);
	$getAdditionalInfo = get_user_meta($userID, 'additionalInfo');
	//print_r($getAdditionalInfo);
	if(empty($getAdditionalInfo)){
		add_user_meta($userID, 'additionalInfo', $serializeAdditionalInfo);
	}else{
		update_user_meta($userID, 'additionalInfo', $serializeAdditionalInfo);
	}


	//$checkCompanyMobile = 

	// echo $wpdb->last_query;
	//if($res){
		echo "Company Detail successfully Changed!!";
	//}
	die;

}
add_action('wp_ajax_edit_comapany_detail', 'edit_comapany_detail');
add_action('wp_ajax_nopriv_edit_comapany_detail', 'edit_comapany_detail');


/*Ajax Function for Edit Invoice Detail*/
function edit_invoice(){
	global $wpdb;
	$userID = $_POST['userID'];
	$hardgoodMultiple = $_POST['hardgoodMultiple'];
	$freshFlowerMultiple = $_POST['freshFlowerMultiple'];
	$chnageCardRate = $_POST['chnageCardRate'];
	$saleTax	= $_POST['saleTax'];
	$delivery = $_POST['delivery'];
	$labor = $_POST['labor'];
	$increaseFlowersOrder = $_POST['increaseFlowerOrdered'];

	$datacheck = $wpdb->get_row("SELECT `meta_value` FROM `wp_usermeta` WHERE user_id= $userID AND meta_key = 'wp_s2member_custom_fields' ", ARRAY_A);
	$undata = unserialize($datacheck[meta_value]);
	$undata[hardgood_multiple] = $hardgoodMultiple;
	$undata[fresh_flower_multiple] = $freshFlowerMultiple;
	$undata[increase_flowers_order] = $increaseFlowersOrder;
	$undata[__charge_card_rate] = $chnageCardRate;
	$undata[sales_tax__] = $saleTax;
	$undata[__delivery_cost] = $delivery;
	$undata[__labor] = $labor;

	$serialData = serialize($undata);
	$res = $wpdb->update("wp_usermeta", array("meta_value" =>$serialData), array('user_id'=> $userID, 'meta_key' =>'wp_s2member_custom_fields'));
	// echo $wpdb->last_query;
	if($res){
		echo "Invoicing Settings successfully Changed!!";
	}
	die;

}
add_action('wp_ajax_edit_invoice', 'edit_invoice');
add_action('wp_ajax_nopriv_edit_invoice', 'edit_invoice');

/*Function To display items in Arrangements on "StemCounter" Page*/
/*function item_fields(){
echo '<select name="flow_name[]" class="form-control flow_type mbot5 flow_name"><option value="--">Select Flower</option>';	
$loop = new wp_Query('post_type=flowers&posts_per_page=999999');
while($loop->have_posts()) : $loop->the_post();
$flo_cost = get_field('flower_cost', $post->ID);
echo '<option value="'.get_the_title().'" dataitem="'.$flo_cost.'">'.get_the_title().'</option>'; 
endwhile; wp_reset_postdata(); 
echo '<option value="Other">Other....</option></select><input type="text" name="flow_cost[]" value="0.00" class="form-control mbot5 flow_cost"><input type="text" name="flow_qty[]" class="form-control mbot5 flow_qty">';
}*/

function item_fields($uid){
	echo '<div class="xt-col-3"><label>Items</label><select name="flow_name[]" class="form-control flow_type mbot5 flow_name"><option value="--">Select Item</option>';	
	global $wpdb;
	$flowerLoop = $wpdb->get_results("SELECT `flower`, `cost`, `color` FROM `wp_user_flowers` WHERE user_id = $uid", ARRAY_A);
	$item_type = "";
	if(!empty($flowerLoop)){
		//print_r($flowerLoop); die;
		echo '<optgroup label="Flower" id="flower">';
		foreach ($flowerLoop as $flower) {
			
			$color_var = $flower['color'];
			$color_var = explode(",", $color_var);
			$var_count = count($color_var);
			//print_r($color_var);
			if(count($color_var)>1){
				for($i=0; $i<count($color_var); $i++){
					//$color_flower = $flower['flower']."-".$color_var[$i];
					echo '<option value="'.$flower['flower']."-".$color_var[$i].'" dataitem="'.$flower['cost'].'">'.$flower['flower']."-".$color_var[$i].'</option>';
					//echo $color_var[$i];
				}
			}else{
				//echo "No Variation";
				echo '<option value="'.$flower['flower'].'" dataitem="'.$flower['cost'].'">'.$flower['flower'].'</option>';
			}
			//echo '<option value="'.$flower['flower'].'" dataitem="'.$flower['cost'].'">'.$flower['flower'].'</option>';
		}
		echo '</optgroup>';
	}
	$hardgoodLoop = $wpdb->get_results("SELECT `user_item`, `user_cost` FROM `wp_user_hardgood` WHERE user_id = $uid", ARRAY_A);
	if(!empty($hardgoodLoop)){
		echo '<optgroup label="Hardgoods" id="hardgood">';
		foreach ($hardgoodLoop as $Hardgoods) {
			echo '<option value="'.$Hardgoods['user_item'].'" dataitem="'.$Hardgoods['user_cost'].'">'.$Hardgoods['user_item'].'</option>'; 
		}
		echo '</optgroup>';
	}
	$myItemLoop = $wpdb->get_results("SELECT `user_item`, `user_cost` FROM `wp_user_items` WHERE user_id = $uid", ARRAY_A);
	if(!empty($myItemLoop)){
		echo '<optgroup label="My Items" id="myItems">';
		foreach ($myItemLoop as $myItems) {
			echo '<option value="'.$myItems['user_item'].'" dataitem="'.$myItems['user_cost'].'">'.$myItems['user_item'].'</option>'; 
		}
		echo '</optgroup>';
	}
	/*while($loop->have_posts()) : $loop->the_post();
	$flo_cost = get_field('flower_cost', $post->ID);
	echo '<option value="'.get_the_title().'" dataitem="'.$flo_cost.'">'.get_the_title().'</option>'; 
	endwhile; wp_reset_postdata(); */
	//echo '<option value="Other">Other....</option>';
	echo '</select></div><div class="xt-col-3 mid"><label>Cost</label><input type="text" name="flow_cost[]" value="0.00" class="form-control mbot5 flow_cost"></div><div class="xt-col-3"><label>Quantity</label><input type="text" name="flow_qty[]" class="form-control mbot5 flow_qty"><input type="hidden" class="itemType"></div>';
}
//Auto Login after Registration
/*
function auto_login_new_user($user_id) {
        wp_set_current_user($user_id);
        wp_set_auth_cookie($user_id);
        wp_redirect( home_url() );
        exit;
}
add_action('user_register', 'auto_login_new_user');
*/
 function pr($data)
{
echo '<pre>';print_r($data);echo '</pre>';
}


/*Function To display event Name, Use event ID as function's parameter */
function the_xt_event($eid, $uid){
global $wpdb;
	$result = $wpdb->get_results("SELECT `event_name` FROM `wp_user_events` WHERE event_id = $eid AND user = $uid ", ARRAY_A);
	$ename = $result[0]['event_name'];
 	return $ename;
}

function the_event_date($eid, $uid){
global $wpdb;
	$result = $wpdb->get_results("SELECT `event_date` FROM `wp_user_events` WHERE event_id = $eid AND user = $uid ", ARRAY_A);
	$edate = $result[0]['event_date'];
 	return $edate;
}

function check_wp_login(){
	// First check the nonce, if it fails the function will break
    //check_ajax_referer('ajax-login-nonce', 'security');
    // Nonce is checked, get the POST data and sign user on
    $info = array();
    $info['user_login'] = $_POST['user'];
    $info['user_password'] = $_POST['pass'];
    $info['remember'] = true;
    $user_signon = wp_signon( $info, false );
    if(is_wp_error($user_signon)){
		//json_encode(array('loggedin'=>false, 'message'=>__('Wrong username or password.')));
		echo "0";
		die;
    }
    die();
}
add_action('wp_ajax_check_wp_login', 'check_wp_login');
add_action('wp_ajax_nopriv_check_wp_login', 'check_wp_login');

function user_gen_info(){
	global $wpdb;
	$uid = $_POST['uid'];
	$eid = $_POST['eid'];
	$ename = $_POST['ename'];
	$edate = $_POST['edate'];
	$edate = explode("/", $edate);
	$edate = $edate[2]."-".$edate[0]."-".$edate[1];
	$fname = $_POST['fname'];
	$lname = $_POST['lname'];
	$phone = $_POST['phone'];
	$email = $_POST['email'];
	
	$edata =  $wpdb->get_results("SELECT details FROM wp_user_events WHERE user = ".$uid." AND event_id = ".$eid);
	$edata = $edata[0]->details;
	$edata = json_decode($edata);
	$evanue = $edata->Venue;
	$edata = get_object_vars($edata);
	$exe_details = array("firstName" => $fname,"lastName" => $lname, "Venue" => $evanue);
	$add_details['contact_info'] = array("phone"=>$phone, "email"=>$email);
	$mrg_array = array_merge($exe_details,$add_details);
	$mrg_array = json_encode($mrg_array);
	//print_r($mrg_array);
	$nameCheck = $wpdb->get_row("SELECT `event_name` FROM `wp_user_events` WHERE user= $uid AND event_id = $eid", ARRAY_A);
	$nameCheck = $nameCheck['event_name'];
	if($nameCheck == $ename){
		$res = $wpdb->update("wp_user_events", array("details" => $mrg_array, "event_date"=>$edate), array('event_id'=>$eid,'user'=>$uid));
		if($res){
			echo "Record Updated sucessfully!!";
		}else{
			echo "No Changes in Record";
		}
	}else{
		$check_query = $wpdb->get_results("SELECT `event_name` FROM `wp_user_events` WHERE user= $uid", ARRAY_A);
		$count = count($check_query);
		for($i =0; $i<$count; $i++){
			$in_ary[] = $check_query[$i]['event_name'];
		}
			if (in_array($ename, $in_ary))
			{
				echo "Event ".$ename." Already Exist";
			}
			else
			{
				$res = $wpdb->update("wp_user_events", array("event_name"=>$ename, "details" => $mrg_array), array('event_id'=>$eid,'user'=>$uid));
				if($res){
					echo "Record Updated sucessfully!!";
				}else{
					echo "No Changes in Record";
				}
			}
	}
	die;
}
add_action('wp_ajax_user_gen_info', 'user_gen_info');
add_action('wp_ajax_nopriv_user_gen_info', 'user_gen_info');

function user_venue_info(){
	global $wpdb;
	$uid = $_POST['uid'];
	$eid = $_POST['eid'];
	$venue = $_POST['venue'];
	$address = $_POST['address'];
	
	$edata =  $wpdb->get_results("SELECT details FROM wp_user_events WHERE user = ".$uid." AND event_id = ".$eid);
	$edata = $edata[0]->details;
	$edata = json_decode($edata);
	$fix_array = array("firstName"=>$edata->firstName, "lastName"=>$edata->lastName, "Venue"=>$venue, "address"=>$address,
	'contact_info'=>array("phone"=>$edata->contact_info->phone, "email"=>$edata->contact_info->email));
	$fix_array = json_encode($fix_array);
	
	$res = $wpdb->update("wp_user_events", array("details" => $fix_array), array('event_id'=>$eid,'user'=>$uid));
	if($res){
	echo "Record Updated sucessfully!!";
	}else{
	echo "No Changes in Record";
	}
	die;
}
add_action('wp_ajax_user_venue_info', 'user_venue_info');
add_action('wp_ajax_nopriv_user_venue_info', 'user_venue_info');


add_action( 'user_register', 'register_user_testdata');
function register_user_testdata($user_id){
global $wpdb;
$details = '{"firstName":"Kate","lastName":"Middleton","Venue":"The Palace","address":"London SW1A 1AA","contact_info":{"phone":+44 20 7766 7300,"email":kate@gmail.com}}';
$ordr = '[{"arrangement":"Bridal Bouquet","qty":"1","items":{"Hydrangeas-White":{"qty":"4","cost":"2","type":"flower"},"Peonies-Light Pink":{"qty":"8","cost":"3.5","type":"flower"},"50 cm Rose- Blush":{"qty":"12","cost":"1.15","type":"flower"},"Stock-Light Pink":{"qty":"5","cost":".80","type":"flower"},"Ranunculus-White":{"qty":"8","cost":"2","type":"flower"},"Dusty miller":{"qty":"6","cost":"8","type":"flower"},"Hypercium Berries-Green":{"qty":"6","cost":".90","type":"flower"},"Vase\/Ribbon":{"qty":"1","cost":"2.5","type":"hardgood"},"Labor":{"qty":"1","cost":"15","type":"my items"}},"arrangement_note":"Classic, round ball, hand tied bouquet!"},{"arrangement":"Bridesmaids Bouquets","qty":"7","items":{"Hydrangeas-White":{"qty":"3","cost":"2","type":"flower"},"Hypercium Berries-Green":{"qty":"5","cost":".90","type":"flower"},"Ranunculus-White":{"qty":"5","cost":"2","type":"flower"},"Dusty miller":{"qty":"3","cost":"8","type":"flower"},"Vase\/Ribbon":{"qty":"1","cost":"2.5","type":"hardgood"},"Labor":{"qty":".5","cost":"15","type":"my items"}},"arrangement_note":"Classic, round ball, hand tied bouquet"},{"arrangement":"Boutonniere","qty":"8","items":{"Boutonniere":{"qty":"1","cost":"11","type":"my items"}},"arrangement_note":"Small, modern boutonniere. 1 ranunculus, 1 hypericum berry. "}]';
$wpdb->insert("wp_user_events", array("event_name" => "Royal Wedding","event_date" => "2015-4-19","user" => $user_id, "details"=>$details,"event_order"=>$ordr));	

$wpdb->query("INSERT INTO `wp_user_flowers` 
	(`user_id`, `flower`, `color`, `stems`, `cost`, `type`) VALUES
	($user_id, 'Hydrangeas', 'White, Blue, Pink, Green, Lime Green, Dark Green, Vintage Red, Purple', '1', '2', 'Focal'),
	($user_id, 'Dahlia', 'Pink, Peach, White, Purple, Red, Yellow, Orange', '10', '3', 'Focal'),
	($user_id, '50 cm Rose', 'Light pink, Blush, Red, Purple, Orange, Green, White, Pink, Hot Pink', '25', '1.15', 'Focal'),
	($user_id, 'Peonies', 'Light Pink, White, Hot Pink, Coral, Burgundy', '5', '3.5', 'Flower'),
	($user_id, 'Spray roses', 'White, Light Pink, Pink, Red, Yellow, Orange, Purple', '10', '1.30', 'Focal'),
	($user_id, 'Stock', 'Light Pink, White, Purple, Dark Purple, Fuchsia, Yellow, Peach', '10', '.80', 'Focal'),
	($user_id, 'Carnation', 'White, Pink, Yellow, Light Pink, Hot Pink, Peach, Purplse', '25', '.50', 'Flower'),
	($user_id, 'Mini Calla Lily', 'White', '10', '3.50', 'Flower'),
	($user_id, 'Calla Lily', 'White, Orange, Purple, Burgundy, Yellow, Red', '10', '2.75', 'Flower'),
	($user_id, 'Poppies', 'White, Red, Purple, Pink', '10', '2', 'Flower'),
	($user_id, 'Tulip', 'Light pink, Blush, Red, Purple, Orange, Green, White, Pink, Hot Pink', '10', '1.5', 'Flower'),
	($user_id, 'Snow on the Mountain', '', '10', '1.25', 'Flower'),
	($user_id, 'Hypercium Berries', 'Green, White, Peach, Red, Burgundy', '10', '1.10', 'Flower'),
	($user_id, 'Cockscomb', 'Hot pink, Orange, Red', '10', '2', 'Flower'),
	($user_id, 'Mini Gerber Daisy', 'Hot pink, White, Yellow, Orange, Red, Peach, Pink, Light Pink', '10', '1.6', 'Flower'),
	($user_id, 'Pennycress', 'green', '1', '8', 'Flower'),
	($user_id, 'Ranunculus', 'White, Yellow, Peach, Red, Purple, Burgundy, Pink', '10', '2', 'Flower'),
	($user_id, 'Viburnum', 'green', '10', '1.6', 'Flower'),
	($user_id, 'Viburnum berry', 'blue', '10', '1', 'Flower'),
	($user_id, 'Garden Rose', 'Light pink, Blush, Red, Purple, Orange, Green, White, Pink, Hot Pink', '12', '3', 'Flower'),
	($user_id, 'Sweet Peas', 'White, Light Pink, Light Purple, Dark Purple', '10', '2', 'Flower'),
	($user_id, 'Dusty miller', 'gray', '10', '8', 'Flower'),
	($user_id, 'Hanging Amaranthus', 'Green, Red', '10', '2', 'Flower')
	");

$wpdb->query("INSERT INTO `wp_user_hardgood`
	(`user_id`, `user_item`, `user_description`, `user_cost`, `user_item_type`) VALUES
	($user_id, 'Vase and Ribbon','Cylinder Vase and ribbon for each bouquet','2.5','hardgood'),
	($user_id, '9\" Designing Tray','','1','hardgood'),
	($user_id, 'Oasis Block','','0.5','hardgood'),
	($user_id, 'Oasis Wreath','','3','hardgood'),
	($user_id, '3\" Oasis Floral Foam Sphere','Great for flower balls','1','hardgood')
	");

$wpdb->query("INSERT INTO `wp_user_items`
	(`user_id`, `user_item`, `user_description`, `user_cost`, `user_item_type`) VALUES
	($user_id, 'Toss','Base price for a toss bouquet','15','Base Price'),
	($user_id, 'Standard Corsage','This a standard cost for a corsage.','20','Base Price'),
	($user_id, 'Customer Service','Sometimes referred to as another popular acronym','15','Fee'),
	($user_id, 'Clear Hobnail Vase','Rental Item','3','Rental'),
	($user_id, 'Medicine Bottle','Varying heights \/ rental','3','Rental'),
	($user_id, 'Labor','Standard hourly labor cost','15','Fee'),
	($user_id, 'Short Delivery','Fee for a short delivery time','50','Fee'),
	($user_id, 'Setup','Fee for setup \/ teardown','200','Fee'),
	($user_id, 'Boutonniere','Small men\'s floral','13','Base Price')
	");

}

include 'dev-function.php';
include 'generate-pdf.php';
include 'flow-function.php';
?>
