<?php get_header(); ?>
<?php get_sidebar('event'); ?>
<!-- On Page PHP -->



<!--Add New Arrangement Form Start-->
<div class="modal fade" id="newArgmnt" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">Add New Arrangement</h4>
			</div>
			<div class="modal-body">
			<!--Form Used for add new Arrangement-->	
				<form class="form-horizontal style-form" method="post" onsubmit="return add_arng();">
					<input type="hidden" id="editEveID" value="" />
					<div class="form-group">
						<label class="col-sm-8">Arrangement name*</label>
						<div class="col-sm-8">
							<input type="text" name="argName" class="form-control" id="argName">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-8">No. of Arrangement</label>
						<div class="col-sm-8">
							<input type="text" name="argNumber" class="form-control" id="argNumber">
						</div>
					</div>

					<div class="form-group">
						<!--Removing this title for space-->
						<!--<label class="col-sm-8">Items</label>-->
						<div class="col-sm-10 input_fields_wrap">
						<?php item_fields(get_current_user_id()); ?>
						<button class="add_field_button">Add More Fields</button>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-8">Arrangement Note.</label>
						<div class="col-sm-8">
							<textarea name="argNote" class="form-control" id="argNote"></textarea>	
						</div>
					</div>

					<div class="modal-footer">
						<div class="pull-left">*required</div>
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						<button type="submit" id="newArrgActin" value="addCustomer" class="btn btn-primary" >Let's Go!</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!--//Add New Arrangement Form End-->

<!--#addNewFlower-->
<?php include('inc-forms/add_flower.php'); ?>
   <!--#addNewFlower-->


<!--Add New Hardgood Form Start-->
<?php include('inc-forms/add_hardgood.php'); ?>
<!--//Add New Hardgood Form End-->


<!--Edit Arrangement Form start-->
<div class="modal fade" id="editArgmnt" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">Edit Arrangement</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal style-form" method="post" onsubmit="return edit_arng();">
					<input type="hidden" class="setAryIndex" value="" />	
					<input type="hidden" id="editEveID" value="" />
					<div class="form-group">
						<label class="col-sm-8">Arrangement name*</label>
						<div class="col-sm-8">
							<input type="text" class="form-control edArgName">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-8">No. of Arrangement</label>
						<div class="col-sm-8">
							<input type="text" class="form-control edArgQty">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-8">Items</label>
						<div class="col-sm-10 input_fields_wrap">
							<?php item_fields(get_current_user_id()); ?>
							<button class="add_field_button">Add More Fields</button>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-8">Arrangement Note.</label>
						<div class="col-sm-8">
							<textarea name="editargNote" class="form-control editargNote"></textarea>	
						</div>
					</div>

					<div class="modal-footer">
						<div class="pull-left">*required</div>
						<button type="button" class="btn btn-default" onclick="return del_arrangement();">Delete</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						<button type="submit" value="addCustomer" class="btn btn-primary" >Let's Go!</button>
					</div>
				</form>
			</div>
	    </div> 
	</div>
</div>
<!--//Edit Arrangement Form End-->


<script>
$(document).ready(function(){
	var event_ID= "<?php echo $_GET['eid']; ?>";
	var proposal = "../proposal";
	$("#nav-accordion li").each(function(){
		//alert($(this).children("a").attr('href'));
		if($(this).children("a").attr('href')==proposal){
			$(this).children("a").attr('href' ,proposal+'/?eid='+event_ID);
		}
	});

	$(document).on('change', '.flow_type', function(){
	var selected = $("option:selected", this);
    if(selected.parent()[0].id == "flower"){
    	$(this).parent().next().next().children('.itemType').val("flower");	
    }else if(selected.parent()[0].id == "hardgood"){
    	$(this).parent().next().next().children('.itemType').val("hardgood");
    }else if(selected.parent()[0].id == "myItems"){
    	$(this).parent().next().next().children('.itemType').val("my items");
    }
    		
		if($(this).val()=="Other"){
			$(this).parent().next().next(".xt-col-3").after("<div class='xt-col-4'><input type='text' class='form-control mbot5 flow_other_name' name='flow_name[]'></div>")
			$(this).removeAttr('name');

		}else{
			$(this).attr('name', 'flow_name[]');
			$(this).parent().next().next().next(".xt-col-4").remove();
			var f_cost = $(this).children().children(":selected").attr("dataitem"); 
			$(this).parent().next().children(".flow_cost").val(f_cost); 

		}
	});

	var max_fields      = 99999;
	var wrapper         = $(".input_fields_wrap");
	var add_button      = $(".add_field_button");
	var x = 1;
	$(add_button).click(function(e){
		e.preventDefault();
		if(x < max_fields){
			x++;
			var new_input = "<input type='text' class='form-control mbot5 flow_other_name' name='flow_name[]'>";
			$(wrapper).append('<div class="newGroup"><?php item_fields(get_current_user_id()); ?><a href="#" class="remove_field">Remove</a></div>'); //add input box
		}
		$("select").select2(); 
	});
	$(wrapper).on("click",".remove_field", function(e){
		e.preventDefault(); $(this).parent('div').remove(); x--;
	});
	
	$("#add_new_stemcounter").click(function(){
		$("#argName").val("");
		$("#argNumber").val("");
		$(".flow_type").val("--");
		$(".flow_cost").val("0.00");
		$(".flow_qty").val("");
		$(".flow_other_name").remove();
		$(".newGroup").remove();
		$("select").select2();
		$("#newArrgActin").removeAttr('disabled');
	});
        
});



function add_arng(){
	var argName = $("#argName").val();
	var argQty = $("#argNumber").val();
	var argNote = $("#argNote").val();
	//$("#newArrgActin").attr('disabled','disabled');
	var fnameArr = [];
	$('#newArgmnt select.flow_name').each(function(){
		var flow_name = $(this).val();
		if(flow_name=="Other"){
			fnameArr.push($(this).parent().next().next().next().children(".flow_other_name").val()); 	
		}else{
			fnameArr.push(flow_name); 	
		}
		
	});
	
	var fcostArr = [];
	$('#newArgmnt input.flow_cost').each(function(){
		var flow_cost = $(this).val();
		fcostArr.push(flow_cost); 
	});
	
	var fqtyArr = [];
	$('#newArgmnt input.flow_qty').each(function(){
		var flow_qty = $(this).val();
		fqtyArr.push(flow_qty); 
	});
	
	var typArr = [];
	$('#newArgmnt input.itemType').each(function(){
		var ftyp_ = $(this).val();
		typArr.push(ftyp_); 
	});
	
	var flow_name = fnameArr;
	var flow_cost = fcostArr;
	var flow_qty = fqtyArr;
	var itm_type = typArr;
	
	var eid = "<?php echo $_GET['eid']; ?>";
	
	jQuery.ajax({
	type:"post",
	url: "<?php bloginfo('url'); ?>/wp-admin/admin-ajax.php",
	data: {action: 'add_arrangement', argName:argName,  argQty:argQty, flow_name:flow_name , eid:eid, flow_cost:flow_cost, 
	flow_qty:flow_qty, itm_type:itm_type, argNote:argNote},
	success:function(edata){
		//alert(edata);
		location.reload();
	}
	});
	return false;
}




$(document).ready(function(){
	$(".edit-bqt").on('click', function(){
		var edName = $(this).find('.edName').val();
		var edQty = $(this).find('.edQty').val();
		var edNote = $(this).find('.edNote').val();
		var aryIndex = $(this).find('.aryIndex').val();
		var edItem = $(this).closest('.edItem').val();
		$('#editArgmnt .setAryIndex').val(aryIndex);
		var max_fields      = 99999;
		var wrapper         = $(".input_fields_wrap");
		var add_button      = $(".add_field_button");
		var x = 1;
		var fMainClass= $("#editArgmnt .col-sm-10.input_fields_wrap");
		fMainClass.children(".newGroup").each(function(){
			$(this).children("a").click();
		});
		//alert(edQty);
		$(".edArgName").val(edName);
		$(".edArgQty").val(edQty);
		$(".editargNote").val(edNote);
		
		var editTabletbody = $(this).parent().parent().next().next("table").children("tbody");
		var countItem = editTabletbody.children("tr").length;
		for(var i = 1; i<countItem; i++){
			if(x < max_fields){
				x++;
				$(wrapper).append('<div class="newGroup"><?php item_fields(get_current_user_id()); ?><a href="#" class="remove_field">Remove</a></div>'); //add input box

			}
		}
		var fFlower = editTabletbody.children("tr:nth-child(1)").find('td').eq(0).html();
		var fQuantity = editTabletbody.children("tr:nth-child(1)").find('td').eq(1).html();
		var fCost = editTabletbody.children("tr:nth-child(1)").find('td').eq(2).html();
		var fType = editTabletbody.children("tr:nth-child(1)").find('td').eq(3).children("span").html();
		var addFlower = new Array();
		var addQuantity = new Array();
		var addCost = new Array();
		var addType = new Array();

		editTabletbody.children("tr").each(function(){
			addFlower.push($(this).find('td').eq(0).html());
			addQuantity.push($(this).find('td').eq(1).html());
			addCost.push($(this).find('td').eq(2).html());
			addType.push($(this).find('td').eq(3).children("span").html());
		}); 
		var n = 0;
		fMainClass.children().children("select").find("option").each(function(){
			
			if($(this).val()==fFlower){
				//alert($(this).val());
				$(this).prop("selected", true);
				n = n+1;
				
			}
			if(n<=0 && $(this).val()=="Other"){
				$(this).prop("selected", true);
				$(this).parent("select").parent().next().next(".xt-col-3").after("<div class='xt-col-4'><input type='text' value='"+fFlower+"'' class='form-control mbot5 flow_other_name' name='flow_name[]'></div>")
				$(this).removeAttr('name');
			}
			
		}); 
		fMainClass.children().children(".flow_cost").val(fCost);
		fMainClass.children().children(".flow_qty").val(fQuantity);
		fMainClass.children().children(".itemType").val(fType);
		var j = 1; 
		var new_n = [];
		// alert(addFlower);
		fMainClass.children(".newGroup").each(function(){
			new_n[j] = 0;	
			$(this).children().children("select").find("option").each(function(){
				 
				if($(this).val()==addFlower[j]){
					//alert($(this).val());
					$(this).prop("selected", true);
					new_n[j] = new_n[j]+1;
				}
				if(new_n[j]<=0 && $(this).val()=="Other"){
					$(this).prop("selected", true);
					$(this).parent("select").parent().next().next(".xt-col-3").after("<div class='xt-col-4'><input type='text' value='"+addFlower[j]+"'' class='form-control mbot5 flow_other_name' name='flow_name[]'></div>")
					$(this).removeAttr('name');
				}
			});
			$(this).children().children(".flow_cost").val(addCost[j]);
			$(this).children().children(".flow_qty").val(addQuantity[j]);
			$(this).children().children(".itemType").val(addType[j]);
			j++;
			$(this).children().children('select').select2();	
		});
		$("select").select2();
		//alert(editTabletbody.children("tr:nth-child(1)").html());
		//alert(editTabletbody.children("tr").length);
	});
});
function edit_arng(){
	var argName = $('#editArgmnt .edArgName').val();
	var argNumber = $('#editArgmnt .edArgQty').val();
	var argNote = $('#editArgmnt .editargNote').val();
	var argNote = $('#editArgmnt .editargNote').val();
	
	var aryIndex = $('#editArgmnt .setAryIndex').val();
	
	var fnameArr = [];
	$('#editArgmnt select.flow_name').each(function(){
		var flow_name = $(this).val();
		if(flow_name=="Other"){
			fnameArr.push($(this).parent().next().next().next().children(".flow_other_name").val()); 	
		}else{
			fnameArr.push(flow_name); 	
		}
	});
	
	var fcostArr = [];
	$('#editArgmnt input.flow_cost').each(function(){
		var flow_cost = $(this).val();
		fcostArr.push(flow_cost); 
	});
	
	var fqtyArr = [];
	$('#editArgmnt input.flow_qty').each(function(){
		var flow_qty = $(this).val();
		fqtyArr.push(flow_qty); 
	});

	var ftypeArr = [];
	$('#editArgmnt input.itemType').each(function(){
		var flow_type = $(this).val();
		ftypeArr.push(flow_type); 
	});
	
	var flow_name = fnameArr;
	var flow_cost = fcostArr;
	var flow_qty = fqtyArr;
	var flow_type = ftypeArr;
	var eid = "<?php echo $_GET['eid']; ?>";
	
	jQuery.ajax({
	type:"post",
	url: "<?php bloginfo('url'); ?>/wp-admin/admin-ajax.php",
	data: {action: 'edit_arrangement', argName:argName,  argQty:argNumber, flow_name:flow_name , eid:eid, flow_cost:flow_cost, 
	flow_qty:flow_qty, flow_type:flow_type, ary_index:aryIndex, argNote:argNote},
	success:function(edata){
		//alert(edata);
		location.reload();
	}
	});
	return false;
}
function del_arrangement(){
	var ary_index = $("#editArgmnt .setAryIndex").val();
	var eid = "<?php echo $_GET['eid']; ?>";
	if(confirm("Are you sure you want to delete Arrangement")){
	jQuery.ajax({
	type:"post",
	url: "<?php bloginfo('url'); ?>/wp-admin/admin-ajax.php",
	data: {action: 'delete_arrangement', eid:eid, ary_index:ary_index},
	success:function(edata){
		//alert(edata);
		location.reload();
	}
	});
	return false;
	}
}
</script>

<?php 
	$user_ID = get_current_user_id();
	echo $eventTitle = the_xt_event($_GET['eid'], $user_ID);
	if(!empty($eventTitle)){
		echo $eventTitle; 
	} else { 
		 echo "<script>
		 window.location = '".get_site_url()."/events';
		 </script>";
	} 
	?>
<!--main content start-->
<section id="main-content">
          <section class="wrapper">
          	<div class="col-lg-4">
           	 <!--<h3><i class="fa fa-angle-right"></i> StemCounter</h3>-->
           	 <h4>Event: "<?php echo $eventTitle; ?>"</h4>
             </div>
             <div class="col-lg-2"></div>
             <div class="col-lg-2">

			<button type="button" align="right" style="margin-top: 10px; margin-bottom:10px; width:100%;" 
			class="btn btn-primary btn-lg" id="add_new_stemcounter" data-toggle="modal" data-target="#newArgmnt">
			New Arrangement
			</button>
            </div>
             <div class="col-lg-2">
            	<button type="button" align="right" style="margin-top: 10px; margin-bottom:10px; width:100%;" 
				class="btn btn-primary btn-lg" id="add_new_hardgood" data-toggle="modal" data-target="#addHardgoods">
				New Hardgood
				</button>
             </div>
             <div class="col-lg-2">
            	<button type="button" align="right" style="margin-top: 10px; margin-bottom:10px; width:100%;" 
				class="btn btn-primary btn-lg" id="add_new_flower" data-toggle="modal" data-target="#addNewFlower">
				New Flower
				</button>
             </div>

            <div class="row mt">
                <div class="col-md-12" id="m-container">
                <?php
                    
                    global $wpdb;
                    $user_ID = get_current_user_id();
                    $event_ID = $_GET['eid'];
                    $event_details = $wpdb->get_results("SELECT * FROM wp_user_events WHERE user = ".$user_ID." AND event_id = ".$event_ID) 
                    or die(mysql_error());
                    foreach($event_details as $event_detail){
                    $arrangements = json_decode($event_detail->event_order);
                    if($arrangements){
                    $ar= 0;
                        
                        foreach($arrangements as $arrangement){
                    ?>
                <!--Each arrangement-->
            
                <div class="content-panel content-panel-stem-count col-md-6 item">
                    <div>
                        <div class="pull-right col-md-1 edit-bqt-wrap">
                        <button class="btn btn-primary btn-xs edit-bqt"data-toggle="modal" data-target="#editArgmnt">
                        <i class="fa fa-pencil"></i>
                        <input type="hidden" class="edQty" value="<?php echo $arrangement->qty; ?>" />
                        <input type="hidden" class="aryIndex" value="<?php echo $ar; ?>" />
                        <input type="hidden" class="edName" value="<?php echo $arrangement->arrangement; ?>" />
                        <input type="hidden" class="edNote" value="<?php echo $arrangement->arrangement_note; ?>" />
                        </button>
                        </div> 
                        <div><h4><?php echo stripslashes($arrangement->arrangement);?></h4></div>
                    </div>
                    <div><h5>Quantity: <?php echo $arrangement->qty; ?></h5></div>
                    <table class="table table-striped table-advance table-hover arrangement-tables">
                        <thead>
                            <tr>
                              <th><i class="fa fa-bullhorn"></i> Item</th>
                              <th>Quantity</th>
                              <th>Cost</th>
                              <th>Type</th>
                              <th></th>
                            </tr>
                         </thead>
                        <tbody>
                  
                         <?php 
                        $items = $arrangement->items;
                        foreach($items as $item => $details){
                    
                        //Add items as the loop goes through
                        $price += $details->qty * $details->cost;
                         ?>
                        <tr>
                        <td><?php echo $item; if($item->color!=""){echo " - ".$item->color; } ?></td>
                        <td><?php echo $details->qty; ?></td>
                        <td><?php echo $details->cost; ?></td>
                        <td><span class="label label-info label-mini"><?php echo $details->type; ?></span></td>           
                        </tr>
                                          
                        <?php } ?> 
                         </tbody>
                        
                        <tfoot>
                        <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                        <!--Print price-->
                        Arrangement Cost = <?php echo $price;	?>
                        </td>
                      
                        </tr>
                        </tfoot>
                    </table>
                    </div>
                    <?php 
                    $price = 0;
                    $ar++; 
                    }
                    }else{
                        echo "<h2>No Arrangements</h2>";
                    }
                    }//endforeach;  ?>
                  
                   
                  </div><!-- /col-md-12 -->
               </div><!-- /row -->

		</section><!--/wrapper -->
      </section><!-- /MAIN CONTENT -->
      <!--main content end-->
<!-- Modal -->
<?php get_footer(); ?>

<script>
	jQuery(document).ready(function(){
		var container = document.querySelector('#m-container');
		var msnry = new Masonry( container, {
  		// options
  		columnWidth: 100,
  		itemSelector: '.item'
		});
	});
</script>