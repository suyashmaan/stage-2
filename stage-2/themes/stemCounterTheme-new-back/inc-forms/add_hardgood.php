<div class="modal fade" id="addHardgoods" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Add New Hardgood</h4>
			</div>
			<div class="modal-body">
			<!-- <div class="form-panel">-->
				<form class="form-horizontal style-form" id="newHardgoodForm" method="post" onsubmit="return add_new_hardgood();">
					<div class="form-group">
						<label class="col-sm-2 col-sm-2 control-label">Hardgood*</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" id="hardgoodName">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 col-sm-2 control-label">Description</label>
						<div class="col-sm-8">
							<textarea class="form-control" id="hardgoodDesc"></textarea>	
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 col-sm-2 control-label">Number Available</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" id="hardgoodAvl">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 col-sm-2 control-label">Cost Per Hardgood</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" id="hardgoodCost">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 col-sm-2 control-label">Hardgood Type</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" id="hardgoodType" value="Hardgood" disabled="disabled">
						</div>
					</div>
					<div class="modal-footer">
						<div class="pull-left">
						*required
						</div>
						<button type="button" class="btn btn-default" data-dismiss="modal">
						Cancel
						</button>
						<button type="submit" name="action" value="addCustomer" class="btn btn-primary" >
						Let's Go!
						</button>
					</div>
				</form>

			</div>
		</div>
	</div>     
</div>

<script>
	/*Ajax Function for Add Hardgood*/
	function add_new_hardgood(){
		var uhardgoodId = $("#userHardgoodId").val();
		var iname = $("#hardgoodName").val();
		var idesc = $("#hardgoodDesc").val();
		var iavl = $("#hardgoodAvl").val();
		var icost = $("#hardgoodCost").val();
		var itype = $("#hardgoodType").val();
		var cuser = "<?php echo $current_user->ID; ?>";

		if(iname !=""){
			jQuery.ajax({
			type:"post",
			url: "<?php bloginfo('url'); ?>/wp-admin/admin-ajax.php",
			data: {action: 'add_user_hardgood', uid:cuser, iname: iname, idesc:idesc, iavl:iavl, icost:icost, itype:itype},
			success:function(resp){
				//alert(resp);
				location.reload();
			}
			
			});//ajax
		}
		return false;
	}
</script>