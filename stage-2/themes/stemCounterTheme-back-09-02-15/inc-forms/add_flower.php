<div class="modal fade" id="addNewFlower" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">Add New Event</h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal style-form" id="newCustomerForm" method="post" onsubmit="return add_flower(this);">

						<div class="form-group">
							<label class="col-sm-2 col-sm-2 control-label">Flower</label>
							<div class="col-sm-8">
								<input type="text" name="venue" class="form-control" id="FlowerName">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 col-sm-2 control-label">Colors</label>
							<div class="col-sm-8">
								<input type="text" name="venue" class="form-control" id="FlowerColor">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 col-sm-2 control-label">Stems Per Bunch</label>
							<div class="col-sm-8">
								<input type="text" name="venue" class="form-control" id="FlowerBranch">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 col-sm-2 control-label">Cost</label>
							<div class="col-sm-8">
								<input type="text" name="venue" class="form-control" id="FlowerCost">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 col-sm-2 control-label">Type</label>
							<div class="col-sm-8">
								<input type="text" name="venue" class="form-control" value="Flower" disabled id="FlowerType">
							</div>
						</div>

						<div class="modal-footer">
							<div class="pull-left">
							*required
							</div>
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
							<button type="submit" name="action" value="addCustomer" class="btn btn-primary" >Let's Go!</button>
						</div>
					</form>
				</div>
			</div>
		</div>     
	</div>

<script>
	function add_flower(){
	var uid = "<?php echo $current_user->ID; ?>";
	var name = $("#FlowerName").val();
	var color = $("#FlowerColor").val();
	var branch = $("#FlowerBranch").val();
	var cost = $("#FlowerCost").val();
	var type = $("#FlowerType").val();
	if(name != ""){
		jQuery.ajax({
			type:"post",
			url: "<?php bloginfo('url'); ?>/wp-admin/admin-ajax.php",
			data: {action: 'add_new_flower', uid:uid, name: name, color: color, branch:branch, cost:cost, type:type},
			success:function(edata){
				//alert(edata);
				location.reload();
			}
		});
	}		
		return false;
}//add_flower()
</script>