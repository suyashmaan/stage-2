<div class="modal fade" id="editArgmnt" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">Edit Arrangement</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal style-form" method="post" onsubmit="return edit_arng();">
					<input type="hidden" class="setAryIndex" value="" />	
					<input type="hidden" id="editEveID" value="" />
					<div class="form-group">
						<label class="col-sm-8">Arrangement name*</label>
						<div class="col-sm-8">
							<input type="text" class="form-control edArgName">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-8">No. of Arrangement</label>
						<div class="col-sm-8">
							<input type="text" class="form-control edArgQty">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-8">Items</label>
						<div class="col-sm-10 input_fields_wrap">
							<?php item_fields(get_current_user_id()); ?>
							<button class="add_field_button">Add More Fields</button>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-8">Arrangement Note.</label>
						<div class="col-sm-8">
							<textarea name="editargNote" class="form-control editargNote"></textarea>	
						</div>
					</div>

					<div class="modal-footer">
						<div class="pull-left">*required</div>
						<button type="button" class="btn btn-default" onclick="return del_arrangement();">Delete</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						<button type="submit" value="addCustomer" class="btn btn-primary" >Let's Go!</button>
					</div>
				</form>
			</div>
	    </div> 
	</div>
</div>
<script>
$(".edit-bqt").on('click', function(){
	var edName = $(this).find('.edName').val();
	var edQty = $(this).find('.edQty').val();
	var edNote = $(this).find('.edNote').val();
	var aryIndex = $(this).find('.aryIndex').val();
	var edItem = $(this).closest('.edItem').val();
	$('#editArgmnt .setAryIndex').val(aryIndex);
	var max_fields      = 99999;
	var wrapper         = $(".input_fields_wrap");
	var add_button      = $(".add_field_button");
	var x = 1;
	var fMainClass= $("#editArgmnt .col-sm-10.input_fields_wrap");
	fMainClass.children(".newGroup").each(function(){
		$(this).children("a").click();
	});
	//alert(edQty);
	$(".edArgName").val(edName);
	$(".edArgQty").val(edQty);
	$(".editargNote").val(edNote);
	
	var editTabletbody = $(this).parent().parent().next().next("table").children("tbody");
	var countItem = editTabletbody.children("tr").length;
	for(var i = 1; i<countItem; i++){
		if(x < max_fields){
			x++;
			$(wrapper).append('<div class="newGroup"><?php item_fields(get_current_user_id()); ?><a href="#" class="remove_field">Remove</a></div>'); //add input box

		}
	}
	var fFlower = editTabletbody.children("tr:nth-child(1)").find('td').eq(0).html();
	var fQuantity = editTabletbody.children("tr:nth-child(1)").find('td').eq(1).html();
	var fCost = editTabletbody.children("tr:nth-child(1)").find('td').eq(2).html();
	var fType = editTabletbody.children("tr:nth-child(1)").find('td').eq(3).children("span").html();
	var addFlower = new Array();
	var addQuantity = new Array();
	var addCost = new Array();
	var addType = new Array();

	editTabletbody.children("tr").each(function(){
		addFlower.push($(this).find('td').eq(0).html());
		addQuantity.push($(this).find('td').eq(1).html());
		addCost.push($(this).find('td').eq(2).html());
		addType.push($(this).find('td').eq(3).children("span").html());
	}); 
	var n = 0;
	fMainClass.children().children("select").find("option").each(function(){
		
		if($(this).val()==fFlower){
			//alert($(this).val());
			$(this).prop("selected", true);
			n = n+1;
			
		}
		if(n<=0 && $(this).val()=="Other"){
			$(this).prop("selected", true);
			$(this).parent("select").parent().next().next(".xt-col-3").after("<div class='xt-col-4'><input type='text' value='"+fFlower+"'' class='form-control mbot5 flow_other_name' name='flow_name[]'></div>")
			$(this).removeAttr('name');
		}
		
	}); 
	fMainClass.children().children(".flow_cost").val(fCost);
	fMainClass.children().children(".flow_qty").val(fQuantity);
	fMainClass.children().children(".itemType").val(fType);
	var j = 1; 
	var new_n = [];
	// alert(addFlower);
	fMainClass.children(".newGroup").each(function(){
		new_n[j] = 0;	
		$(this).children().children("select").find("option").each(function(){
			 
			if($(this).val()==addFlower[j]){
				//alert($(this).val());
				$(this).prop("selected", true);
				new_n[j] = new_n[j]+1;
			}
			if(new_n[j]<=0 && $(this).val()=="Other"){
				$(this).prop("selected", true);
				$(this).parent("select").parent().next().next(".xt-col-3").after("<div class='xt-col-4'><input type='text' value='"+addFlower[j]+"'' class='form-control mbot5 flow_other_name' name='flow_name[]'></div>")
				$(this).removeAttr('name');
			}
		});
		$(this).children().children(".flow_cost").val(addCost[j]);
		$(this).children().children(".flow_qty").val(addQuantity[j]);
		$(this).children().children(".itemType").val(addType[j]);
		j++;
		$(this).children().children('select').select2();	
	});
	$("select").select2();
	//alert(editTabletbody.children("tr:nth-child(1)").html());
	//alert(editTabletbody.children("tr").length);
});
function edit_arng(){
	var argName = $('#editArgmnt .edArgName').val();
	var argNumber = $('#editArgmnt .edArgQty').val();
	var argNote = $('#editArgmnt .editargNote').val();
	var argNote = $('#editArgmnt .editargNote').val();
	
	var aryIndex = $('#editArgmnt .setAryIndex').val();
	
	var fnameArr = [];
	$('#editArgmnt select.flow_name').each(function(){
		var flow_name = $(this).val();
		if(flow_name=="Other"){
			fnameArr.push($(this).parent().next().next().next().children(".flow_other_name").val()); 	
		}else{
			fnameArr.push(flow_name); 	
		}
	});
	
	var fcostArr = [];
	$('#editArgmnt input.flow_cost').each(function(){
		var flow_cost = $(this).val();
		fcostArr.push(flow_cost); 
	});
	
	var fqtyArr = [];
	$('#editArgmnt input.flow_qty').each(function(){
		var flow_qty = $(this).val();
		fqtyArr.push(flow_qty); 
	});

	var ftypeArr = [];
	$('#editArgmnt input.itemType').each(function(){
		var flow_type = $(this).val();
		ftypeArr.push(flow_type); 
	});
	
	var flow_name = fnameArr;
	var flow_cost = fcostArr;
	var flow_qty = fqtyArr;
	var flow_type = ftypeArr;
	var eid = "<?php echo $_GET['eid']; ?>";
	
	jQuery.ajax({
	type:"post",
	url: "<?php bloginfo('url'); ?>/wp-admin/admin-ajax.php",
	data: {action: 'edit_arrangement', argName:argName,  argQty:argNumber, flow_name:flow_name , eid:eid, flow_cost:flow_cost, 
	flow_qty:flow_qty, flow_type:flow_type, ary_index:aryIndex, argNote:argNote},
	success:function(edata){
		//alert(edata);
		location.reload();
	}
	});
	return false;
}
function del_arrangement(){
	var ary_index = $("#editArgmnt .setAryIndex").val();
	var eid = "<?php echo $_GET['eid']; ?>";
	if(confirm("Are you sure you want to delete Arrangement")){
	jQuery.ajax({
	type:"post",
	url: "<?php bloginfo('url'); ?>/wp-admin/admin-ajax.php",
	data: {action: 'delete_arrangement', eid:eid, ary_index:ary_index},
	success:function(edata){
		//alert(edata);
		location.reload();
	}
	});
	return false;
	}
}
</script>