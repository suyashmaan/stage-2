<?php get_header(); ?>
<?php get_sidebar(); ?>
<!--MAIN CONTENT-->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<!--<h3><i class="fa fa-angle-right"></i> My Items</h3>-->

<div class="col-lg-2">
<!--<h3><i class="fa fa-angle-right"></i>My Items</h3>-->
</div>
<div class="col-lg-10">
<button class="btn btn-primary btn-lg pull-right" data-toggle="modal" data-target="#addItems" style="margin-top: 10px; margin-bottom:10px;">
Add Items
</button>
</div>
        
              <div class="row mt">
                  <div class="col-md-12">
                      <div class="content-panel">
                          <table class="table table-striped table-advance table-hover">
                              <thead>
                              <tr>
                                  <th>Item</th>
                                  <th>Description</th>
                                  <th>Number Available</th>
                                  <th>Cost Per</th>
                                  <th>Type</th>
                                  <th></th>
                              </tr>
                              </thead>
                              <tbody>
							<?php
							$user_id = get_current_user_id();
							$items = $wpdb->get_results("SELECT * FROM `wp_user_items` WHERE user_id = $user_id && user_item_type != 'hardgood'", ARRAY_A); // get user items
							
							if($items){
							$i = 0;
							foreach ($items as $row){ ?>
							<tr class="row_item_<?php echo $i; ?>">
								<td><a href="basic_table.html#"><?php echo $row['user_item']; ?></a></td>
								<td class="hidden-phone"><?php echo $row['user_description']; ?></td>
								<td><?php echo $row['user_item_avl']; ?></td>
								<td><?php echo $row['user_cost']; ?></td>
								<td><?php echo $row['user_item_type']; ?></td>
								<td>
								<button class="btn btn-primary btn-xs" onclick="return edit_item(<?php echo $row[userItemId]; ?>,'<?php echo $row[user_item]; ?>','<?php echo $row[user_description]; ?>','<?php echo $row[user_item_avl]; ?>','<?php echo $row[user_cost]; ?>','<?php echo $row[user_item_type]; ?>');" itemID="<?php echo $row[userItemId]; ?>" data-toggle="modal" data-target="#editItems">
									<i class="fa fa-pencil"></i>
									<input type="hidden" class="myItem" value="<?php echo $row['user_item']; ?>" />
									<input type="hidden" class="myDescription" value="<?php echo $row['user_description']; ?>" />
									<input type="hidden" class="myNuAva" value="<?php echo $row['user_item_avl']; ?>" />
									<input type="hidden" class="myCostPer" value="<?php echo $row['user_cost']; ?>" />
									<input type="hidden" class="myType" value="<?php echo $row['user_item_type']; ?>" />
								</button>
								<button class="btn btn-danger btn-xs" onclick="return delete_item(this);" value="<?php echo $row[userItemId]; ?>">
								<i class="fa fa-trash-o "></i>
								</button>
								</td>
							</tr>
							<?php 
							$i++; }
							}else{
								echo "<h2>No Items</h2>";
							} ?>
                              </tbody>
                          </table>
                      </div><!-- /content-panel -->
                  </div><!-- /col-md-12 -->
              </div><!-- /row -->

		</section><!--/wrapper -->
      </section><!-- /MAIN CONTENT -->

<!--main content end-->

<!--Add New Item Form Start-->
<div class="modal fade" id="addItems" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id="myModalLabel">Add New Item</h4>
	      </div>
	      <div class="modal-body">
         <!-- <div class="form-panel">-->
		<form class="form-horizontal style-form" id="newItemForm" method="post" onsubmit="return add_new_item();">
		<div class="form-group">
		<label class="col-sm-2 col-sm-2 control-label">Item*</label>
		<div class="col-sm-8">
		<input type="text" class="form-control" id="itemName">
		</div>
		</div>
		
		<div class="form-group">
		<label class="col-sm-2 col-sm-2 control-label">Description</label>
		<div class="col-sm-8">
		<textarea class="form-control" id="itemDesc"></textarea>	
		</div>
		</div>
		
		<div class="form-group">
		<label class="col-sm-2 col-sm-2 control-label">Number Available</label>
		<div class="col-sm-8">
		<input type="text" class="form-control" id="itemAvl">
		</div>
		</div>
		
		<div class="form-group">
		<label class="col-sm-2 col-sm-2 control-label">Cost Per Item</label>
		<div class="col-sm-8">
		<input type="text" class="form-control" id="itemCost">
		</div>
		</div>
		
		<div class="form-group">
		<label class="col-sm-2 col-sm-2 control-label">Item Type</label>
		<div class="col-sm-8">
		<select class="form-control flow_type mbot5 item_type" name="itemType" id="itemType">
			<option value="">Select</option>
			<option value="Fee">Fee</option>
			<option value="Base Price">Base Price</option>
			<option value="Rental">Rental</option>
		</select>
		<!-- <input type="text" class="form-control" id="itemType"> -->
		</div>
		</div>
		<div class="modal-footer">
		<div class="pull-left">
		*required
		</div>
		<button type="button" class="btn btn-default" data-dismiss="modal">
		Cancel
		</button>
		<button type="submit" name="action" value="addCustomer" class="btn btn-primary" >
		Let's Go!
		</button>
		</div>
		</form>
	      
	    </div>
	  </div>
	</div>     
</div>
<!--//Add New Item Form End-->

<!--Edit Item Form Start-->
<div class="modal fade" id="editItems" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id="myModalLabel">Edit This Item</h4>
	      </div>
	      <div class="modal-body">
         <!-- <div class="form-panel">-->
		<form class="form-horizontal style-form" id="newItemForm" method="post" onsubmit="return edit_new_item();">
		<input value="" type="hidden" id="itemID" name="itemID">
			<div class="form-group">
				<label class="col-sm-2 col-sm-2 control-label">Item*</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" id="itemName">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-2 col-sm-2 control-label">Description</label>
				<div class="col-sm-8">
					<textarea class="form-control" id="itemDesc"></textarea>	
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-2 col-sm-2 control-label">Number Available</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" id="itemAvl">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-2 col-sm-2 control-label">Cost Per Item</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" id="itemCost">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-2 col-sm-2 control-label">Item Type</label>
				<div class="col-sm-8">
				<select class="form-control flow_type mbot5 item_type" name="itemType" id="itemType">
					<option value="">Select</option>
					<option value="Fee">Fee</option>
					<option value="Base Price">Base Price</option>
					<option value="Rental">Rental</option>
				</select>
					<!-- <input type="text" class="form-control" id="itemType"> -->
				</div>
			</div>
			<div class="modal-footer">
				<div class="pull-left">
				*required
				</div>
				<button type="button" class="btn btn-default" data-dismiss="modal">
					Cancel
				</button>
				<button type="submit" name="action" value="addCustomer" class="btn btn-primary" >
					Let's Go!
				</button>
			</div>
		</form>
	      
	    </div>
	  </div>
	</div>     
</div><!--//Edit Item Form Start-->

<script>
/*Ajax Function for Add Item*/
function add_new_item(){
	var uitemId = $("#userItemId").val();
	var iname = $("#itemName").val();
	var idesc = $("#itemDesc").val();
	var iavl = $("#itemAvl").val();
	var icost = $("#itemCost").val();
	var itype = $("#itemType").val();
	var cuser = "<?php echo $current_user->ID; ?>";
	if(iname !=""){
		jQuery.ajax({
		type:"post",
		url: "<?php bloginfo('url'); ?>/wp-admin/admin-ajax.php",
		data: {action: 'add_user_item', uid:cuser, iname: iname, idesc:idesc, iavl:iavl, icost:icost, itype:itype},
		success:function(resp){
			//alert(resp);
			location.reload();
		}
		
		});//ajax
	}
	return false;
}

/*Ajax Function for Delete Item*/
function delete_item(obj){
	var itm_index = $(obj).attr('value');
	var cuser = "<?php echo $current_user->ID; ?>";
	var del_erow = $(obj).parents("tr");
	var del_row_class = "row_item_"+itm_index;
	
	if(confirm("Are you sure you want to delete Item")){
		jQuery.ajax({
		type:"post",
		url: "<?php bloginfo('url'); ?>/wp-admin/admin-ajax.php",
		data: {action: 'delete_user_item', uid:cuser, itm_index:itm_index},
		success:function(resp){
			if(resp == del_row_class){
					del_erow.fadeOut(500).remove();
				}
		}
		});//ajax
	}
}


/*Get data in popup*/
function edit_item(row_obj,myItem,myDescription,myNuAva,myCostPer,myType){
	$("#editItems #itemID").val("");
	$("#editItems #itemName").val("");
	$("#editItems #itemDesc").html("");
	$("#editItems #itemAvl").val("");
	$("#editItems #itemCost").val("");
	$("#editItems #itemType").val("");

	//add values
	$("#editItems #itemID").val(row_obj);
	$("#editItems #itemName").val(myItem);
	$("#editItems #itemDesc").html(myDescription);
	$("#editItems #itemAvl").val(myNuAva);
	$("#editItems #itemCost").val(myCostPer);
	$("#editItems #itemType").val(myType);
}

/*Ajax Function for Edit Item*/
function edit_new_item(){
	var iItemID = $("#editItems #itemID").val();
	var iname = $("#editItems #itemName").val();
	var idesc = $("#editItems #itemDesc").val();
	var iavl = $("#editItems #itemAvl").val();
	var icost = $("#editItems #itemCost").val();
	var itype = $("#editItems #itemType").val();
	var cuser = "<?php echo $current_user->ID; ?>";
	if(iname !=""){
		jQuery.ajax({
		type:"post",
		url: "<?php bloginfo('url'); ?>/wp-admin/admin-ajax.php",
		data: {action: 'edit_user_item', uid:cuser, iItemID:iItemID, iname: iname, idesc:idesc, iavl:iavl, icost:icost, itype:itype},
		success:function(resp){
			//alert(resp);
			location.reload();
		}
		
		});//ajax
	}
	return false;
}

</script>
<?php get_footer(); ?>