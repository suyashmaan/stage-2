<?php
/*Ajax Function for Add User Items*/
function add_user_item(){
	global $wpdb;
	$userItemId = $_POST['itemId'];
	$uid = $_POST['uid'];
	$iname = $_POST['iname'];
	$idesc = $_POST['idesc'];
	$iavl = $_POST['iavl'];
	$icost = $_POST['icost'];
	$itype = $_POST['itype'];
	/*$iarray[] = array("item"=>$iname, "description"=>$idesc, "item_avl"=>$iavl, "item_cost"=>$icost, "item_type"=>$itype);
	$iarray_json = json_encode($iarray);
	
	$check_query = $wpdb->query("SELECT * FROM `wp_user_items` WHERE user_id = $uid", ARRAY_A);
	if($check_query ==1){
		$wpdb->update("wp_user_items", array("user_items"=>$mrg_array), array("user_id"=>$uid));
		$wpdb->insert("wp_user_items", array("user_id"=>$uid, "user_item"=>$iname, "user_description"=>$idesc, "user_item_avl"=>$iavl, "user_cost"=>$icost, "user_item_type"=>$itype));
		
		echo "Record Updated Entered";
	}else{*/
	$iquery = $wpdb->insert("wp_user_items", array("user_id"=>$uid, "user_item"=>$iname, "user_description"=>$idesc, "user_item_avl"=>$iavl, "user_cost"=>$icost, "user_item_type"=>$itype));
	
	if($iquery){
		echo "Record Entered";
	}
	// }
	die(); 
}
add_action('wp_ajax_add_user_item', 'add_user_item');
add_action('wp_ajax_nopriv_add_user_item', 'add_user_item');

/*Ajax Function for Edit User Items*/
function edit_user_item(){
	global $wpdb;
	$itemID = $_POST['iItemID'];
	$uid = $_POST['uid'];
	$iname = $_POST['iname'];
	$idesc = $_POST['idesc'];
	$iavl = $_POST['iavl'];
	$icost = $_POST['icost'];
	$itype = $_POST['itype'];
	/*$iarray = array("item"=>$iname, "description"=>$idesc, "item_avl"=>$iavl, "item_cost"=>$icost, "item_type"=>$itype);
	$iarray_json = json_encode($iarray);
	
	$check_query = $wpdb->get_results("SELECT `user_items` FROM `wp_user_items` WHERE user_id = $uid", ARRAY_A);
	$cd_val = json_decode($check_query[0]['user_items']);*/
	//print_r($cd_val);
	//print_r($iarray);
	/*$cd_val[$itemID] = $iarray;
	$updateData = json_encode($cd_val);*/
	$res = $wpdb->update("wp_user_items", array("user_id"=>$uid, "user_item"=>$iname, "user_description"=>$idesc, "user_item_avl"=>$iavl, "user_cost"=>$icost, "user_item_type"=>$itype), array("userItemId"=>$itemID));
	// $res = $wpdb->update("wp_user_items", array("user_items" => $updateData), array('user_id'=>$uid));
	//if($res){
		echo "Record updated successfully!!";
	//}
	die(); 

}
add_action('wp_ajax_edit_user_item', 'edit_user_item');
add_action('wp_ajax_nopriv_edit_user_item', 'edit_user_item');

/*Ajax Function for Delete User Items*/
function delete_user_item(){
	global $wpdb;
	$uid = $_POST['uid'];
	$itm_index = $_POST['itm_index'];
	/*$datacheck = $wpdb->get_results("SELECT `user_items` FROM `wp_user_items` WHERE user_id = $uid", ARRAY_A);
	$cd_val = json_decode($datacheck[0]['user_items']);
	unset($cd_val[$itm_index]);
	$cd_val = array_values($cd_val);
	$new_itm_arg = json_encode($cd_val);
	$res = $wpdb->update("wp_user_items", array("user_items" => $new_itm_arg), array('user_id'=>$uid));*/
	$res = $wpdb->delete("wp_user_items", array("userItemId"=>$itm_index));
	if($res){
		echo "row_item_".$itm_index;
	}	
	die;
}
add_action('wp_ajax_delete_user_item', 'delete_user_item');
add_action('wp_ajax_nopriv_delete_user_item', 'delete_user_item');



/*Ajax Function for Add User Items*/
function add_user_hardgood(){
	global $wpdb;
	$userItemId = $_POST['hardgoodId'];
	$uid = $_POST['uid'];
	$iname = $_POST['iname'];
	$idesc = $_POST['idesc'];
	$iavl = $_POST['iavl'];
	$icost = $_POST['icost'];
	$itype = $_POST['itype'];
	$iquery = $wpdb->insert("wp_user_hardgood", array("user_id"=>$uid, "user_item"=>$iname, "user_description"=>$idesc, "user_item_avl"=>$iavl, "user_cost"=>$icost, "user_item_type"=>$itype));
	
	if($iquery){
		echo "Record Entered";
	}
	die(); 
}
add_action('wp_ajax_add_user_hardgood', 'add_user_hardgood');
add_action('wp_ajax_nopriv_add_user_hardgood', 'add_user_hardgood');

/*Ajax Function for Edit User Hardgood*/
function edit_user_hardgood(){
	global $wpdb;
	$itemID = $_POST['iHardgoodID'];
	$uid = $_POST['uid'];
	$iname = $_POST['iname'];
	$idesc = $_POST['idesc'];
	$iavl = $_POST['iavl'];
	$icost = $_POST['icost'];
	$itype = $_POST['itype'];
	$res = $wpdb->update("wp_user_hardgood", array("user_id"=>$uid, "user_item"=>$iname, "user_description"=>$idesc, "user_item_avl"=>$iavl, "user_cost"=>$icost, "user_item_type"=>$itype), array("item_id"=>$itemID));

	echo "Record updated successfully!!";

	die(); 

}
add_action('wp_ajax_edit_user_hardgood', 'edit_user_hardgood');
add_action('wp_ajax_nopriv_edit_user_hardgood', 'edit_user_hardgood');

/*Ajax Function for Delete User Items*/
function delete_user_hardgood(){
	global $wpdb;
	$uid = $_POST['uid'];
	$itm_index = $_POST['itm_index'];
	$res = $wpdb->delete("wp_user_hardgood", array("item_id"=>$itm_index));
	if($res){
		echo "row_item_".$itm_index;
	}	
	die;
}
add_action('wp_ajax_delete_user_hardgood', 'delete_user_hardgood');
add_action('wp_ajax_nopriv_delete_user_hardgood', 'delete_user_hardgood');

?>