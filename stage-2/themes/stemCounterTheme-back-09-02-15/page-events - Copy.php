<?php get_header(); ?>
<?php get_sidebar(); ?>

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
            <div class="col-lg-2">
           	 <h3><i class="fa fa-angle-right"></i> Events <br>
             <?php $user_ID = get_current_user_id(); 
			 		echo " ".$user_ID." ";
//* /
/* Config Values - You need to know these */
//$db_host = 'localhost';
//$db_user = 'root';
//$db_pass = 'root';
//$db_name = 'local.stemcounter.com';

/* Connect to the new database */
//$externalQuery = new wpdb($db_user, $db_pass, $db_name, $db_host);

/* Call WP's "get_results" on your query and create the array */
global $wpdb;
$newQuery = $wpdb->get_results("SELECT User_Events FROM User_Events WHERE user =".$user_ID) or die(mysql_error());

print_r($newQuery);

/* Run a loop as usual on your array
foreach($newQuery as $q){
	echo $q->prodTitle.'<br>';
}*/
?>
             
             
             
             </h3>
             
             
             
             
             </div>
             <div class="col-lg-8"></div>
             <div class="col-lg-2">
				<button type="button" align="right" style="margin-top: 10px; margin-bottom:10px;" class="btn btn-primary btn-lg">New Event</button>
              </div>
              <div class="row mt">
                  <div class="col-md-12">
                      <div class="content-panel">
                          <table class="table table-striped table-advance table-hover">
	                  	  	  <h4><i class="fa fa-angle-right"></i> Current Events</h4>
	                  	  	  <hr>
                              <thead>
                              <tr>
                                  <th><i class="fa fa-bullhorn"></i> Event</th>
                                  <th><i class="fa fa-question-circle"></i> Date</th>
                                  <th><i class="fa fa-bullhorn"></i> Contact</th>
                                  <th><i class="fa fa-bullhorn"></i> Venue</th>
                                  <th><i class=" fa fa-edit"></i> Status</th>
                                  <th><i class="fa fa-bullhorn"></i> Action</th>
                                  <th></th>
                              </tr>
                              </thead>
                              <tbody>
                              <tr>
                                  <td><a href="../stem_counter">Marshall - Hendricks Wedding</a></td>
                                  <td class="hidden-phone">5/5/2015</td>
                                  <td>Sarah Hendricks </td>
                                  <td>Coronado </td>
                                  <td><span class="label label-info label-mini">Quoted</span></td>
                                  <td>
              							  <div class="btn-group">
                                          <button type="button" class="btn btn-theme03">Action</button>
                                          <button type="button" class="btn btn-theme03 dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                          </button>
                                      <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Go</a></li>
                                        <li><a href="#">Archive</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Delete</a></li>
                                      </ul>
                                    </div>      		
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      <a href="Events/stem_counter.html">
                                          Arch Grants Gala
                                      </a>
                                  </td>
                                  <td class="hidden-phone">12/29/14</td>
                                  <td>Susan Weremyer </td>
                                  <td>Hyatt Arch </td>
                                  <td><span class="label label-success label-mini">Booked</span></td>
                                  <td>
                                      <div class="btn-group">
                                          <button type="button" class="btn btn-theme03">Action</button>
                                          <button type="button" class="btn btn-theme03 dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                          </button>
                                      <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Go</a></li>
                                        <li><a href="#">Archive</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Delete</a></li>
                                      </ul>
                                    </div>   
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      <a href="Events/stem_counter.html">
                                          Johnson - Abraham Wedding
                                      </a>
                                  </td>
                                  <td class="hidden-phone">2/29/14</td>
                                  <td>Sarah Abraham </td>
                                  <td>Jewel Box </td>
                                  <td><span class="label label-success label-mini">Booked</span></td>
                                  <td>
                                       <div class="btn-group">
                                          <button type="button" class="btn btn-theme03">Action</button>
                                          <button type="button" class="btn btn-theme03 dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                          </button>
                                      <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Go</a></li>
                                        <li><a href="#">Archive</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Delete</a></li>
                                      </ul>
                                    </div>   
                                  </td>
                              </tr>
          
                              </tbody>
                          </table>
                      </div><!-- /content-panel -->
                  </div><!-- /col-md-12 -->
              
              
              <!--ARCHIVED EVENTS-->
              </div><!-- /row -->
                  <div class="col-md-12" style="margin-top: 10px; margin-right: 20px;">
                         <a class="pull-right" href="Events/stem_counter.html">Archived Events</a>
                  </div><!-- /col-md-12 -->

		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->

<?php get_footer(); ?>