<?php get_header(); ?>
<?php get_sidebar(); ?>
<?php
wp_enqueue_script('jquery');
wp_enqueue_script('thickbox');
wp_enqueue_style('thickbox');
$user_ID = get_current_user_id();
//echo '<pre>';
$replace_zip=$current_user->wp_s2member_custom_fields; //Get User Details
$additionalInfo = unserialize($current_user->additionalInfo); //Get User Details

//echo $replace_zip[company_name];
//print_r($replace_zip);
//print_r($additionalInfo);
?>
 <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      

<script type="text/javascript">
        function user_avatar_refresh_image(img){
         jQuery('#user-avatar-display-image').html(img);
                location.reload();//reloading the page after edit/add photo to display the delete link.
        }
        function add_remove_avatar_link(){
                   /*nothing to do here, cause we are going to use our own delete link later. This function still here because of the user avatar plugin still need this*/
                }

</script>
 <?php
function delete_avatar() {
        $uid = $_POST['uid'];
        $nonce = $_POST['nonce'];
        $current_user = wp_get_current_user();

                // If user clicks the remove avatar button, in URL deleter_avatar=true
                if( isset($uid) && wp_verify_nonce($nonce, 'delete_user_avatar') && $uid == $current_user->id )
                {
                        user_avatar_delete_files($uid);//here I using user avatar own plugin delete photo function.

                }

        echo '1';//sending back to the js script to notify that the photo is deleted.
        exit;
}
// need to add these action for ajax
add_action( 'wp_ajax_nopriv_delete_avatar', 'delete_avatar');
add_action( 'wp_ajax_delete_avatar', 'delete_avatar');

//3rd step: The js files
//You will need to enqueue this js file to the page you want to use this script.
wp_register_script('profile', plugin_dir_url( __FILE__ ) .'js/profile.js',__FILE__ );
wp_enqueue_script('profile');
wp_localize_script( 'profile', 'Ajax', array( 'url' => admin_url( 'admin-ajax.php' ) ) );//this is wp ajax reference

//here is the js in profile.js
?>
<script>
jQuery(document).ready(function($) {
        $( "#user-avatar-remove" ).click(function() {
           var uid = $('#userid').val();
                   var nonce = $('#_nonce').val();     

                   jQuery.ajax({
                                 type: 'POST',
                                 url: Ajax.url,
                                 data: ({action : 'delete_avatar',uid:uid,nonce:nonce }),
                                 success: function(html) {
                                if(html){
                                        var image = $('.noimage').val();
                                         $("#user-avatar-display-image")
                                                .fadeOut(400, function() {
                                                        $("#user-avatar-display-image").find('img').attr('src',$(".noimage").val());
                                                        })
                                        .fadeIn(400);
                                        $('#user-avatar-remove').remove();
                                        }

                                 }
                                 });

        });    

});
</script>
      <section id="main-content">
          <section class="wrapper">
           	 <!--<h3><i class="fa fa-angle-right"></i> Settings</h3>-->
          	<input type="hidden" value="<?php echo $user_ID; ?>" id="userID">
          	<!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt">
                <div class="col-lg-12">
                  <div class="form-panel">
                      <h4 class="mb"><i class="fa fa-angle-right"></i> Company Details</h4>
                      <button type="button" style="margin-top: 0px; margin-bottom:5px; margin-left:10px" class="btn btn-primary btn-sm" data-toggle="modal" onclick="return editCompanyDetail(<?php echo $user_ID; ?>, '<?php echo $replace_zip[company_name]; ?>', '<?php echo $replace_zip[company_address]; ?>', '<?php echo $replace_zip[company_website]; ?>', '<?php echo $additionalInfo[companyMobile]; ?>', '');" data-target="#editCompanyDetail">Edit</button>
                      
                      <form class="form-horizontal style-form" method="get">
                          <fieldset>
                          <div class="form-group col-lg-12">
                          
                          <label class="col-lg-12 control-label">Company Name: <br> <?php echo $replace_zip[company_name]; ?></label>
                          <label class="col-lg-12 control-label"><br>Address: <br> <?php echo $replace_zip[company_address]; ?></label>
                          <label class="col-lg-12 control-label"><br>Website: <br> <?php echo $replace_zip[company_website]; ?> <br> Phone: <br><?php echo $additionalInfo[companyMobile]; ?></label>
                          
                          </div>
                          
                        </fieldset>
                        
                      </form>
                    <div id="user-avatar-display" >
						<span>Profile Picture</span>
						<p id="user-avatar-display-image">

							<?php echo user_avatar_get_avatar( $current_user->ID, 150);?>
						</p>
						<a id="user-avatar-link" class="button thickbox" href="<?php echo admin_url('admin-ajax.php'); ?>?action=user_avatar_add_photo&step=1&uid=<?php echo $current_user->ID; ?>&TB_iframe=true&width=720&height=450" title="Customize your profile picture" >Change Picture</a>

						<input type="hidden" class="noimage" value="http://0.gravatar.com/avatar/ad516503a11cd5ca435acc9bb6523536?s=150"/>

						<input type="hidden" id="userid" value="<?php echo $current_user->ID ;?>"/>
						<input type="hidden" id="_nonce" value="<?php  echo wp_create_nonce('delete_user_avatar') ;?>"/>
					</div>
              	</div>
          		</div>   	
          	</div><!-- /row -->
        
 			<div class="row mt">
                  <div class="col-lg-12">
                      <div class="form-panel">
                          <table class="table table-striped table-advance table-hover">
	                  	  	  <h4><i class="fa fa-angle-right"></i> Invoicing Settings</h4>
	                  	  	  <hr>
                              <thead>
                              <tr>
                                  <th><i class="fa fa-bullhorn"></i> Hardgood Multiple</th>
                                  <th><i class="fa fa-question-circle"></i> Fresh Flower Multiple</th>
                                  <th><i class="fa fa-bookmark"></i> % Increase in flowers ordered</th>
                                  <th><i class=" fa fa-edit"></i> Charge Card Rate</th>
                                  <th><i class=" fa fa-edit"></i> Sales Tax</th>
                                  <th><i class="fa"></i>% Delivery cost</th>
                                  <th><i class="fa"></i>% Labor</th>
                                  <th></th>
                              </tr>
                              </thead>
                              <tbody>
                              <tr>
                                  <td><?php echo $replace_zip[hardgood_multiple]; ?></td>
                                  <td><?php echo $replace_zip[fresh_flower_multiple]; ?></td>
                                  <td><?php echo $replace_zip[increase_flowers_order]; ?></td>
                                  <td><?php echo $replace_zip[__charge_card_rate]; ?></td>
                                  <td><?php echo $replace_zip[sales_tax__]; ?></td>
                                  <td><?php echo $replace_zip[__delivery_cost]; ?></td>
                                  <td><?php echo $replace_zip[__labor]; ?></td>
                                  
                                  <td>
                                      <button class="btn btn-success btn-xs"><i class="fa fa-check"></i></button>
                                      <button class="btn btn-primary btn-xs" data-toggle="modal"  onclick="return edit_invoice(<?php echo $user_ID; ?>, '<?php echo $replace_zip[hardgood_multiple]; ?>', '<?php echo $replace_zip[fresh_flower_multiple]; ?>', '<?php echo $replace_zip[increase_flowers_order]; ?>', '<?php echo $replace_zip[__charge_card_rate]; ?>', '<?php echo $replace_zip[sales_tax__]; ?>','<?php echo $replace_zip[__delivery_cost]; ?>','<?php echo $replace_zip[__labor]; ?>');" data-target="#editInvoice" ><i class="fa fa-pencil"></i></button>
                                      <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button>
                                  </td>
                              </tr>
          
                              </tbody>
                          </table>
                      </div><!-- /content-panel -->
                  </div><!-- /col-md-12 -->
              </div><!-- /row -->
          	
          	       	<!-- INLINE FORM ELELEMNTS -->
          	<div class="row mt">
          		<div class="col-lg-12">
          			<div class="form-panel">
                  	  <h4 class="mb"><i class="fa fa-angle-right"></i> Change Password</h4>
                      <form class="form-inline" role="form">
                          <div class="form-group">
                              <label class="sr-only" for="exampleInputEmail2">Password</label>
                              <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Password">
                          </div>
                          <div class="form-group">
                              <label class="sr-only" for="exampleInputPassword2">Confirm Password</label>
                              <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Confirm Password">
                          </div>
                          <button type="submit" class="btn btn-theme">Change Password</button>
                      </form>
          			</div><!-- /form-panel -->
          		</div><!-- /col-lg-12 -->
          	</div><!-- /row -->
          	
		</section><!--/wrapper -->
    </section><!-- /MAIN CONTENT -->
	<div class="modal fade" id="editCompanyDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">Edit Company Details</h4>
				</div>
				<div class="modal-body">
			 <!-- <div class="form-panel">-->
					<form class="form-horizontal style-form" id="newItemForm" method="post" onsubmit="return edit_comapany_detail_save();">
					<input type="hidden" name="userID" value="<?php echo get_current_user_id(); ?>">
					<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Profile Picture</label>
						<div class="col-sm-8">
								
								<input type="file" name="profilePic" class="form-control" id="cProfilePic">
						</div>
					</div>

						<div class="form-group">
							<label class="col-sm-2 col-sm-2 control-label">Comapny Name</label>
							<div class="col-sm-8">
								<input type="text" name="companyName" class="form-control" id="companyName">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 col-sm-2 control-label">Address</label>
							<div class="col-sm-8">
								<textarea name="companyAddress" class="form-control" id="address"></textarea>	
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 col-sm-2 control-label">website</label>
							<div class="col-sm-8">
								<input type="text" name="companyWebsite" class="form-control" id="webSite">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 col-sm-2 control-label">Phone</label>
							<div class="col-sm-8">
								<input type="text" name="companyMobile" class="form-control" id="phone">
							</div>
						</div>

						<div class="modal-footer">
							<div class="pull-left">
							*required
							</div>
							<button type="button" class="btn btn-default" data-dismiss="modal">
							Cancel
							</button>
							<button type="submit" name="action" value="addCustomer" class="btn btn-primary" >
							Let's Go!
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>     
	</div>

	<div class="modal fade" id="editInvoice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">Edit Invoicing Settings</h4>
				</div>
				<div class="modal-body">
			 <!-- <div class="form-panel">-->
					<form class="form-horizontal style-form" id="newItemForm" method="post" onsubmit="return edit_invoice_save();">
						<div class="form-group">
							<label class="col-sm-2 col-sm-2 control-label">Hardgood Multiple</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="hardgoodMultiple">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 col-sm-2 control-label">Fresh Flower Multiple</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="freshFlowerMultiple">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 col-sm-2 control-label">Increase in flower ordered</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="increaseFlowerOrdered">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 col-sm-2 control-label">Change Card Rate</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="chnageCardRate">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 col-sm-2 control-label">Sales Tax</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="saleTax">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 col-sm-2 control-label">Delivery</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="delivery">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 col-sm-2 control-label">Labor</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="labor">
							</div>
						</div>
						<div class="modal-footer">
							<div class="pull-left">
							*required
							</div>
							<button type="button" class="btn btn-default" data-dismiss="modal">
							Cancel
							</button>
							<button type="submit" name="action" value="addCustomer" class="btn btn-primary" >
							Let's Go!
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>     
	</div>

<script type="text/javascript">
	function editCompanyDetail(userID, companyName, companyAddress, companyWebsite, companyMobile, profilePic){

		$("#userID").val(userID);
		$("#companyName").val(companyName);
		$("#address").html(companyAddress);
		$("#webSite").val(companyWebsite);
		$("#phone").val(companyMobile);
		$("#profilePic").attr('src',profilePic);

	}

	function edit_invoice(userID, hardgoodMultiple, freshFlowerMultiple, increaseFlowerOrdered, chnageCardRate, saleTax, delivery, labor){

		$("#hardgoodMultiple").val(hardgoodMultiple);
		$("#freshFlowerMultiple").val(freshFlowerMultiple);
		$("#increaseFlowerOrdered").val(increaseFlowerOrdered);
		$("#chnageCardRate").val(chnageCardRate);
		$("#saleTax").val(saleTax);
		$("#delivery").val(delivery);
		$("#labor").val(labor);


	}

	function edit_comapany_detail_save(){

		var userID = $("#userID").val();
		var companyName = $("#companyName").val();
		var companyAddress = $("#address").val();
		var companyWebsite = $("#webSite").val();
		var companyMobile = $("#phone").val();
		/*var profilePic = $("cProfilePic").val();*/
		if(companyName!=""){

			$.ajax({
				type: 'POST',
				url	: "<?php bloginfo('url'); ?>/wp-admin/admin-ajax.php",
				data: {action: 'edit_comapany_detail', userID:userID, companyName:companyName, companyAddress:companyAddress, companyWebsite:companyWebsite, companyMobile:companyMobile},
				success: function(resp){
					//alert(resp);
					//return false;
					location.reload();
				}
			});
			return false;
		}
		return false;
	}

	function edit_invoice_save(){

		var userID = $("#userID").val();
		var hardgoodMultiple = $("#hardgoodMultiple").val();
		var freshFlowerMultiple = $("#freshFlowerMultiple").val();
		var chnageCardRate = $("#chnageCardRate").val();
		var saleTax = $("#saleTax").val();
		var delivery = $("#delivery").val();
		var labor = $("#labor").val();
		var increaseFlowerOrdered  = $("#increaseFlowerOrdered").val();
		if(companyName!=""){

			$.ajax({
				type: 'POST',
				url	: "<?php bloginfo('url'); ?>/wp-admin/admin-ajax.php",
				data: {action: 'edit_invoice', userID:userID, hardgoodMultiple:hardgoodMultiple, freshFlowerMultiple:freshFlowerMultiple, chnageCardRate:chnageCardRate, saleTax:saleTax, delivery:delivery, labor:labor, increaseFlowerOrdered:increaseFlowerOrdered },
				success: function(resp){
					//alert(resp);
					location.reload();
				}
			});
			return false;
		}
		return false;
	}
</script>
      <!--main content end-->

<?php get_footer(); ?>