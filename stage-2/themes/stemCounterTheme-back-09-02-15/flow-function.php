<?php 
function add_new_flower(){
	global $wpdb;
	$name = $_POST['name'];
	$uid = $_POST['uid'];
	$color = $_POST['color'];
	$branch = $_POST['branch'];
	$cost = $_POST['cost'];
	$type = $_POST['type'];
	
	$check_query = $wpdb->query("SELECT * FROM `wp_user_flowers` WHERE flower = '$name' && user_id = $uid", ARRAY_A);
	if($check_query>=1){
		echo "Flower ".$name." Already created";
	} else {
		$res = $wpdb->insert("wp_user_flowers", array("user_id" => $uid, "flower"=>$name, "color"=>$color, "stems"=>$branch, "cost"=>$cost, "type"=>$type));
		echo "Flower ".$name." added sucessfully.";	
	}
	die;
}
add_action('wp_ajax_add_new_flower', 'add_new_flower');
add_action('wp_ajax_nopriv_add_new_flower', 'add_new_flower');

function edit_flower(){
	global $wpdb;
	$name = $_POST['name'];
	$uid = $_POST['uid'];
	$fid = $_POST['fid'];
	$color = $_POST['color'];
	$branch = $_POST['branch'];
	$cost = $_POST['cost'];
	$type = $_POST['type'];
	
	$check_query = $wpdb->query("SELECT * FROM `wp_user_flowers` WHERE flower = '$name' && user_id = $uid", ARRAY_A);
	if($check_query>=1){
		$checkRow = $wpdb->get_row("SELECT * FROM `wp_user_flowers` WHERE fid = $fid", ARRAY_A);
		if($checkRow['color']!=$color || $checkRow['stems']!=$branch || $checkRow['type']!=$type || $checkRow['cost']!=$cost){
			$res = $wpdb->update("wp_user_flowers", array("color"=>$color, "stems"=>$branch, "cost"=>$cost, "type"=>$type), array("fid"=>$fid));
			echo "Record Updated sucessfully.";
		} else {
			echo "Flower ".$name." Already created";	
		}
	} else {
		$res = $wpdb->update("wp_user_flowers", array("flower"=>$name, "color"=>$color, "stems"=>$branch, "cost"=>$cost, "type"=>$type), array("fid"=>$fid));
		echo "Record Updated sucessfully."; 
	}
	
	die;
}
add_action('wp_ajax_edit_flower', 'edit_flower');
add_action('wp_ajax_nopriv_edit_flower', 'edit_flower');

function delete_flower(){
	global $wpdb;
	$fid = $_POST['fid'];
	$wpdb->delete("wp_user_flowers", array("fid"=>$fid));
	echo "Record Deleted Sucessfully..";
	die;
}
add_action('wp_ajax_delete_flower', 'delete_flower');
add_action('wp_ajax_nopriv_delete_flower', 'delete_flower');
?>