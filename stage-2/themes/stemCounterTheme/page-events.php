<?php 
session_start();
//pr($_SESSION); die;
	if( isset($_SESSION['user_session']) )
	{
			$creds = array();
			$creds['user_login'] = $_SESSION['user_session']['user_login'];
			$creds['user_password'] = $_SESSION['user_session']['user_password'];
			$creds['remember'] = $_SESSION['user_session']['remember'];
			
			session_unset('user_session');
			$user = wp_signon( $creds, true );
			$current_user = $user->data->ID;
			if($current_user)
			{
				wp_set_auth_cookie($current_user);
			}	
			if (is_wp_error($user))
			{
				//echo $user->get_error_message();
			}
	
		
	}

get_header();
?>
<?php get_sidebar(); ?>
<!--main content start-->
      <section id="main-content">
          <section class="wrapper">
            <div class="col-lg-2">
           	 <!--<h3><i class="fa fa-angle-right"></i> Events</h3>-->
             </div>
        <div class="col-lg-10">
		<button class="btn btn-primary btn-lg pull-right" data-toggle="modal" data-target="#addNewEvent" style="margin-top: 10px; margin-bottom:10px;">
		New Event
		</button>
        </div>
            <div class="row mt">
                  <div class="col-md-12">
                      <div class="content-panel">
                          <table class="table table-striped table-advance table-hover user_ev_meta">
	                  	  	  <!--<h4><i class="fa fa-angle-right"></i> Current Events</h4>
	                  	  	  <hr>-->
                              <thead>
                              <tr>
                                  <th><i class="fa fa-bullhorn"></i> Event</th>
                                  <th><i class="fa fa-question-circle"></i> Date</th>
                                  <th><i class="fa fa-bullhorn"></i> Contact</th>
                                  <th><i class="fa fa-bullhorn"></i> Venue</th>
                                  <th><i class=" fa fa-edit"></i> Status</th>
                                  <th><i class="fa fa-bullhorn"></i> Action</th>
                                  <th></th>
                              </tr>
                              </thead>
                              <tbody>
	  <?php 
	  $user_ID = get_current_user_id();
      /* Call WP's "get_results" on your query and create the array */
     global $wpdb;
     $check_row = $wpdb->get_row("SELECT * FROM `wp_user_events` WHERE user =".$user_ID."&& archived = 0");
	 if($check_row){
	 	$current_events = $wpdb->get_results("SELECT * FROM `wp_user_events` WHERE user =".$user_ID."&& archived = 0") or die(mysql_error());
            foreach($current_events as $current_event){
            	$current_event = stripslashes_full($current_event);
				$event_name = $current_event->event_name;
				$date = $current_event->event_date;
				$json_info = json_decode($current_event->details);
				$venue = $json_info->Venue;
				$contact = $json_info->firstName . " " . $json_info->lastName;
				$status = $event_info_json->status;
				
				echo "<tr class='user_event_".$current_event->event_id."'>
                   	<td><a href='".get_bloginfo('url')."/stem_counter/?eid=$current_event->event_id'>" . $event_name . "</a></td>
					<td>".	$date	."</td>
                 	<td>".	$contact	."</td>
                 	<td>".	$venue	."</td>
					<td>".	$status	."</td>
					<td>" ?>
					<div class="btn-group">
					<button type="button" class="btn btn-theme03">Action</button>
					<button type="button" class="btn btn-theme03 dropdown-toggle" data-toggle="dropdown">
					<span class="caret"></span>
					<span class="sr-only">Toggle Dropdown</span>
					</button>
					<ul class="dropdown-menu" role="menu">
					<li><a href="<?php echo get_bloginfo('url')."/stem_counter/?eid=".$current_event->event_id; ?>">Go</a></li>
					<li><a href="#" onclick="return event_archive('<?php echo $current_event->event_id; ?>')">Archive</a></li>
					<li>
					<a href="<?php echo "#" ?>" 
					rel="<?php echo $current_event->event_id; ?>" class="editEventdetail" 
					data-target="#editEventFrom" onclick="return edit_event_id('<?php echo $current_event->event_id; ?>');" data-toggle="modal" title="<?php echo $current_event->event_name; ?>">Edit</a></li>
					<li class="divider"></li>
					<li><a href="<?php echo "#" ?>" rel="<?php echo $current_event->event_id; ?>" 
					title="<?php echo $current_event->event_name; ?>" class="delEvent">Delete</a></li>
					</ul>
            		</div>      		
                	</td>
           			</tr>
			<?php } }else{
				echo "<h2>No Event</h2>";
			} ?>
                     </tbody>
                     </table>
                     </div><!-- /content-panel -->
                  </div><!-- /col-md-12 -->
              
              
              <!--ARCHIVED EVENTS-->
              </div><!-- /row -->
                  <div class="col-md-12" style="margin-top: 10px; margin-right: 20px;">
                         <a class="pull-right" href="Events/stem_counter.html">Archived Events</a>
                  </div><!-- /col-md-12 -->
                  <div id="feedback"></div>

		</section><!--/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
		<!-- Modal -->
	<div class="modal fade" id="addNewEvent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id="myModalLabel">Add New Event</h4>
	      </div>
	      <div class="modal-body">
         <!-- <div class="form-panel">-->
		<form class="form-horizontal style-form" id="newCustomerForm" method="post" onsubmit="return add_event();">
		<div class="form-top">
		<h4> Contact </h4>
		</div>
		
		<div class="form-group">
		<label class="col-sm-2 col-sm-2 control-label" required>First Name*</label>
		<div class="col-sm-3">
		<input type="text"  class="form-control" name="first_name" placeholder="e.g. Sarah" id="firstName">
		</div>
		<label class="col-sm-2 col-sm-2 control-label align-right">Last Name*</label>
		<div class="col-sm-3">
		<input type="text"  class="form-control" name="last_name" placeholder="e.g. Johnson" id="lastName">
		</div>
		</div>
		
		<div class="form-group">
		<label class="col-sm-2 col-sm-2 control-label">Event Name*</label>
		<div class="col-sm-8">
		<input type="text" name="event_name" class="form-control" id="eventName">
		</div>
		</div>
		
		<div class="form-group">
		<label class="col-sm-2 col-sm-2 control-label">Venue</label>
		<div class="col-sm-8">
		<input type="text" name="venue" class="form-control" id="eventVanue">
		</div>
		</div>
		
		<div class="form-group">
		<label class="col-sm-2 col-sm-2 control-label">Date*</label>
		<div class="col-sm-3">
		<input type="text"  class="form-control" name="date" id="datepicker" placeholder="MM/DD/YYYY">
		</div>
		</div>
		
		<!--</div>-->
		<div class="modal-footer">
		<div class="pull-left">
		*required
		</div>
		<button type="button" class="btn btn-default" data-dismiss="modal">
		Cancel
		</button>
		<input type="hidden" name="action" value="addCustomer"/>
		<button type="submit" name="action" value="addCustomer" class="btn btn-primary" >
		Let's Go!
		</button>
		</div>
		</form>
	      
	    </div>
	  </div>
	</div>     
    </div>
<?php include('inc-forms/edit-event-form.php'); ?>    
<?php get_footer(); ?>
