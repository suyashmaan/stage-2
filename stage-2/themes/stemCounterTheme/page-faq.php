<?php get_header(); ?>
<?php get_sidebar(); ?>

<!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      
      
      
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
          	<div class="row mt">
          		<div class="content-panel">
                <div class="col-lg-12">
                
             
		   		<p><a name="top"></a></p>
<h3 style="text-align: center;"><strong>FAQ’s of StemCounter.com</span></strong></h3>
<h4><strong><a href="#FAQ1">What is StemCounter.com?</a></span></strong></h4>
<h4><strong><a href="#FAQ2">How do you add new hardgoods?</a></span></strong></h4>
<h3><strong><a name="FAQ1"></a>What is StemCounter.com?</span></strong></h3>
<p>StemCounter.com saves you time and money. No longer do you have to mess with long quote times and complex spreadsheets. This software does all the stem counting for you. Before you get up from consulting with a bride, you can have a completely finished and accurate quote that she can sign right there on the spot.<a href="#top">top</a></p>

<p><iframe width="560" height="315" src="//player.vimeo.com/video/117995239" frameborder="0" allowfullscreen></iframe></p>

<h3><strong><a name="FAQ2"></a>How do you add new hardgoods?</span></strong></h3>
<p>You'll want to go to Data->My Items->Add button <a href="#top">top</a></p>
			</div>
           </div>
          	</div>
			
		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->

<?php get_footer(); ?>