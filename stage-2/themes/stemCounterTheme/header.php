<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="StemCounter.com">
    <meta name="keyword" content="StemCounter, Stem Count, Flowers, Wedding Flowers">

    <title><?php wp_title(' ', true, 'right'); ?></title>
    
	<link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet">
    <?php wp_head(); ?>
    
    <!-- Bootstrap core CSS -->
    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="<?php echo get_template_directory_uri(); ?>/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>   

    <!-- Custom styles for this template -->
    <link href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/style-responsive.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/select2.css" rel="stylesheet">

    <script src="http://malsup.github.com/jquery.form.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/select2.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/masonry.js"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <?php
  
  if(is_user_logged_in()){
    $location = get_bloginfo('url');
    //echo '<script> window.location.href = "'.$location.'/profile/; </script>';
  }
  else{
    $location = get_bloginfo('url')."/logon/";
    echo '<script> window.location.href = "'.$location.'"; </script>';
  }
  ?>
<section id="container" >
	<!--
	*******************************
	TOP BAR CONTENT & NOTIFICATIONS
	*******************************
	-->
	<!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="<?php bloginfo('url'); ?>" class="logo"><b>StemCounter.com</b></a>
            <!--logo end-->
            <div class="nav notify-row" id="top_menu">
                <!--  notification start -->
                
                <!--  notification end -->
            </div>
            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
            	<?php
            	$log_out_redirect = get_bloginfo('url')."/logon/";
            	if(is_user_logged_in()){
            		echo '<li><a class="logout" href="'.wp_logout_url($log_out_redirect).'">Logout</a></li>';
            	}else{
                echo '<li><a class="logout" href="'.get_bloginfo('url').'/logon/">Login</a></li>'; ?>
            		<li><a class="logout" href="<?php bloginfo('url'); ?>/register">Register</a></li>
           		<?php } ?>	
            	</ul>
            </div>
</header>
      <!--header end-->     