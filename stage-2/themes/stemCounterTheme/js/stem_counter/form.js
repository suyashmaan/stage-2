// JavaScript Document


function ajaxSubmit(){
	   
		var newCustomerForm = jQuery(this).serialize();
	   
		jQuery.ajax({
				type:"POST",
				url: "/wp-admin/admin-ajax.php",
				data: newCustomerForm,
				success:function(data){
		jQuery("#feedback").html(data);
				},
	error: function(errorThrown){
		alert(errorThrown);
	}  
		});
	   
		return false;
}




$("#sub").click( function() {
 $.post( $("#newEvent").attr("action"),
         $("#newEvent :input").serializeArray(),
         function(info){ $("#result").html(info);
  });
});


$("#newEvent").submit( function() {
  return false;
});
 
function clearInput() {
    $("#newEvent :input").each( function() {
       $(this).val('');
    });
}

$(function() {
	//Get the form.
	var form = $('#newevent');
	
	//Get the messages div.
	var formMessages = $('#form-messages');
	
	//The rest of the code will go here
	
		//Event listener
	$(form).submit(function(event) {
		//stop the broswer from submitting the form.
		even.preventDefault();
		
	});

	//Serialize the form data
	var formData = $(form).serialize();
	
	$.ajax({
		type:	'POST',
		url:	$(form).attr('action'),
		data:	formData
	})
	
	.done(function(response) {
    // Make sure that the formMessages div has the 'success' class.
    $(formMessages).removeClass('error');
    $(formMessages).addClass('success');

    // Set the message text.
    $(formMessages).text(response);

    // Clear the form.
    $('#sub').val('');
	})

.fail(function(data) {
    // Make sure that the formMessages div has the 'error' class.
    $(formMessages).removeClass('success');
    $(formMessages).addClass('error');

    // Set the message text.
    if (data.responseText !== '') {
        $(formMessages).text(data.responseText);
    } else {
        $(formMessages).text('Oops! An error occured and your message could not be sent.');
    }
});
	
});


