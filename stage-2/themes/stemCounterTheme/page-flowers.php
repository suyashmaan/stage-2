<?php get_header(); ?>
<?php get_sidebar(); ?>
<link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css" rel="stylesheet">
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/dataTables.bootstrap.js"></script>
<!--**********************************************************************************************************************************************************
MAIN CONTENT
*********************************************************************************************************************************************************** -->
<!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<div class="col-lg-2">
             </div>
			<div class="col-lg-10">
			<button class="btn btn-primary btn-lg pull-right" data-toggle="modal" data-target="#addNewFlower" style="margin-top: 10px; margin-bottom:10px;">Add New Flower</button>
			</div>

              <div class="row mt">
                  <div class="col-md-12">
                      <div class="content-panel">
                          <table class="table table-striped table-advance table-hover" id="listFlowers">
                              <thead>
                              <tr>
                                  <th><i class="fa fa-bullhorn"></i> Flowers</th>
                                  <th><i class="fa fa-question-circle"></i>Colors</th>
                                  <th><i class="fa fa-bookmark"></i> Stems Per Bunch</th>
                                  <th>Cost/Bunch</th>
                                  <th><i class=" fa fa-edit"></i> Type</th>
                                  <th></th>
                              </tr>
                              </thead>
              	<?php
			   	$current_user = wp_get_current_user();
              	global $wpdb;
				$check = $wpdb->get_row("SELECT * FROM `wp_user_flowers` WHERE user_id =".$current_user->ID);
				if($check){
					echo '<tbody>';
					$rows = $wpdb->get_results("SELECT * FROM `wp_user_flowers` WHERE user_id =".$current_user->ID) or die(mysql_error());
					foreach($rows as $row){
					echo '<tr>
					  <td><a href="basic_table.html#" class="flower_title">'.$row->flower.'</a></td>
					  <td class="hidden-phone">'.$row->color.'</td>
					  <td>'.$row->stems.'</td>
					  <td>'.$row->cost.'</td>
					  <td>'.$row->type.'</td>
					  <td>';?>
					  <button data-target="#editFlower" data-toggle="modal" class="btn btn-primary btn-xs" 
					  onclick="return ex_value(<?php echo "'".$row->fid."', '".$row->flower."', '".$row->color."', '". $row->stems."', '".$row->cost."', '".$row->type."'"; ?>)">
					  <i class="fa fa-pencil"></i></button>
					  <?php echo '</td>
					  </tr>';
					}
					echo '</tbody>';
				}	
              	?>
                          </table>
                      </div><!-- /content-panel -->
                  </div><!-- /col-md-12 -->
              </div><!-- /row -->

    		</section><!--/wrapper -->
      </section><!-- /MAIN CONTENT -->
      
      <!--#addNewFlower-->
		<?php include('inc-forms/add_flower.php'); ?>
   <!--#addNewFlower-->
   
   <!--edit FLower-->
	<div class="modal fade" id="editFlower" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Edit <span class="eflower_name"></span> Flower</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal style-form" method="post" onsubmit="return edit_flower();">
					<input type="hidden" id="editFlowerID" />
					<div class="form-group">
						<label class="col-sm-2 col-sm-2 control-label">Flower</label>
						<div class="col-sm-8">
							<input type="text" name="venue" class="form-control" id="editName">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 col-sm-2 control-label">Colors</label>
						<div class="col-sm-8">
							<input type="text" name="venue" class="form-control" id="editColor">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 col-sm-2 control-label">Stems Per Bunch</label>
						<div class="col-sm-8">
							<input type="text" name="venue" class="form-control" id="editBranch">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 col-sm-2 control-label">Cost</label>
						<div class="col-sm-8">
							<input type="text" name="venue" class="form-control" id="editCost">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 col-sm-2 control-label">Type</label>
						<div class="col-sm-8">
							<input type="text" name="venue" class="form-control" id="editType">
						</div>
					</div>
					<div class="modal-footer">
						<div class="pull-left">
						*required
						</div>
						<button type="button" class="btn btn-default" onclick="return delete_flower()">Delete</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						<button type="submit" name="action" value="addCustomer" class="btn btn-primary">Let's Go!</button>
					</div>
				</form>
			</div>
			</div>
		</div>     
	</div><!--//#editFlower-->
   
   	<script>
   	
   	
   	function ex_value(fid, flower, color, stem, cost, type){
		$("#editName").val("");
		$("#editColor").val("");
		$("#editBranch").val("");
		$("#editType").val("");
		$("#editCost").val("");
		$("#editFlowerID").val("");
		
		$("#editName").val(flower);
		$("#editColor").val(color);
		$("#editBranch").val(stem);
		$("#editType").val(type);
		$("#editCost").val(cost);
		$("#editFlowerID").val(fid);
	}//ex_value()
   	
   	function edit_flower(){
   		var uid = "<?php echo $current_user->ID; ?>";
   		var fid = $("#editFlowerID").val();
   		var name = $("#editName").val();
   		var color = $("#editColor").val();
   		var branch = $("#editBranch").val();
   		var cost = $("#editCost").val();
   		var type = $("#editType").val();
   		
   		if(name != ""){
   		jQuery.ajax({
					type:"post",
					url: "<?php bloginfo('url'); ?>/wp-admin/admin-ajax.php",
					data: {action: 'edit_flower', fid:fid, uid:uid, name: name, color: color, branch:branch, cost:cost, type:type},
					success:function(edata){
						//alert(edata);
						location.reload();
					}
		});
		}
		
   		return false;
   	}//edit_flower()
   	
   	function delete_flower(){
   		var fid = $("#editFlowerID").val();
   		if(confirm("Are you sure you want to delete Flower")){
   		jQuery.ajax({
					type:"post",
					url: "<?php bloginfo('url'); ?>/wp-admin/admin-ajax.php",
					data: {action: 'delete_flower', fid:fid},
					success:function(edata){
						//alert(edata);
						location.reload();
					}
		});
   		}
   	}//delete_flower();
   	
	$(function(){
		$("#listFlowers").dataTable();
	});
	</script>
      <!--main content end-->

<?php get_footer(); ?>
