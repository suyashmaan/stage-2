<script type="text/javascript">
	function edit_event_id(event_id){
		if(event_id==""){
			return false;
		}else{
			jQuery.ajax({
				type:"post",
				url: "<?php bloginfo('url'); ?>/wp-admin/admin-ajax.php",
				data: {action: 'edit_user_event_id', evID:event_id},
				success:function(edata){
					//alert(edata);
					var jsonpars = JSON.parse(edata); 
					$("#editEveID").val(jsonpars.event_id);
					$("#e_eventName").val(jsonpars.event_name);
					$("#e_firstName").val(jsonpars.details.firstName);
					$("#e_lastName").val(jsonpars.details.lastName);
					$("#e_eventVanue").val(jsonpars.details.Venue);
					//alert(jsonpars.event_date);
					//alert($.datepicker.formatDate('mm/dd/yy', new Date(jsonpars.event_date)));
					$("#e_datepicker").val($.datepicker.formatDate('mm/dd/yy', new Date(jsonpars.event_date)));
					//location.reload();
				}
			});
		}

	}
	function event_archive(event_id){
		var r = confirm("Press a button!");
		if (r == true) {
		    if(event_id==""){
				alert("Please Select One Event.");
			}else{
				jQuery.ajax({
					type:"post",
					url: "<?php bloginfo('url'); ?>/wp-admin/admin-ajax.php",
					data: {action: 'archive_user_event_id', evID:event_id},
					success:function(edata){
						alert(edata);
						location.reload();
					}
				});
			}
		} else {
		    return false;
		}
		
	}
</script>
<div class="modal fade" id="editEventFrom" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">
        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Edit "<span class="editEtitle"></span>" Events</h4>
      </div>
      <div class="modal-body">
		<form class="form-horizontal style-form" method="post" onsubmit="return edit_event_info();">
			<input type="hidden" id="editEveID" value="" />
			<div class="form-top"><h4>Edit Event</h4></div>
	<div class="form-group">
		<label class="col-sm-2 col-sm-2 control-label" required>First Name*</label>
		<div class="col-sm-3">
			<input type="text"  class="form-control" name="e_first_name" placeholder="e.g. Sarah" id="e_firstName">
		</div>
		<label class="col-sm-2 col-sm-2 control-label align-right">Last Name*</label>
		<div class="col-sm-3">
			<input type="text"  class="form-control" name="e_last_name" placeholder="e.g. Johnson" id="e_lastName">
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 col-sm-2 control-label">Event Name*</label>
		<div class="col-sm-8">
			<input type="text" name="e_event_name" class="form-control" id="e_eventName">
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 col-sm-2 control-label">Venue</label>
		<div class="col-sm-8">
			<input type="text" name="e_venue" class="form-control" id="e_eventVanue">
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 col-sm-2 control-label">Date*</label>
		<div class="col-sm-3">
			<input type="text"  class="form-control" name="e_date" id="e_datepicker" placeholder="MM/DD/YYYY">
		</div>
	</div>
<!--</div>-->
	<div class="modal-footer">
			<div class="pull-left">*required</div>
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			<button type="submit" value="addCustomer" class="btn btn-primary" >Let's Go!</button>
		</div>
		</form>
      </div>
    </div>
  </div>
</div>
<?php $current_user = wp_get_current_user(); ?>
<script type="text/javascript">
$(document).ready(function(){
$("ul.dropdown-menu li").delegate(".editEventdetail", "click", function(){
	var eveID = $(this).attr("rel");
	var e_edt_eve_name = $(this).attr("title");
	$("span.editEtitle").html(e_edt_eve_name);
	$("#editEveID").val(eveID);
});

$("ul.dropdown-menu li").delegate(".delEvent", "click", function(){
	var del_eid = $(this).attr("rel");
	var del_etitle = $(this).attr("title");
	var del_erow = $(this).parents("tr");
	var del_row_class = "user_event_"+del_eid;
	if(confirm("Are you sure you want to delete "+del_etitle+" Event")){
		jQuery.ajax({
		type:"post",
		url: "<?php bloginfo('url'); ?>/wp-admin/admin-ajax.php",
		data: {action: 'delete_user_event', event_id:del_eid},
			success:function(respo){
				if(respo == del_row_class){
					del_erow.fadeOut(500).remove();
				}
			}
		});	
	}
});

});

function edit_event_info(){
	var ename = $('#e_eventName').val();
	var edate = $('#e_datepicker').val();
	var efname = $('#e_firstName').val();
	var elname = $('#e_lastName').val();
	var evanue = $('#e_eventVanue').val();
	var evID = $("#editEveID").val();
	var cuser = "<?php echo $current_user->ID; ?>";
	if( ename == "" || edate == "" || efname == "null" || elname == "" || evanue == ""){
		alert("Please fill the text field..");
		return false;
	}else{
	jQuery.ajax({
	type:"post",
	url: "<?php bloginfo('url'); ?>/wp-admin/admin-ajax.php",
	data: {action: 'edit_user_event', evID:evID,  ename:ename, edate:edate, efname:efname,elname:elname,evanue:evanue, cuser:cuser},
	success:function(edata){
		alert(edata);
		location.reload();
	}
	});
	return false;
	}
}

function add_event(){
	var ename = $('#eventName').val();
	var edate = $('#datepicker').val();
	var efname = $('#firstName').val();
	var elname = $('#lastName').val();
	var evanue = $('#eventVanue').val();
	var cuser = "<?php echo $current_user->ID; ?>";
	jQuery.ajax({
	type:"post",
	url: "<?php bloginfo('url'); ?>/wp-admin/admin-ajax.php",
	data: {action: 'add_user_event', ename:ename, edate:edate, efname:efname,elname:elname,evanue:evanue, cuser:cuser},
	success:function(resp){
		resp=jQuery.trim(resp);
		if(resp=="Event already created.."){
			alert(resp);
			return false;
		}else if(resp=="No Event Added"){
			alert(resp)
			return false;
		}else if(resp=="No Record Added"){
			alert(resp);
			return false;
		}else{
			/*alert('asdfa');
			alert("<?php echo get_bloginfo('url')?>/stem_counter/?eid="+resp);
			return false;*/
			window.location.href ="<?php echo get_bloginfo('url')?>/stem_counter/?eid="+resp;
		}
		return false;
		/*alert(resp);
		location.reload();*/
	}
	});//Ajax
	return false;
}
</script>