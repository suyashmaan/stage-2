<!--**********************************************************************************************************************************************************
MAIN SIDEBAR MENU
*********************************************************************************************************************************************************** -->
       <!--sidebar start-->
<?php
?>
       <aside class="___Sidebar">
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              <p class="centered">
                <?php
                    // Retrieve The Post's Author ID
                    $user_id = get_current_user_id();
                  // Set the image size. Accepts all registered images sizes and array(int, int)
                  
                 // $size = 'thumbnail';

                  // Get the image URL using the author ID and image size params
                  //$imgURL = get_cupp_meta($user_id, $size);
                    
                ?>

              <?php echo user_avatar_get_avatar( $user_id, 60);?>
              <?php global $current_user;
                    get_currentuserinfo();
              ?>
              </p>
              <h5 class="centered"><?php echo $current_user->display_name; ?></h5>
                  <li class="mt">
                      <?php if ( is_page( 'Events' ) ){
						  	echo "<a class=\"active\" href=\"../events\">";
							} else {
							echo "<a href=\"../events\">";
							}
							?>
                          <i class="fa fa-book"></i>
                          <span>Events</span>
                      </a>
                  </li>
                  
                  
                  


       <li class="sub-menu">
                    <?php  if ( is_page( 'Flowers' ) ){
						  	echo "<a class=\"active\" href=\"../flowers\">";
							} else {
							echo "<a href=\"../flowers\">";
							}
							?>
                          <i class="fa fa-leaf"></i>
                          <span>Flowers</span>
                      </a>
                  </li>
                  
                  <li class="sub-menu">
    				<?php  if (is_page( 'hardgoods')){
						  	echo "<a class=\"active\" href=\"../hardgoods\">";
							} else {
							echo "<a href=\"../hardgoods\">";
							}
							?>
                          <i class="fa fa-tasks"></i>
                          <span>Hardgoods</span>
                      </a>
                  </li>
                  
                  <li class="sub-menu">
    				<?php  if (is_page( 'my_items')){
						  	echo "<a class=\"active\" href=\"../my_items\">";
							} else {
							echo "<a href=\"../my_items\">";
							}
							?>
                          <i class="fa fa-tasks"></i>
                          <span>My Items</span>
                      </a>
                  </li>


                  <li class="sub-menu">
                     <?php if ( is_page( 'profile' ) ){
						  	echo "<a class=\"active\" href=\"../profile\">";
							} else {
							echo "<a href=\"../profile\">";
							}
							?>
                          <i class="fa fa-cogs"></i>
                          <span>Profile</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <?php if ( is_page( 'FAQ' ) ){
						  	echo "<a class=\"active\" href=\"../FAQ\">";
							} else {
							echo "<a href=\"../FAQ\">";
							}
							?>
                          <i class="fa fa-question"></i>
                          <span>FAQ/Help</span>
                      </a>
                  </li>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->