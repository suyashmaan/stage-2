<?php get_header(); ?>
<?php get_sidebar('event'); ?>



<!-- On Page PHP -->
<!--Fix!!!!-->
<!--< ? php 
	/* Call WP's "get_results" on your query and create the array */
	global $wpdb;
	
	//USER SECURITY
   $user_ID = get_current_user_id(); 
	
	//LOAD EVENT DETAILS
	//FIX!! WE WANT THIS EVENT ID TO PULL FROM THE BROWSER
	$event_ID = "1001";
	$event_details = $wpdb->get_results("SELECT * FROM sc_events WHERE user = ".$user_ID." AND event = ".$event_ID) or die(mysql_error()); 
	foreach($event_details as $event_detail){
		$event_info = $event_detail->details;
		$event_info_json = json_decode($event_info);
		$event_name = $event_info_json->event_name;
		$date = $event_detail->event_date;
		$contact = $event_info_json->contact->first_name . " " . $event_info_json->contact->last_name;
		$venue = $event_info_json->venue;
		$status = $event_info_json->status; ?>-->
     <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3><i class="fa fa-angle-right"></i> Event Details</h3>
          	
          	<!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt">
                <div class="col-lg-12">
                  <div class="form-panel">
                  	  <h4 class="mb"><i class="fa fa-angle-right"></i> General Information</h4>
                      
                      
                      <form class="form-horizontal style-form" method="get">
                      <fieldset>
                      <div class="form-group">
                             
                              <label class="col-sm-2 col-sm-2 control-label">Event Name</label>
                              <div class="col-sm-10">
                                  <label type="text"  class="control-label" placeholder="e.g. Johnson Wedding">Johnson Wedding </label>
                              </div>
                             
                          </div>
                          
                          <div class="form-group">
                             
                              <label class="col-sm-2 col-sm-2 control-label">Date</label>
                              <div class="col-sm-2">
                                  <label type="text"  class="control-label" placeholder="MM/DD/YYYY">04/10/2014</label>
                              </div>
                          </div>
                          
                          <!--Bride's Info-->
                         <form class="form-inline" role="form">
                          <div class="form-group col-sm-2">
                              <label class="control-label">Bride's First Name</label><br>
                              <label type="text" class="control-label">Stacey</label>
                              
                          </div>
                          <div class="form-group col-sm-2" style="margin-left:2%">
                              <label class="control-label">Bride's Last Name</label><br>
                              <label type="text" class="control-label">Washington</label>
                              
                              </div>
                              
                          <div class="form-group col-sm-2" style="margin-left:2%">
                              <label class="control-label">Phone Number</label><br>
                              <label type="text" class="control-label">888-888-8888</label>
                              
                          </div>
                          <div class="form-group col-sm-2" style="margin-left:2%">
                              <label class="control-label">Email Address</label><br>
                              <label type="text" class="control-label">stacy@gmail.com</label>
                              
                          </div>
                        </form>  <!-- End Bride's Info-->
                        </fieldset>
                      </form>
              	</div>
          		</div><!--  col-lg-6-->      	
          	</div><!-- /row -->
          	
         <!-- VENUE -->
          	<div class="row mt">
                <div class="col-lg-12">
                  <div class="form-panel">
                  	  <h4 class="mb"><i class="fa fa-angle-right"></i> Venue</h4>
                      
                      
                      <form class="form-horizontal style-form" method="get">
                      <fieldset>
                      <div class="form-group">
                             
                              <label class="col-sm-2 col-sm-2 control-label">Venue</label>
                              <div class="col-sm-10">
                                  <label type="text"  class="control-label" placeholder="e.g. The Coronado">Coronado </label>
                              </div>
                             
                          </div>
                          
                          <div class="form-group">
                             
                              <label class="col-sm-2 col-sm-2 control-label">Address</label>
                              <div class="col-sm-10">
                                  <label type="text"  class="control-label" placeholder="Address">8855 Dunn Road</label>
                              </div>
                          </div>
                         
                      </form>
              	</div>
          		</div><!--  col-lg-6-->      	
          	</div><!-- /row -->
          	
          	
          	
          	
		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->

<?php get_footer(); ?>