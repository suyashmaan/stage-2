<?php 
if(is_user_logged_in())
{
	wp_redirect(get_bloginfo('url')."/events"); exit;
} 
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="StemCounter.com">
    <meta name="keyword" content="StemCounter, Stem Count, Flowers, Wedding Flowers">

    <title><?php wp_title(' ', true, 'right'); ?></title>
    
	<link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet">
    <?php wp_head(); ?>
    
    <!-- Bootstrap core CSS -->
    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="<?php echo get_template_directory_uri(); ?>/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->

	  <div id="login-page">
	  	<div class="container">
	  	
		      <form class="form-login" onsubmit="return custom_login_function();">
		        <h2 class="form-login-heading">sign in now</h2>
		        <div class="login-wrap">
		            <input type="text" id="wpUser" class="form-control" placeholder="User ID" autofocus>
		            <br>
		            <input type="password" id="wpPass" class="form-control" placeholder="Password">
		            <label class="checkbox">
		                <span class="pull-right">
		                <a data-toggle="modal" href="login.html#myModal"> Forgot Password?</a>
		                </span>
		            </label>
		            <?php wp_nonce_field('ajax-login-nonce', 'security' ); ?>
		            <button class="btn btn-theme btn-block" href="index.html" type="submit"><i class="fa fa-lock"></i> SIGN IN</button>
		            <div class="log_ajax_resp"></div>
		            <hr>
		            <!-- <div class="login-social-link centered">
		            <p>or you can sign in via your social network</p>
		                <button class="btn btn-facebook" type="submit"><i class="fa fa-facebook"></i> Facebook</button>
		                <button class="btn btn-twitter" type="submit"><i class="fa fa-twitter"></i> Twitter</button>
		            </div> -->
		            <div class="registration">Don't have an account yet?<br/>
		            <a class="" href="<?php bloginfo('url'); ?>/register/">Create an account</a>
		            </div>
		
		        </div>
		      </form>	  	
	  	</div>
	  </div>
	  
	  <!-- Modal -->
		       <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
		              <div class="modal-dialog">
		                  <div class="modal-content">
		                      <div class="modal-header">
		                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                          <h4 class="modal-title">Forgot Password ?</h4>
		                      </div>
		                      <div class="modal-body">
		                          <p>Enter your e-mail address below to reset your password.</p>
		                          <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">
		                      </div>
		                      <div class="modal-footer">
		                          <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
		                          <button class="btn btn-theme" type="button">Submit</button>
		                      </div>
		                  </div>
		              </div>
		       </div>
	<!-- modal -->
		          
    <!-- js placed at the end of the document so the pages load faster -->
    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.backstretch.min.js"></script>
    <script>
        jQuery.backstretch("<?php bloginfo('template_url'); ?>/img/login-bg.jpg", {speed: 500});
    
    function custom_login_function(){
    	var user = $("#wpUser").val();
    	var pass = $("#wpPass").val();
    	var security = $("#security").val();
    	if(user == "" || pass == ""){
    		alert("Please Enter your user name and password");
    	}else{
			jQuery.ajax({
			type:"post",
			url: "<?php bloginfo('url'); ?>/wp-admin/admin-ajax.php",
			data: {action: 'check_wp_login', user: user, pass:pass, security:security},
			success:function(edata){
				if(edata == 0){
					jQuery(".log_ajax_resp").html("<span class='red'>Please enter correct user name and password.</span>");
				}else{
					jQuery(".log_ajax_resp").html("<span class='green'>Login Sucessfully....</span>");
					window.setTimeout(function(){
					window.location.href = "<?php bloginfo('url'); ?>/events/";
					}, 1500);
				}
			}
			});
    	}
    	return false;
    }    
    </script>
    
  </body>
</html>
