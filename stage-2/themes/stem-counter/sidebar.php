<!--**********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
       <!--sidebar start-->
            <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
              	  <p class="centered"><img src=" <?php bloginfo('stylesheet_directory'); ?>/img/ui-tw.jpg" class="img-circle" width="60"></p>
              	  <h5 class="centered">Twisted Willow</h5>
                  
                  <li class="mt">
                      <?php if ( is_page( 'Events' ) ){
						  	echo "<a class=\"active\" href=\"../events\">";
							} else {
							echo "<a href=\"../events\">";
							}
							?>
                          <i class="fa fa-book"></i>
                          <span>Events</span>
                      </a>
                  </li>
                  
                  
                  
<!--           UPDATE WHEN THE REST IS DONE!!

       <li class="sub-menu">
                    <?php /*?> <?php if ( is_page( 'Flowers' ) ){
						  	echo "<a class=\"active\" href=\"../flowers\">";
							} else {
							echo "<a href=\"../flowers\">";
							}
							?><?php */?>
                          <i class="fa fa-leaf"></i>
                          <span>Flowers</span>
                      </a>
                  </li>
                  
                  <li class="sub-menu">
    <?php /*?>                  <?php if ( is_page( 'My Items' ) ){
						  	echo "<a class=\"active\" href=\"../my_items\">";
							} else {
							echo "<a href=\"../my_items\">";
							}
							?><?php */?>
                          <i class="fa fa-tasks"></i>
                          <span>My Items</span>
                      </a>
                  </li>-->


                  <li class="sub-menu">
                     <?php if ( is_page( 'Settings' ) ){
						  	echo "<a class=\"active\" href=\"../settings\">";
							} else {
							echo "<a href=\"../settings\">";
							}
							?>
                          <i class="fa fa-cogs"></i>
                          <span>My Settings</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <?php if ( is_page( 'FAQ' ) ){
						  	echo "<a class=\"active\" href=\"../FAQ\">";
							} else {
							echo "<a href=\"../FAQ\">";
							}
							?>
                          <i class="fa fa-question"></i>
                          <span>FAQ/Help</span>
                      </a>
                  </li>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->