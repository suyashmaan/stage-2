<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="StemCounter.com">
    <meta name="keyword" content="StemCounter, Stem Count, Flowers, Wedding Flowers">

    <title><?php wp_title(' ', true, 'right'); ?></title>
    
	<link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet">
    <?php wp_head(); ?>
    
    <!-- Bootstrap core CSS -->
    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="<?php echo get_template_directory_uri(); ?>/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
	<!--
	*******************************
	TOP BAR CONTENT & NOTIFICATIONS
	*******************************
	-->
	<!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="<?php bloginfo('url'); ?>" class="logo"><b>StemCounter.com</b></a>
            <!--logo end-->
            <div class="nav notify-row" id="top_menu">
                <!--  notification start -->
                
                <!--  notification end -->
            </div>
            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
            	<?php
            	if(is_user_logged_in()){
            		echo '<li><a class="logout" href="'.wp_logout_url().'">Logout</a></li>';
            	}else{
            		echo '<li><a class="logout" href="'.wp_login_url().'">Login</a></li>';
            	}
            	?>	
            	</ul>
            </div>
        </header>
      <!--header end-->     