<?php //Custom functions.php file for StemCounter.com

function theme_styles() {
    wp_register_style( 'fullpage-css', get_template_directory_uri() . '/css/jquery.fullPage.css' );   
    wp_enqueue_style( 'fullpage-css' ); 
}
add_action('wp_enqueue_scripts', 'theme_styles');

function theme_scripts() {
   	//jquery not included as wordpress has this built in.	
    wp_register_script( 'bootstrap.min', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'),null,true  ); 
    wp_enqueue_script( 'bootstrap.min' );
    wp_register_script( 'accordian', get_template_directory_uri() . '/js/jquery.dcjqaccordion.2.7.js', array('jquery'),null,true  ); 
    wp_enqueue_script( 'accordian' );
    wp_register_script( 'slimscroll', get_template_directory_uri() . '/js/vendors/jquery.slimscroll.min.js', array('jquery'), null, true  );
    wp_enqueue_script( 'slimscroll' );
	wp_register_script( 'nicescroll', get_template_directory_uri() . '/js/vendors/jquery.nicescroll.js', array('jquery'), null, true  );
    wp_enqueue_script( 'nicescroll' );
	wp_register_script( 'common-scripts', get_template_directory_uri() . '/js/common-scripts.js', array('jquery'), null, true  );
    wp_enqueue_script( 'common-scripts' );
}
add_action('wp_enqueue_scripts', 'theme_scripts');


   // <!-- js placed at the end of the document so the pages load faster -->
    

/*

    <!--script for this page-->
    <script src="<?php echo get_template_directory(); ?>/js/jquery-ui-1.9.2.custom.min.js"></script>

	<!--custom switch-->
	<script src="<?php echo get_template_directory(); ?>/js/bootstrap-switch.js"></script>
	
	<!--custom tagsinput-->
	<script src="<?php echo get_template_directory(); ?>/js/jquery.tagsinput.js"></script>
	
	<!--custom checkbox & radio-->
	
	<script type="text/javascript" src="<?php echo get_template_directory(); ?>/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory(); ?>/js/bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory(); ?>/js/bootstrap-daterangepicker/daterangepicker.js"></script>
	
	<script type="text/javascript" src="<?php echo get_template_directory(); ?>/js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
	
	
	<script src="<?php echo get_template_directory(); ?>/js/form-component.js"></script>    */
    
// remove junk from head
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);

function remove_admin_bar(){
	return false;
}
add_filter('show_admin_bar', 'remove_admin_bar');

?>