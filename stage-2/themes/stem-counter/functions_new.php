<?php //Custom functions.php file for StemCounter.com




function theme_styles() {
    wp_register_style( 'fullpage-css', "//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css", array('jquery') );   
    wp_enqueue_style( 'fullpage-css' ); 

	wp_register_style( 'datepicker-css', get_template_directory_uri() . '/css/jquery.fullPage.css', array('jquery') );   
    wp_enqueue_style( 'datepicker-css' ); 
	
	
}
add_action('wp_enqueue_scripts', 'theme_styles');

//ADD EVENT. MUCH CLEANUP NEEDED.
wp_enqueue_script('jquery');
 
function addCustomer(){
       
        global $wpdb;
       
        $first_name = $_POST['first_name'];
		 $last_name = $_POST['last_name'];
        $phone = $_POST['phone'];
        $email = $_POST['email'];
        $venue = $_POST['venue'];
		$event_name = $_POST['event_name'];
		$date = $_POST['date'];
       
	   			$details = '{"event_name":"' . $event_name . '", "contact": {"first_name":"' . $first_name . '", "last_name":"' . $last_name . '","phone_number":"' . $phone . '","email":"' . $email . '"}, "venue":"' . $venue . '", "status":"Quoted"}';
		
	   
        if($wpdb->insert('sc_events',array(
                'user'=> get_current_user_id(),
				'event_date' => $date,
                'details'=>$details))===FALSE){
       
                echo "Error";
               
        }
        else {
                        echo "Customer '".$name. "' successfully added, row ID is ".$wpdb->insert_id;
 
                }
        die(); 
}
add_action('wp_ajax_addCustomer', 'addCustomer');
add_action('wp_ajax_nopriv_addCustomer', 'addCustomer');

//Adding ajax

/*function new_event()
{

     wp_localize_script( 'function', 'insert_new_event', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
     wp_enqueue_script( 'function', get_template_directory_uri().'/js/stem_counter/form.js', 'jquery', true);

}
add_action('template_redirect', 'new_event');

$dirName = dirname(__FILE__);
$baseName = basename(realpath($dirName));
require_once ("$dirName/MyFunctions.php");

add_action("wp_ajax_get_my_option", "get_my_option");

*/


//THESE ARE THE THEME SCRIPTS. WE'RE GOING TO WANT TO ADJUST THESE TO ENQUEUE THE PIECES AS NEEDED PER PAGE.
add_action('wp_enqueue_scripts', 'theme_scripts');

function theme_scripts() {
	
	//TODO FIGURE OUT HOW TO GET THESE SET UP IN THE FOOTER
	
	
    wp_register_script( 'bootstrap.min', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery')); 
    wp_enqueue_script( 'bootstrap.min' );
    wp_register_script( 'accordian', get_template_directory_uri() . '/js/jquery.dcjqaccordion.2.7.js', array('jquery')); 
    wp_enqueue_script( 'accordian' );
    wp_register_script( 'slimscroll', get_template_directory_uri() . '/js/vendors/jquery.slimscroll.min.js', array('jquery'), null, true  );
    wp_enqueue_script( 'slimscroll' );
	wp_register_script( 'nicescroll', get_template_directory_uri() . '/js/vendors/jquery.nicescroll.js', array('jquery'), null, true  );
    wp_enqueue_script( 'nicescroll' );
	wp_register_script( 'common-scripts', get_template_directory_uri() . '/js/common-scripts.js', array('jquery'));
    wp_enqueue_script( 'common-scripts' );
	
	wp_register_script( 'scrolltomin', get_template_directory_uri() . '/js/jquery.scrollTo.min.js', array('jquery'));
    wp_enqueue_script( 'scrolltomin' );
	
	
	wp_register_script( 'customswitch', get_template_directory_uri() . '/js/jquery-ui-1.9.2.custom.min.js', array('jquery'));
    wp_enqueue_script( 'customswitch' );
	
	
	
	//Date pickers
	wp_register_script( 'datepicker', get_template_directory_uri() . '/js/bootstrap-datepicker.js', array('jquery'));
    wp_enqueue_script( 'datepicker' );
	
		
	wp_register_script( 'date', get_template_directory_uri() . '/js/bootstrap-daterangepicker/date.js', array('jquery'));
    wp_enqueue_script( 'date' );
	
		
	wp_register_script( 'daterangepicker', get_template_directory_uri() . '/js/bootstrap-daterangepicker/daterangepicker.js', array('jquery'));
    wp_enqueue_script( 'daterangepicker' );
	
	//Custom Tagsinput
	wp_register_script( 'tagsinput', get_template_directory_uri() . '/js/jquery.tagsinput.js', array('jquery'));
    wp_enqueue_script( 'tagsinput' );
	
	
//Custom Tagsinput
	wp_register_script( 'inputmask', get_template_directory_uri() . '/js/bootstrap-inputmask/bootstrap-inputmask.min.js', array('jquery'));
    wp_enqueue_script( 'inputmask' );
	
	//Custom Tagsinput
	wp_register_script( 'formcomponent', get_template_directory_uri() . '/js/form-component.js', array('jquery'));
    wp_enqueue_script( 'formcomponent' );
	
	
	
	//STEMCOUNTER FILES
	wp_register_script( 'form_edit', get_template_directory_uri() . '/js/stem_counter/form.js', array('jquery'));
	wp_enqueue_script( 'form_edit' );

	
}


add_action('wp_enqueue_script','register_my_scripts');

function register_my_scripts(){
wp_enqueue_script('script');
}


    
// remove junk from head
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);




?>