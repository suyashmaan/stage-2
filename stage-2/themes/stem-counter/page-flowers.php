<?php get_header(); ?>
<?php get_sidebar(); ?>

<!--   **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3><i class="fa fa-angle-right"></i> Venues</h3>

              <div class="row mt">
                  <div class="col-md-12">
                      <div class="content-panel">
                          <table class="table table-striped table-advance table-hover">
	                  	  	  <h4><i class="fa fa-angle-right"></i> Advanced Table</h4>
	                  	  	  <hr>
                              <thead>
                              <tr>
                                  <th><i class="fa fa-bullhorn"></i> Flowers</th>
                                  <th><i class="fa fa-question-circle"></i>Colors</th>
                                  <th><i class="fa fa-bookmark"></i> Stems Per Bunch</th>
                                  <th>Cost/Bunch</th>
                                  <th><i class=" fa fa-edit"></i> Type</th>
                                  <th></th>
                              </tr>
                              </thead>
                              <tbody>
                              <tr>
                                  <td><a href="basic_table.html#">Agapanthus</a></td>
                                  <td class="hidden-phone">Purple</td>
                                  <td>10</td>
                                  <td><input type="text" name="cost" placeholder="10.00"></td>
                                  <td>Focal/Accent</td>
                                  <td>
                                      <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      <a href="basic_table.html#">Sweat Peas</a>
                                  </td>
                                  <td>Light Pink</td>
                                  <td>10</td>
                                  <td><input type="text" name="cost" placeholder="15.00"></td>
                                  <td>Focal/Accent</td>
                                  <td>
                                      <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button>
                                  </td>
                              </tr>
                            
                              </tbody>
                          </table>
                      </div><!-- /content-panel -->
                  </div><!-- /col-md-12 -->
              </div><!-- /row -->

		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->

<?php get_footer(); ?>