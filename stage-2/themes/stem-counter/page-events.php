<?php get_header(); ?>
<?php get_sidebar(); ?>



      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
            <div class="col-lg-2">
           	 <h3><i class="fa fa-angle-right"></i> Events</h3>
             
             </div>
             <div class="col-lg-10">
             
             
             
               <! -- New Event MODALS -->
      			
						<!-- Button trigger modal -->
						<button class="btn btn-primary btn-lg pull-right" data-toggle="modal" data-target="#myModal" style="margin-top: 10px; margin-bottom:10px;">
						  New Event
						</button>
             
             
             </div>
             
            
              <div class="row mt">
                  <div class="col-md-12">
                      <div class="content-panel">
                          <table class="table table-striped table-advance table-hover">
	                  	  	  <h4><i class="fa fa-angle-right"></i> Current Events</h4>
	                  	  	  <hr>
                              <thead>
                              <tr>
                                  <th><i class="fa fa-bullhorn"></i> Event</th>
                                  <th><i class="fa fa-question-circle"></i> Date</th>
                                  <th><i class="fa fa-bullhorn"></i> Contact</th>
                                  <th><i class="fa fa-bullhorn"></i> Venue</th>
                                  <th><i class=" fa fa-edit"></i> Status</th>
                                  <th><i class="fa fa-bullhorn"></i> Action</th>
                                  <th></th>
                              </tr>
                              </thead>
                              <tbody>
	  <?php 
	  
	  $user_ID = get_current_user_id(); 

            /* Call WP's "get_results" on your query and create the array */
            global $wpdb;
            $current_events = $wpdb->get_results("SELECT * FROM sc_events WHERE user =".$user_ID."&& archived = 0") or die(mysql_error());
            
            
            /* Run a loop as usual on your array to create a table of events*/
            foreach($current_events as $current_event){
				$event_info = $current_event->details;
				$event_info_json = json_decode($event_info);
				$event_name = $event_info_json->event_name;
				$date = $current_event->event_date;
				$contact = $event_info_json->contact->first_name . " " . $event_info_json->contact->last_name;
				$venue = $event_info_json->venue;
				$status = $event_info_json->status;
				
				echo "<tr>
                   	<td><a href=\"../stem_counter\">" . $event_name . "</a></td>
						<td>".	$date	."</td>
                     	<td>".	$contact	."</td>
                     	<td>".	$venue	."</td>
						<td>".	$status	."</td>
 						<td>" ?>
							<div class="btn-group">
                         		<button type="button" class="btn btn-theme03">Action</button>
                       		<button type="button" class="btn btn-theme03 dropdown-toggle" data-toggle="dropdown">
                        			<span class="caret"></span>
                            		<span class="sr-only">Toggle Dropdown</span>
                             </button>
                             <ul class="dropdown-menu" role="menu">
                             	<li><a href="<?php echo "#" ?>">Go</a></li>
                               	<li><a href="<?php echo "#" ?>">Archive</a></li>
                                	<li class="divider"></li>
                                	<li><a href="<?php echo "#" ?>">Delete</a></li>
                      		</ul>
                    		</div>      		
                    	</td>
           		</tr>
			<?php ;}
				

            ?>
       
                              
                              
          
                              </tbody>
                          </table>
                      </div><!-- /content-panel -->
                  </div><!-- /col-md-12 -->
              
              
              <!--ARCHIVED EVENTS-->
              </div><!-- /row -->
                  <div class="col-md-12" style="margin-top: 10px; margin-right: 20px;">
                         <a class="pull-right" href="Events/stem_counter.html">Archived Events</a>
                         

                  </div><!-- /col-md-12 -->
                  <div id="feedback"></div>

		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
						
						<!-- Modal -->
						<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						  <div class="modal-dialog">
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						        <h4 class="modal-title" id="myModalLabel">Add New Event</h4>
						      </div>
						      <div class="modal-body">
                             <!-- <div class="form-panel">-->
                              <form class="form-horizontal style-form" type="post" id="newCustomerForm" action="">
                                  <div class="form-top"><h4> Contact </h4></div>
                                  
                                  <div class="form-group">
                             
                                  <label class="col-sm-2 col-sm-2 control-label" required>First Name*</label>
                                  <div class="col-sm-3">
                                      <input type="text"  class="form-control" name="first_name" placeholder="e.g. Sarah">
                                  </div>
                             
                                  <label class="col-sm-2 col-sm-2 control-label align-right">Last Name*</label>
                                  <div class="col-sm-3">
                                      <input type="text"  class="form-control" name="last_name" placeholder="e.g. Johnson">
                                  
                                  </div>
                                  </div>
                                  
                                  
                                  
                                  
                                  
                                  <div class="form-group">
                                      <label class="col-sm-2 col-sm-2 control-label">Event Name*</label>
                                      <div class="col-sm-8">
                                          <input type="text" name="event_name" class="form-control">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-sm-2 col-sm-2 control-label">Venue</label>
                                      <div class="col-sm-8">
                                          <input type="text" name="venue" class="form-control">
                                         
                                      </div>
                                  </div>
                                  <div class="form-group">
                             
                                  <label class="col-sm-2 col-sm-2 control-label">Date*</label>
                                  <div class="col-sm-3">
                                      <input type="text"  class="form-control" name="date" id="datepicker" placeholder="MM/DD/YYYY">
                                  </div>
                                  </div>
                              </form>
						      <!--</div>-->
						      <div class="modal-footer">
                              <div class="pull-left">*required</div>
						        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						        <input type="hidden" name="action" value="addCustomer"/>
                                <button type="submit" name="action" value="addCustomer" class="btn btn-primary" >Let's Go!</button>
						      </div>
						    </div>
						  </div>
						</div>     
                        </div>				


<?php get_footer(); ?>