<?php get_header(); ?>
<?php get_sidebar('event'); ?>

  <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3><i class="fa fa-angle-right"></i> Shopping List</h3>
		  		<div class="row mt">
			  		<div class="col-lg-12">
                      <div class="content-panel">
                      <h4><i class="fa fa-angle-right"></i> Live Floral</h4>
                          <section id="unseen">
                            <table class="table table-bordered table-striped table-condensed">
                              <thead>
                              <tr>
                                  <th>Type</th>
                                  <th>Flower</th>
                                  <th class="numeric"># Flowers</th>
                                  <th class="numeric">Flowers Per Bunch</th>
                                  <th class="numeric">Bunches Needed</th>
                                  <th class="numeric">Price Per Bunch</th>
                                  <th class="numeric">SubTotal Price</th>
                              </tr>
                              </thead>
                              <tbody>
                              <tr>
                                  <td>Focal</td>
                                  <td>Green Medium Succulent</td>
                                  <td class="numeric">4</td>
                                  <td class="numeric">1</td>
                                  <td class="numeric">4</td>
                                  <td class="numeric">$3</td>
                                  <td class="numeric">$12</td>
                              </tr>
                              <tr>
                                  <td>Focal</td>
                                  <td>Small Airplant</td>
                                  <td class="numeric">4</td>
                                  <td class="numeric">1</td>
                                  <td class="numeric">7</td>
                                  <td class="numeric">$2</td>
                                  <td class="numeric">$14</td>
                              </tr>
								 <tr>
                                  <td>Focal</td>
                                  <td>Light Pink Sweet Peas</td>
                                  <td class="numeric">36</td>
                                  <td class="numeric">10</td>
                                  <td class="numeric">4</td>
                                  <td class="numeric">$15</td>
                                  <td class="numeric">$60</td>
                              </tr>
                              <tr>
                                  <td>Focal</td>
                                  <td>Yellow Football Mums</td>
                                  <td class="numeric">10</td>
                                  <td class="numeric">10</td>
                                  <td class="numeric">1</td>
                                  <td class="numeric">$9</td>
                                  <td class="numeric">$9</td>
                              </tr>
                              <tr>
                                  <td>Focal</td>
                                  <td>Light Pink Stock</td>
                                  <td class="numeric">37</td>
                                  <td class="numeric">10</td>
                                  <td class="numeric">4</td>
                                  <td class="numeric">$9</td>
                                  <td class="numeric">$36</td>
                              </tr>
                              <tr>
                                  <td>Focal</td>
                                  <td>White Football Mums</td>
                                  <td class="numeric">27</td>
                                  <td class="numeric">10</td>
                                  <td class="numeric">3</td>
                                  <td class="numeric">$9</td>
                                  <td class="numeric">$27</td>
                              </tr>
                              <tr>
                                  <td>Focal</td>
                                  <td>Mini Succulent</td>
                                  <td class="numeric">2</td>
                                  <td class="numeric">1</td>
                                  <td class="numeric">2</td>
                                  <td class="numeric">$1</td>
                                  <td class="numeric">$2</td>
                              </tr>
                             	<tr>
                                  <td>Secondary</td>
                                  <td>Yellow Billyball</td>
                                  <td class="numeric">47</td>
                                  <td class="numeric">10</td>
                                  <td class="numeric">5</td>
                                  <td class="numeric">$10</td>
                                  <td class="numeric">$50</td>
                              </tr>
                              <tr>
                                  <td>Secondary</td>
                                  <td>Blue Thistle</td>
                                  <td class="numeric">17</td>
                                  <td class="numeric">10</td>
                                  <td class="numeric">2</td>
                                  <td class="numeric">$7.50</td>
                                  <td class="numeric">$15.00</td>
                              </tr>
                              <tr>
                                  <td>Secondary</td>
                                  <td>Light Yellow Sweet Peas</td>
                                  <td class="numeric">10</td>
                                  <td class="numeric">10</td>
                                  <td class="numeric">1</td>
                                  <td class="numeric">$15</td>
                                  <td class="numeric">$15</td>
                              </tr>
                              <tr>
                                  <td>Greenery</td>
                                  <td>Jasmine Vine</td>
                                  <td class="numeric">2</td>
                                  <td class="numeric">1</td>
                                  <td class="numeric">2</td>
                                  <td class="numeric">$8</td>
                                  <td class="numeric">$16</td>
                              </tr>
                               <tr>
                                  <td></td>
                                  <td></td>
                                  <td class="numeric"></td>
                                  <td class="numeric"></td>
                                  <td class="numeric"></td>
                                  <td>Estimated Cost</td>
                                  <td class="numeric">$256.00</td>
                              </tr>
                              
                              </tbody>
                          </table>
                          </section>
                  </div><!-- /content-panel -->
               </div><!-- /col-lg-4 -->			
		  	</div><!-- /row -->

		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      
<?php get_footer(); ?>