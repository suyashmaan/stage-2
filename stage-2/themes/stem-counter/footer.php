
      <!-- /MAIN CONTENT -->

      <!--main content end-->
      <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2014 - StemCounter.com
              <a href="basic_table.html#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.scrollTo.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.nicescroll.js" type="text/javascript"></script>


    <!--common script for all pages-->
    <script src="<?php echo get_template_directory_uri(); ?>/js/common-scripts.js"></script>

    <!--script for this page-->
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery-ui-1.9.2.custom.min.js"></script>

	<!--custom switch-->
	<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap-switch.js"></script>
	
	<!--custom tagsinput-->
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.tagsinput.js"></script>
	
	<!--custom checkbox & radio-->
	
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/bootstrap-daterangepicker/daterangepicker.js"></script>
	
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
	
	
	<script src="<?php echo get_template_directory_uri(); ?>/js/form-component.js"></script>    
    
    
  <script>
      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>
  </body>
</html>
