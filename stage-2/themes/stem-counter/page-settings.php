<?php get_header(); ?>
<?php get_sidebar(); ?>

 <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
           	 <h3><i class="fa fa-angle-right"></i> Settings</h3>
          	
          	<!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt">
                <div class="col-lg-12">
                  <div class="form-panel">
                      <h4 class="mb"><i class="fa fa-angle-right"></i> Company Details</h4>
                      <button type="button" style="margin-top: 0px; margin-bottom:5px; margin-left:10px" class="btn btn-primary btn-sm">Edit</button>
                      
                      <form class="form-horizontal style-form" method="get">
                          <fieldset>
                          <div class="form-group col-lg-12">
                          <label class="col-lg-12 control-label">Company Name: <br> Twisted Willow</label>
                          <label class="col-lg-12 control-label"><br>Address: <br> 26 St. Stanislaus <br> Florissant, MO 63031</label>
                          <label	class="col-lg-12 control-label"><br>Website: <br> http://www.TwistedWillow.co <br> Phone: <br> 870-275-1056</label>
                          
                          </div>
                          
                        </fieldset>
                        
                      </form>
              	</div>
          		</div>   	
          	</div><!-- /row -->
          	
            
            
 			<div class="row mt">
                  <div class="col-lg-12">
                      <div class="form-panel">
                          <table class="table table-striped table-advance table-hover">
	                  	  	  <h4><i class="fa fa-angle-right"></i> Invoicing Settings</h4>
	                  	  	  <hr>
                              <thead>
                              <tr>
                                  <th><i class="fa fa-bullhorn"></i> Hardgood Multiple</th>
                                  <th><i class="fa fa-question-circle"></i> Fresh Flower Multiple</th>
                                  <th><i class="fa fa-bookmark"></i> % Increase in flowers ordered</th>
                                  <th><i class=" fa fa-edit"></i> Charge Card Rate</th>
                                  <th><i class=" fa fa-edit"></i> Sales Tax</th>
                                  <th></th>
                              </tr>
                              </thead>
                              <tbody>
                              <tr>
                                  <td>2</td>
                                  <td>2.8</td>
                                  <td>10%</td>
                                  <td>3.5%</td>
                                  <td>8.65%</td>
                                  
                                  <td>
                                      <button class="btn btn-success btn-xs"><i class="fa fa-check"></i></button>
                                      <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button>
                                      <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button>
                                  </td>
                              </tr>
          
                              </tbody>
                          </table>
                      </div><!-- /content-panel -->
                  </div><!-- /col-md-12 -->
              </div><!-- /row -->
          	
          	       	<!-- INLINE FORM ELELEMNTS -->
          	<div class="row mt">
          		<div class="col-lg-12">
          			<div class="form-panel">
                  	  <h4 class="mb"><i class="fa fa-angle-right"></i> Change Password</h4>
                      <form class="form-inline" role="form">
                          <div class="form-group">
                              <label class="sr-only" for="exampleInputEmail2">Password</label>
                              <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Password">
                          </div>
                          <div class="form-group">
                              <label class="sr-only" for="exampleInputPassword2">Confirm Password</label>
                              <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Confirm Password">
                          </div>
                          <button type="submit" class="btn btn-theme">Change Password</button>
                      </form>
          			</div><!-- /form-panel -->
          		</div><!-- /col-lg-12 -->
          	</div><!-- /row -->
          	
		</section><!--/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->

<?php get_footer(); ?>