<?php get_header(); ?>
<?php get_sidebar('event'); ?>



<!-- On Page PHP -->
<!--Fix!!!!-->
<?php 
	/* Call WP's "get_results" on your query and create the array */
	global $wpdb;
	
	//USER SECURITY
   $user_ID = get_current_user_id(); 
	
	//LOAD EVENT DETAILS
	//FIX!! WE WANT THIS EVENT ID TO PULL FROM THE BROWSER
	$event_ID = "1001";
	$event_details = $wpdb->get_results("SELECT event_order FROM sc_events WHERE user = ".$user_ID." AND event = ".$event_ID) or die(mysql_error());
            /* Run a loop as usual on your array to create a table of events*/
			foreach($event_details as $event_detail){
		?>
       <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->

      <section id="main-content">
          <section class="wrapper">
            <div class="col-lg-2">
           	 <h3><i class="fa fa-angle-right"></i> StemCounter</h3>
             </div>
             <div class="col-lg-8"></div>
             <div class="col-lg-2">
				<button type="button" align="right" style="margin-top: 10px; margin-bottom:10px;" class="btn btn-primary btn-lg">New Arrangement</button>
              </div>
              <div class="row mt">
                  <div class="col-md-12">
                  
                  
             
				<?php
				$arrangements = json_decode($event_detail->event_order);
				foreach($arrangements as $arrangement){
					
					?>
                
                <!--Each arrangement-->
                 <div class="content-panel content-panel-stem-count col-md-6">
                  
                   <div>
                     <div class="pull-right col-md-1"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></div> 
                     <div><h4><?php echo $arrangement->arrangement;?></h4></div>
                     
                    </div>
                     <div><h5>Quantity: <?php echo $arrangement->qty; ?></h5></div>
                    
                    <table class="table table-striped table-advance table-hover arrangement-tables">
                    <thead>
                    <tr>
                          <th><i class="fa fa-bullhorn"></i> Flower</th>
                          <th>Quantity</th>
                          <th>Cost</th>
                          <th>Type</th>
                          <th></th>
                      </tr>
                      </thead>
                      <tbody>
              
              <?php 
			  
				  $items = $arrangement->items;
				  foreach($items as $item => $details){ 
						
						//Add items as the loop goes through
						$price += $details->qty * $details->cost;

				  ?>
						  <tr>
							  <td> <?php echo $item; ?> </td>
                         
                           <td><?php echo $details->qty; ?></td>
                           <td><?php echo $details->cost; ?></td>
                           <td><span class="label label-info label-mini"><?php echo $details->type; ?></span></td>           

                           </tr>
									  
					<?php 
				 	}
					?> </tbody>
                    
                    <tfoot>
                    <tr>
                    <td></td>
                    <td></td>
                      <td></td>
                    <td>
                    
                    		<!--Print price-->
                    		Arrangement Price = <?php echo $price;
				
							?>	
                    </td>
                  
                    </tr>
                    </tfoot>
                    </table>
                    </div> <?php 
					//Reset price then end arrangement loop
								 $price = 0;
									}
								}  ?>
				
       
                  
                  
                   
                  </div><!-- /col-md-12 -->
              </div><!-- /row -->

		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->

<?php get_footer(); ?>