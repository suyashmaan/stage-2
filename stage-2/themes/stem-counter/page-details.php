<?php get_header(); ?>
<?php get_sidebar('event'); ?>



<!-- On Page PHP -->
<!--Fix!!!!-->
<!--< ? php 
	/* Call WP's "get_results" on your query and create the array */
	global $wpdb;
	
	//USER SECURITY
   $user_ID = get_current_user_id(); 
	
	//LOAD EVENT DETAILS
	//FIX!! WE WANT THIS EVENT ID TO PULL FROM THE BROWSER
	$event_ID = "1001";
	$event_details = $wpdb->get_results("SELECT * FROM sc_events WHERE user = ".$user_ID." AND event = ".$event_ID) or die(mysql_error()); 
	foreach($event_details as $event_detail){
		$event_info = $event_detail->details;
		$event_info_json = json_decode($event_info);
		$event_name = $event_info_json->event_name;
		$date = $event_detail->event_date;
		$contact = $event_info_json->contact->first_name . " " . $event_info_json->contact->last_name;
		$venue = $event_info_json->venue;
		$status = $event_info_json->status; ?>-->
     <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3><i class="fa fa-angle-right"></i> Event Details</h3>
          	
          	<!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt">
                <div class="col-lg-12">
                  <div class="form-panel">
                  	  <h4 class="mb"><i class="fa fa-angle-right"></i> General Information</h4>
                      
                      
                      <form class="form-horizontal style-form" method="get">
                      <fieldset>
                      <div class="form-group">
                             
                              <label class="col-sm-2 col-sm-2 control-label">Event Name</label>
                              <div class="col-sm-10">
                                  <input type="text"  class="form-control" placeholder="e.g. Johnson Wedding">
                              </div>
                             
                          </div>
                          
                          <div class="form-group">
                             
                              <label class="col-sm-2 col-sm-2 control-label">Date</label>
                              <div class="col-sm-2">
                                  <input type="text"  class="form-control" placeholder="MM/DD/YYYY">
                              </div>
                          </div>
                          
                          <!--Bride's Info-->
                         <form class="form-inline" role="form">
                          <div class="form-group col-sm-2">
                              <label class="control-label">Bride's First Name</label>
                              <input type="text" class="form-control">
                              
                          </div>
                          <div class="form-group col-sm-2" style="margin-left:2%">
                              <label class="control-label">Bride's Last Name</label>
                              <input type="text" class="form-control">
                              
                              </div>
                              
                          <div class="form-group col-sm-2" style="margin-left:2%">
                              <label class="control-label">Phone Number</label>
                              <input type="text" class="form-control">
                              
                          </div>
                          <div class="form-group col-sm-2" style="margin-left:2%">
                              <label class="control-label">Email Address</label>
                              <input type="text" class="form-control">
                              
                          </div>
                        </form>  <!-- End Bride's Info-->
                        </fieldset>
                      </form>
              	</div>
          		</div><!--  col-lg-6-->      	
          	</div><!-- /row -->
          	
         <!-- VENUE -->
          	<div class="row mt">
                <div class="col-lg-12">
                  <div class="form-panel">
                  	  <h4 class="mb"><i class="fa fa-angle-right"></i> Venue</h4>
                      
                      
                      <form class="form-horizontal style-form" method="get">
                      <fieldset>
                      <div class="form-group">
                             
                              <label class="col-sm-2 col-sm-2 control-label">Venue</label>
                              <div class="col-sm-10">
                                  <input type="text"  class="form-control" placeholder="e.g. The Coronado">
                              </div>
                             
                          </div>
                          
                          <div class="form-group">
                             
                              <label class="col-sm-2 col-sm-2 control-label">Address</label>
                              <div class="col-sm-10">
                                  <input type="text"  class="form-control" placeholder="Address">
                              </div>
                          </div>
                         
                      </form>
              	</div>
          		</div><!--  col-lg-6-->      	
          	</div><!-- /row -->
            
          	<!-- INLINE FORM ELELEMNTS -->
          	<div class="row mt">
          		<div class="col-lg-12">
          			<div class="form-panel">
                  	  <h4 class="mb"><i class="fa fa-angle-right"></i> Inline Form</h4>
                      <form class="form-inline" role="form">
                          <div class="form-group">
                              <label class="sr-only" for="exampleInputEmail2">Email address</label>
                              <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Enter email">
                          </div>
                          <div class="form-group">
                              <label class="sr-only" for="exampleInputPassword2">Password</label>
                              <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Password">
                          </div>
                          <button type="submit" class="btn btn-theme">Sign in</button>
                      </form>
          			</div><!-- /form-panel -->
          		</div><!-- /col-lg-12 -->
          	</div><!-- /row -->
          	
          	
          	
          	
		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->

<?php get_footer(); ?>