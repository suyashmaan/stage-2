<?php get_header(); ?>
<?php get_sidebar('event'); ?>

<!-- On Page PHP -->
<!--Fix!!!!-->
<?php 
	/* Call WP's "get_results" on your query and create the array */
	global $wpdb;
	
	//USER SECURITY
   $user_ID = get_current_user_id(); 
	
	//LOAD EVENT DETAILS
	//FIX!! WE WANT THIS EVENT ID TO PULL FROM THE BROWSER
	$event_ID = "1001";
	$event_details = $wpdb->get_results("SELECT * FROM sc_events WHERE user = ".$user_ID." AND event = ".$event_ID) or die(mysql_error()); 
	foreach($event_details as $event_detail){
		$event_info = $event_detail->details;
		$event_info_json = json_decode($event_info);
		$event_name = $event_info_json->event_name;
		$date = $event_detail->event_date;
		$contact = $event_info_json->contact->first_name . " " . $event_info_json->contact->last_name;
		$venue = $event_info_json->venue;
		$status = $event_info_json->status; ?>
  <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">

<div class="container content-panel">
      <div class="row">
        <div class="col-md-9">
          <div class="col-md-3"></div>
          <div class="col-md-3">
          
          
          
          <!--FIX!!! No logo in the first version-->
          <!--
          <h1>
            <a href="https://TwistedWillow.co">
            <img src="../assets/img/logo.png">
            </a>
          </h1>-->
          </div>
          
        </div>
          <div class="col-md-3 text-right">
          <h1>PROPOSAL</h1>
          

          <!--ADD PROPOSAL NUMBER LATER
          <h1><small>Invoice #001</small></h1>-->
          
          
        </div>
      </div>
      <div class="row">
        <div class="col-xs-5">
          <div class="panel panel-default">
            <div class="panel-heading">
           
            <!--Fix!!!!-->
              <h4>From: <a href="#">Twisted Willow</a></h4>
            </div>
            <div class="panel-body">
              
              <!--Fix!!!!-->
              <p>
                Address: 26 St. Stanislaus <br>
                Florissant, MO 63031 <br>
                Website: http://TwistedWillow.co <br>
                Email : rachael@twistedwillow.co <br><br>
                Mobile : 870-275-1056 <br> <br>
                Instagram : <a href="https://instagram.com/TwistedWillowDesign">@TwistedWillowDesign</a>
                </p>
                
              </p>
            </div>
          </div>
        </div>
        <div class="col-xs-5 col-xs-offset-2 text-right">
          <div class="panel panel-default">
            <div class="panel-heading">
              
              <!--Fix!!!!-->
              <h4>To : <a href="#"><?php echo $contact ?></a></h4>
            </div>
            <div class="panel-body">
              
              <!--Fix!!!!-->
              <p>
                123 Elm Street <br>
                St. Louis, MO 63031 <br>
              </p>
            </div>
          </div>
        </div>
      </div>
      <!-- / end client details section -->
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>
              <h4>Product/Service</h4>
            </th>
            <th>
              <h4>Description</h4>
            </th>
            <th>
              <h4>Qty</h4>
            </th>
            <th>
              <h4>Price</h4>
            </th>
            <th>
              <h4>Sub Total</h4>
            </th>
          </tr>
        </thead>
        <tbody>
           
			
			
           <?php
            /* Run a loop as usual on your array to create a table of events*/
				$arrangements = json_decode($event_detail->event_order);
				foreach($arrangements as $arrangement){
					?>
					  <tr>
                    <td><?php echo $arrangement->arrangement;?></td>
					
					<td>
					
					<?php
					//Loop through all of the flowers names to list them in the proposal
					$items = $arrangement->items;
				  	foreach($items as $item => $details){ 
						
						//Add items as the loop goes through
						$price += $details->qty * $details->cost;

					?>
                    

            		<a href="#"><?php echo $item; 
								
					?></a>,
					
					<?php
					}?>

                    
                   </td>
                    <td class="text-right"><?php echo $arrangement->qty; ?></td>
                    <td class="text-right"><?php echo $price;?>	</td>
                    <td class="text-right"><?php $item_total = $arrangement->qty * $price; echo $item_total; ?> </td>
                  </tr>
               

                    </div> <?php 
					//Reset price then end arrangement loop
								 $price = 0;
						$subtotal += $item_total;
									}
								}  ?>
		
        </tbody>
      </table>
      <div class="row text-right">
        <div class="col-xs-2 col-xs-offset-8">
         
          <p>
            <strong>
            Sub Total : <br>
            TAX : <br>
            Total : <br>
            </strong>
          </p>
        </div>
        <div class="col-xs-2">
          <strong>
          <?php echo number_format($subtotal, 2, '.', ','); ?> <br>
          
          <!-- ADD TAX RATE -->
          <?php $taxes = number_format($subtotal * 0.0813, 2, '.',','); echo $taxes; ?> <br>
          $<?php $grand_total = number_format($subtotal + $taxes, 2, '.',','); echo $grand_total;?><br>
          </strong>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-5">
          <div class="panel panel-info">
            <div class="panel-heading">
              <h4>Event details</h4>
            </div>
            
            <div class="panel-body">
              <p>Venue: <?php echo $venue ?></p>
              <p>Date: <?php echo $date ?></p>
              
              <!--FIXX!!! ADD ANY OTHER DETAILS HERE AS NEEDED. ADD COLORS LATER.
              <p>Colors: Pink, Green</p>
              -->
              
            </div>
          </div>
        </div>
        <div class="col-xs-7">
          <div class="span7">
            <div class="panel panel-info">
              <div class="panel-heading">
                <h4>Contract Specifics</h4>

              </div>
              <div class="panel-body">
                <p>
                These are the disclaimers that they need to know.             Invoice was created on a computer and is valid without the signature and seal.
                </p>
                <h4>Payment should be made via the invoice</h4>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
         </div>




		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->
      
      
      
      
      

      <!--main content end-->
      
<?php get_footer(); ?>